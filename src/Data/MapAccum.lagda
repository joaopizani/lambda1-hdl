\begin{code}
module Data.MapAccum where

open import Function.Base using (_∘′_; id)
open import Data.Product.Base using (_×_; _,_; uncurry) renaming (map to map×)
open import Data.Vec.Base using (Vec; _∷_; []; unzip; zip)

open import Lambda1.Core.Notation using (α; β; n; φ; ψ; θ)
\end{code}


%<*mapAccumL>
\AgdaTarget{mapAccumL}
\begin{code}
mapAccumL : (f : θ → α → (θ × β)) (s : θ) (xs : Vec α n) → θ × Vec β n
mapAccumL _ s []        =  s , []
mapAccumL f s (x ∷ xs′) =  let  s′ , y    = f s x
                                s″ , ys′  = mapAccumL f s′ xs′
                           in  s″ , (y ∷ ys′)
\end{code}
%</mapAccumL>



%<*swap12>
\AgdaTarget{swap12}
\begin{code}
swap12 : α × (β × φ) → β × (α × φ)
swap12 (a , (b , c)) = b , (a , c)
\end{code}
%</swap12>


%<*transformF>
\AgdaTarget{transformF}
\begin{code}
transformF : (f : β → θ → α → ψ → (β × α × φ)) → (θ → α → β → ψ → (α × β × φ))
transformF f γ x s y = swap12 (f s γ x y)
\end{code}
%</transformF>


%<*mapAccumTwo>
\AgdaTarget{mapAccum²}
\begin{code}
mapAccum² : (f : θ → α → ψ → (θ × β × φ)) (s : θ) (xs : Vec α n) (ys : Vec ψ n) → θ × Vec β n × Vec φ n
mapAccum² f s xs ys = map× id unzip (mapAccumL (uncurry ∘′ f) s (zip xs ys))
\end{code}
%</mapAccumTwo>
