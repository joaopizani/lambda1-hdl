\begin{code}
module Data.Star.Environment.Extra {ℓ} (Ty : Set ℓ) where

open import Data.Nat.Base using (ℕ; zero; suc; _∸_)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε; _◅_; map)
open import Data.Star.Pointer using (done; step)
open import Data.Star.Decoration using (↦)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
open import Relation.Binary.PropositionalEquality.TrustMe using (trustMe)

import Data.Star.Environment as Env
\end{code}


* Syntactical part (no values, only types)
\begin{code}
open Env Ty using (Ctxt; _∋_; vz; vs)
\end{code}



%<*replicate-Ctxt>
\AgdaTarget{replicateCtxt}
\begin{code}
replicateCtxt : ∀ (Γ : Ctxt) (n : ℕ) (τ : Ty) → Ctxt
replicateCtxt Γ zero      _  = Γ
replicateCtxt Γ (suc n′)  τ  = τ ◅ replicateCtxt Γ n′ τ
\end{code}
%</replicate-Ctxt>


%<*Ctxt-length>
\AgdaTarget{lengthCtxt}
\begin{code}
lengthCtxt : ∀ (Γ : Ctxt) → ℕ
lengthCtxt ε         = zero
lengthCtxt (_ ◅ Γ′)  = suc (lengthCtxt Γ′)
\end{code}
%</Ctxt-length>


%<*ix>
\AgdaTarget{ix₀,ix₁,ix₂,ix₃,ix₄}
\begin{code}
ix₀ : ∀ {Γ : Ctxt} {α}             → α ◅ Γ ∋ α
ix₁ : ∀ {Γ : Ctxt} {α β}           → α ◅ β ◅ Γ ∋ β
ix₂ : ∀ {Γ : Ctxt} {α β δ}         → α ◅ β ◅ δ ◅ Γ ∋ δ
ix₃ : ∀ {Γ : Ctxt} {α β δ θ}       → α ◅ β ◅ δ ◅ θ ◅ Γ ∋ θ
ix₄ : ∀ {Γ : Ctxt} {α β δ θ κ}     → α ◅ β ◅ δ ◅ θ ◅ κ ◅ Γ ∋ κ
ix₀ = vz;  ix₁ = vs ix₀;  ix₂ = vs ix₁;  ix₃ = vs ix₂;  ix₄ = vs ix₃
\end{code}
%</ix>


%<*Ctxt-subeq>
\AgdaTarget{\_⊇\_}
\begin{code}
data _⊇_ : (Δ Γ : Ctxt) → Set ℓ where
  R : ∀ {Γ}          → Γ ⊇ Γ
  O : ∀ {Δ Γ τ}      → Δ ⊇ Γ → (τ ◅ Δ) ⊇ Γ
  I : ∀ {Δ Γ τ}      → Δ ⊇ Γ → (τ ◅ Δ) ⊇ (τ ◅ Γ)
\end{code}
%</Ctxt-subeq>


%<*comp⊇>
\AgdaTarget{comp⊇}
\begin{code}
comp⊇ : ∀ {Γ Δ Θ : Ctxt} (q : Δ ⊇ Γ) (p : Θ ⊇ Δ) → Θ ⊇ Γ
comp⊇ q       R       = q
comp⊇ q       (O p′)  = O (comp⊇ q p′)
comp⊇ R       (I p′)  = I p′
comp⊇ (O q′)  (I p′)  = O (comp⊇ q′ p′)
comp⊇ (I q′)  (I p′)  = I (comp⊇ q′ p′)
\end{code}
%</comp⊇>


%<*⊇ε>
\AgdaTarget{⊇ε}
\begin{code}
⊇ε : ∀ {Γ : Ctxt} → Γ ⊇ ε
⊇ε {ε}      = R
⊇ε {_ ◅ _}  = comp⊇ ⊇ε (O R)
\end{code}
%</⊇ε>


%<*Kix>
\AgdaTarget{Kix}
\begin{code}
Kix : ∀ {Δ Γ : Ctxt} {τ} (p : Δ ⊇ Γ) → (Γ ∋ τ → Δ ∋ τ)
Kix R        i                 = i
Kix (O p)    i                 = vs (Kix p i)
Kix (I _)    (done refl  ◅ _)  = vz
Kix (I p)    (step _     ◅ i)  = vs (Kix p i)
\end{code}
%</Kix>



* Following three definitions (_⊒_; get⊒; reIx₀⊒) are used only in the unembedding technique to go from HOAS to De Bruijn repr.

%<*Ctxt-suffix>
\AgdaTarget{\_⊒\_}
\begin{code}
data _⊒_ : (Δ Γ : Ctxt) → Set ℓ where
  refl⊒  : ∀ {Γ′ Γ}   (p : Γ′ ≡ Γ)   → Γ′ ⊒ Γ
  ◅⊒     : ∀ {Γ Δ τ}  (Δ⊒Γ : Δ ⊒ Γ)  → (τ ◅ Δ) ⊒ Γ
\end{code}
%</Ctxt-suffix>


* Assuming that Γ is a suffix of Δ and d = (length Δ) - (length Γ), otherwise stuck in postulate
%<*get-inclusion>
\AgdaTarget{get⊒}
\begin{code}
get⊒ : ∀ (d : ℕ) (Δ Γ : Ctxt) → Δ ⊒ Γ
-- FROM ASSUMPTION: Γ′ ≡ Γ when d = 0 (no guarantee of def. eq.)
get⊒ zero      Γ′        Γ = refl⊒ trustMe
get⊒ (suc d′)  (σ ◅ Δ′)  Γ = ◅⊒ {τ = σ} (get⊒ d′ Δ′ Γ)
-- IMPOSSIBLE CASE: d > 0, Δ ⊒ Γ → Δ ≠ ε
get⊒ (suc d′)  ε         Γ = ☇ where postulate ☇ : ε ⊒ Γ
\end{code}
%</get-inclusion>


%<*reIx0-given-suffix-inclusion>
\AgdaTarget{reIx₀⊒}
\begin{code}
reIx₀⊒ : ∀ (Δ Γ : Ctxt) τ → Δ ⊒ (τ ◅ Γ) → Δ ∋ τ
reIx₀⊒ .(τ ◅ Γ)  Γ τ (refl⊒ refl)  = vz
reIx₀⊒ (σ ◅ Δ′)  Γ τ (◅⊒ Δ′⊒τ◅Γ)   = vs (reIx₀⊒ Δ′ Γ τ Δ′⊒τ◅Γ)
\end{code}
%</reIx0-given-suffix-inclusion>


%<*reIx0-index>
\AgdaTarget{reIx₀∋}
\begin{code}
reIx₀∋ : ∀ τ (Γ Δ : Ctxt) → Δ ∋ τ
reIx₀∋ τ Γ Δ = reIx₀⊒ Δ Γ τ (get⊒ d Δ (τ ◅ Γ))
  where d = lengthCtxt Δ ∸ suc (lengthCtxt Γ)
\end{code}
%</reIx0-index>



* Semantic part (Env = semantic Ctxt)
\begin{code}
open Env Ty using (Env)
\end{code}


-- Whereas _⊇_ show HOW a Ctxt in embedded in another, Env⊇ will,
-- given a big |Env Δ|, return the small |Env Γ| that resides in it according to |p : Δ ⊇ Γ|
%<*EnvK>
\AgdaTarget{EnvK}
\begin{code}
Env⊇ : ∀ {F : Ty → Set} {Γ Δ : Ctxt} (p : Δ ⊇ Γ) → (Env F Δ → Env F Γ)
Env⊇ R       δ           = δ
Env⊇ (O p′)  (↦ _ ◅ δ′)  = Env⊇ p′ δ′
Env⊇ (I p′)  (↦ t ◅ δ′)  = ↦ t ◅ Env⊇ p′ δ′
\end{code}
