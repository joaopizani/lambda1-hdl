\begin{code}
module Data.Star.Environment.Extra.Properties where

open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (map; ε; _◅_)
open import Data.Star.Pointer using (done; step)
open import Relation.Binary.PropositionalEquality.Core using (refl; _≡_)
import Data.Star.Environment

import Data.Star.Environment.Extra
\end{code}


%<*modules-env-props>
\begin{code}
open module EnvImp {ℓ} {τ : Set ℓ} = Data.Star.Environment τ using (_∋_; Ctxt; Env)
open module EnvExtraImp {ℓ} {τ : Set ℓ} = Data.Star.Environment.Extra τ using (_⊇_; O; I; R; Env⊇; comp⊇)
\end{code}
%</modules-env-props>


\begin{code}
module MapProps {ℓ₁} (Ty₁ : Set ℓ₁) {ℓ₂} (Ty₂ : Set ℓ₂) where
\end{code}

%<*map-∋>
\AgdaTarget{map-∋}
\begin{code}
 map-∋ : ∀ {Γ τ} (f : Ty₁ → Ty₂) (i : Γ ∋ τ) → map f Γ ∋ f τ
 map-∋ {Γ = ε}      f (() ◅ _)
 map-∋ {Γ = _ ◅ _}  f (done refl  ◅ ε)   = done refl  ◅ ε
 map-∋ {Γ = _ ◅ _}  f (step _     ◅ i′)  = step _     ◅ map-∋ f i′
\end{code}
%</map-∋>


%<*map-⊇>
\AgdaTarget{map-⊇}
\begin{code}
 map-⊇ : ∀ {Γ Δ} (f : Ty₁ → Ty₂) (p : Γ ⊇ Δ) → map f Γ ⊇ map f Δ
 map-⊇ f R       = R
 map-⊇ f (O p′)  = O (map-⊇ f p′)
 map-⊇ f (I p′)  = I (map-⊇ f p′)
\end{code}
%</map-⊇>


%<*Env⊇-comp⊇>
\AgdaTarget{Env⊇-comp⊇}
\begin{code}
Env⊇-comp⊇ : ∀  {ℓ} {Ty : Set ℓ} {F : Ty → Set} {Δ Γ Θ : Ctxt} {δ : Env F Δ}
                (q : Γ ⊇ Θ) (p : Δ ⊇ Γ) → Env⊇ q (Env⊇ p δ) ≡ Env⊇ (comp⊇ q p) δ
Env⊇-comp⊇ q       R       = refl
Env⊇-comp⊇ q       (O p′)  = ⊥ where postulate ⊥ : _
Env⊇-comp⊇ R       (I p′)  = refl
Env⊇-comp⊇ (O q′)  (I p′)  = ⊥ where postulate ⊥ : _
Env⊇-comp⊇ (I q′)  (I p′)  = ⊥ where postulate ⊥ : _
\end{code}
%</Env⊇-comp⊇>
