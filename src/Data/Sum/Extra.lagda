\begin{code}
module Data.Sum.Extra where

open import Level using (Level)
open import Data.Sum.Base using (_⊎_; inj₁; inj₂)
open import Data.Product.Base using (Σ-syntax; _,_)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl)
\end{code}


\begin{code}
private
 variable
  ℓ : Level
  α β : Set ℓ
\end{code}


%<*union-injective>
\AgdaTarget{⊎-injective}
\begin{code}
⊎-injective : {xy : α ⊎ β} {wz : α ⊎ β} (p : xy ≡ wz) → (Σ[ x ∈ α ] Σ[ w ∈ α ] x ≡ w) ⊎ (Σ[ y ∈ β ] Σ[ z ∈ β ] y ≡ z)
⊎-injective {xy = inj₁ x} {inj₁ .x} refl = inj₁ (x , x , refl)
⊎-injective {xy = inj₂ y} {inj₂ .y} refl = inj₂ (y , y , refl)
\end{code}
%</union-injective>
