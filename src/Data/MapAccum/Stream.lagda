\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Data.MapAccum.Stream where

open import Data.Product.Base using (_×_; _,_)
open import Codata.Musical.Notation using (♯_; ♭)
open import Codata.Musical.Stream using (Stream; _∷_)

open import Lambda1.Core.Notation using (α; β; φ)
\end{code}


%<*mapAccumStream>
\AgdaTarget{mapAccumStream}
\begin{code}
mapAccumStream : (f : φ → α → (φ × β)) (s : φ) (xs : Stream α) → (Stream φ × Stream β)
mapAccumStream f s (x ∷ xs′) =  let  s′ , y    = f s x
                                     s″ , ys″  = mapAccumStream f s′ (♭ xs′)
                                in (s′ ∷ ♯ s″) , (y ∷ ♯ ys″)
\end{code}
%</mapAccumStream>
