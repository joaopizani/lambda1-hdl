\begin{code}
{-# OPTIONS --guardedness #-}

import Data.MapAccum

import Lambda1.Core.Notation

import Lambda1.Core.Syn.UTypes
import Lambda1.Core.Syn.Gates
import Lambda1.Core.Syn.Lang
import Lambda1.Core.Syn.State

import Lambda1.Core.Sem.UTypes
import Lambda1.Core.Sem.Gates
import Lambda1.Core.Sem.Lang

import Lambda1.Core.Sim.Base
import Lambda1.Core.Sim.K
import Lambda1.Core.Sim.Refine

import Lambda1.HOAS.Syn.Lang
import Lambda1.HOAS.Syn.State

import Lambda1.HOAS.Sim.Base

import Lambda1.HOAS.Patterns.Base
import Lambda1.HOAS.Patterns.Properties

import Lambda1.Gates.Bool
import Lambda1.Gates.Nat
import Lambda1.Gates.PPC
import Lambda1.Gates.Regs
import Lambda1.Gates.FinRegs

import Lambda1.Utils.BasicCombinators
import Lambda1.Utils.Patterns.Base
import Lambda1.Utils.Patterns.Properties

import Data.MapAccum.Stream
import Lambda1.Core.Sim.Stream
\end{code}


import Lambda1.HOAS.Sim.Stream

import Samples.BoolTrioComb
import Samples.Nat

import Samples.SharingAndLooping
import Samples.Adders
import Samples.Regs
import Samples.FinNatRegs
