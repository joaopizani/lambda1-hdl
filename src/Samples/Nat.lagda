\begin{code}
module Samples.Nat where

open import Function using (_$_)
open import Data.Nat.Base using (zero; suc)
open import Data.Vec using (Vec; []; _∷_)
open import Data.Star using (_◅_)
open import Relation.Binary.PropositionalEquality using (_≡_)
open import Notation.Base using (n)

open import Data.SimpleTypes using (Γ; _⊗_; vec; I; ⊇ε)
open import Lambda1.Gates.Nat using (Nat; NAT; ADD; MUL)
open import Lambda1.Surface.Patterns using (foldl-par; foldl-scanl-seq)
open import Lambda1.Lambda1 using (_﹩_; module Lambda1WithGates)
open Lambda1WithGates NAT using (λb)
open import Lambda1.Surface.Lambda1 using (⟨_⟩; reIx₀; module Lambda1SurfaceWithGates)
open Lambda1SurfaceWithGates NAT using (λh)
\end{code}


\begin{code}
infixl 6 _+_
\end{code}

%<*add>
\AgdaTarget{\_+\_}
\begin{code}
_+_ : λh Nat → λh Nat → λh Nat
n + m = ⟨ ADD ⟩ {I $ I $ ⊇ε} ﹩ n ﹩ m
\end{code}
%</add>


\begin{code}
infixl 7 _*_
\end{code}

%<*mul>
\AgdaTarget{\_*\_}
\begin{code}
_*_ : λh Nat → λh Nat → λh Nat
n * m = ⟨ MUL ⟩ {I $ I $ ⊇ε} ﹩ n ﹩ m
\end{code}
%</mul-hoas>



-- Horner's method for evaluating a polynomial at a given point
%<*horner>
\AgdaTarget{horner}
\begin{code}
horner : ∀ {n} (x₀ : λh Nat) (an : λh Nat) (asr : Vec (λh Nat) n) → λh Nat
horner {zero}    an  _   []               = an
horner {suc n′}  x₀  an  (a[n-1] ∷ asr′)  = an + (x₀ * horner {n′} x₀ a[n-1] asr′)
\end{code}
%</horner>

%<*Horner>
\AgdaTarget{Horner}
\begin{code}
\end{code}
%</Horner>
Horner : λb ({-x₀-}Nat ◅ {-an-}Nat ◅ {-asr-}vec Nat n ◅ Γ) Nat
Horner {n} {Γ} = horner {n} (reIx₀ (Nat ◅ vec Nat n ◅ Γ)) (reIx₀ (vec Nat n ◅ Γ)) (reIx₀ Γ)



%<*horner-par>
\AgdaTarget{horner-par}
\begin{code}
horner-par : (x₀ : λh Nat) (an : λh Nat) (asr : λh (vec Nat n)) → λh Nat
horner-par x₀ = foldl-par (λ s a → a + x₀ * s)
\end{code}
%</horner-par>

%<*Horner-par>
\AgdaTarget{Horner-par}
\begin{code}
Horner-par : λb ({-x₀-}Nat ◅ {-an-}Nat ◅ {-asr-}vec Nat n ◅ Γ) Nat
Horner-par {n} {Γ} = horner-par {n} (reIx₀ (Nat ◅ vec Nat n ◅ Γ)) (reIx₀ (vec Nat n ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Horner-par>


%<*horner-seq>
\AgdaTarget{horner-seq}
\begin{code}
horner-seq : ∀ (x₀ : λh Nat) (asr : λh Nat) → λh Nat
horner-seq x₀ = foldl-scanl-seq (λ s a → a + x₀ * s)
\end{code}
%</horner-seq>

%<*Horner-seq>
\AgdaTarget{horner-seq}
\begin{code}
Horner-seq : λb ({-x₀-}Nat ◅ {-asr-}Nat ◅ Γ) Nat
Horner-seq {Γ} = horner-seq (reIx₀ (Nat ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Horner-seq>
