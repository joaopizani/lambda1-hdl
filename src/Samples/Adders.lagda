\begin{code}
module Samples.Adders where

open import Data.Bool using (Bool; false; true; _∧_; _∨_) renaming (not to ¬; _xor_ to _⊻_)
open import Data.Bool.Properties using (∨-isCommutativeMonoid)
open import Data.Product using (proj₂) renaming (_,_ to _,†_)
open import Data.Vec using ([] ; _∷_)
open import Data.Star using (ε; _◅_)
open import Data.Star.Decoration using (↦)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; cong; module ≡-Reasoning)
open ≡-Reasoning

open import Algebra.Structures using (IsCommutativeMonoid)
open IsCommutativeMonoid ∨-isCommutativeMonoid using () renaming (identity to ∨-identity)

open import Lambda1.Core.Notation using (n)
open import Lambda1.Core.Syn.UTypes using (Γ; vec; _⊗_)
open import Lambda1.Core.Syn.State using (St)
open import Lambda1.Core.Sem.UTypes using (Env; Val)

open import Lambda1.Gates.Bool using (B4) renaming (module Sim to B4Sim);  open B4Sim using (Bι; B4gS)
open import Lambda1.Core.Syn.Lang using ( _，_ ; Foldl-par; K₁) renaming (module WithGates to WithGatesCore)
open WithGatesCore B4 using (λb)
open import Lambda1.Utils.Patterns.Base using (MapAccL2-par′)

open import Lambda1.Core.Sim.Base using () renaming (module WithGateSim to WithGateSimCore)
open WithGateSimCore B4gS using () renaming (⟦_⟧S to ⦅_⦆Si)

open import Lambda1.HOAS.Syn.Lang using (⟨_⟩; let′; _,_; case⊗_of_; reIx₀) renaming (module WithGates to WithGatesHOAS)
open WithGatesHOAS B4 using (λh)
open import Lambda1.HOAS.Sim.Base using () renaming (module WithGateSim to WithGateSimSurface); open WithGateSimSurface B4gS using (⦅_⦆S)

open import Samples.BoolTrioComb using (_and_; _or_; _xor_)
\end{code}



%<*ha>
\AgdaTarget{ha}
\begin{code}
ha : (a b : λh Bι) → λh ({-s-}Bι ⊗ {-c-}Bι)
ha a b = (a xor b) , (a and b)
\end{code}
%</ha>

%<*Ha>
\AgdaTarget{Ha}
\begin{code}
Ha : λb ({-a-}Bι ◅ {-b-}Bι ◅ Γ) ({-s-}Bι ⊗ {-c-}Bι)
Ha {Γ} = ha (reIx₀ (Bι ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Ha>


%<*ha-denote>
\AgdaTarget{ha-denote}
\begin{code}
ha-denote : (a b : El Bι) → El ({-s-}Bι ⊗ {-c-}Bι)
ha-denote a b = ((a ∧ ¬ b) ∨ (¬ a ∧ b)) ,† (a ∧ b)
\end{code}
%</ha-denote>

%<*ha-spec>
\AgdaTarget{ha-spec}
\begin{code}
ha-spec : (a b : El Bι) → El ({-s-}Bι ⊗ {-c-}Bι)
ha-spec a b = (a ⊻ b) ,† (a ∧ b)
\end{code}
%</ha-spec>

%<*xor-not-and-or>
\AgdaTarget{xor-not-and-or}
\begin{code}
xor-not-and-or : ∀ a b → (a ∧ ¬ b) ∨ (¬ a ∧ b) ≡ a ⊻ b
xor-not-and-or false  _ = refl
xor-not-and-or true   b = (proj₂ ∨-identity) (¬ b)
\end{code}
%</xor-not-and-or>

%<*ha-correct>
\AgdaTarget{ha-correct}
\begin{code}
ha-correct : ∀ {a b : λh Bι} ⦃ _ : St a ⦄ ⦃ _ : St b ⦄ → ⦅ ha a b ⦆S ≡ ha-spec ⦅ a ⦆S ⦅ b ⦆S
ha-correct {a} {b} =  begin
                        ⦅ ha a b ⦆S              ≡⟨⟩  -- by denotation of ha
                        ha-denote ⦅ a ⦆S ⦅ b ⦆S  ≡⟨ cong (_,† ⦅ a ⦆S ∧ ⦅ b ⦆S) (xor-not-and-or ⦅ a ⦆S ⦅ b ⦆S) ⟩
                        ha-spec ⦅ a ⦆S ⦅ b ⦆S
                      ∎
\end{code}
%</ha-correct>




%<*fa>
\AgdaTarget{fa}
\begin{code}
fa : (ci a b : λh Bι) → λh ({-s-}Bι ⊗ {-co-}Bι)
fa ci a b =  case⊗ (ha a b) of λ sab cab →
               case⊗ (ha ci sab) of λ s cabc →
                 s , (cabc or cab)
\end{code}
%</fa>

%<*Fa>
\AgdaTarget{Fa}
\begin{code}
Fa : λb ({-ci-}Bι ◅ {-a-}Bι ◅ {-b-}Bι ◅ Γ) ({-s-}Bι ⊗ {-co-}Bι)
Fa {Γ} = fa (reIx₀ (Bι ◅ Bι ◅ Γ)) (reIx₀ (Bι ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Fa>

%<*FaN>
\AgdaTarget{FaN}
\begin{code}
FaN : λb ({-ci-}Bι ◅ {-a-} vec Bι n ◅ {-b-}vec Bι n ◅ Γ) ({-s-}Bι ⊗ {-co-}vec Bι n)
FaN = MapAccL2-par′ Fa
\end{code}
%</FaN>

%<*testFaN>
\AgdaTarget{testFaN}
\begin{code}
testFaN : Set
testFaN =  ⦅ FaN ⦆Si  (↦ 𝟎  ◅ ↦ (𝟏 ∷ 𝟎 ∷ [])
                            ◅ ↦ (𝟏 ∷ 𝟏 ∷ []) ◅  ε)
             ≡        (𝟏  ,†  (𝟎 ∷ 𝟏 ∷ []))
  where  𝟎 = false
         𝟏 = true
\end{code}
%</testFaN>
