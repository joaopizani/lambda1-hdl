\begin{code}
module Samples.SharingAndLooping where

open import Data.Bool.Base using (_∨_; _∧_)
open import Data.Star using (_◅_)
open import Lambda1.Core.Syn.UTypes using (Γ)

open import Lambda1.Core.Syn.Lang using () renaming (module WithGates to WithGatesCore)
open import Lambda1.Gates.Bool using (B4) renaming (module Sim to B4Sim);  open B4Sim using (Bι)
open WithGatesCore B4 using (λb)

open import Lambda1.HOAS.Syn.Lang using (let′; reIx₀; loop; loop-out-next; fork) renaming (module WithGates to WithGatesHOAS)
open WithGatesHOAS B4 using (λh)

open import Samples.BoolTrioComb using (_and_; _or_; if†_then_else_)
\end{code}



%<*sh1>
\AgdaTarget{sh1}
\begin{code}
sh1 : (x y : λh 𝔹) → λh 𝔹
sh1 x y =  let′  z ≔ x and y
           in′   z or z
\end{code}
%</sh1>

%<*Sh1>
\AgdaTarget{Sh1}
\begin{code}
Sh1 : λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Sh1 {Γ} = sh1 (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%<*Sh1>


%<*sh2>
\AgdaTarget{sh2}
\begin{code}
sh2 : (x y z : λh 𝔹) → λh 𝔹
sh2 x y z =  let′  w ≔ x and y
             in′   w and z
\end{code}
%</sh2>

%<*Sh2>
\AgdaTarget{Sh2}
\begin{code}
Sh₂ : λb (𝔹 ◅ 𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Sh₂ {Γ} = sh2 (reIx₀ (𝔹 ◅ 𝔹 ◅ Γ)) (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Sh2>


%<*shift>
\AgdaTarget{shift}
\begin{code}
shift : (i : λh 𝔹) → λh 𝔹
shift i = loop l type l ∶ 𝔹  out l
                             next i
\end{code}
%</shift>

%<*Shift>
\AgdaTarget{Shift}
\begin{code}
Shift : λb (𝔹 ◅ Γ) 𝔹
Shift {Γ} = shift (reIx₀ Γ)
\end{code}
%</Shift>


%<*reg>
\AgdaTarget{reg}
\begin{code}
reg : (d s : λh 𝔹) → λh 𝔹
reg d s = loop (λ s x → fork (if† s then d else x)) s
\end{code}
%</reg>

%<*Reg>
\AgdaTarget{Reg}
\begin{code}
Reg : λb ({-d-}𝔹 ◅ {-s-}𝔹 ◅ Γ) 𝔹
Reg {Γ} = reg (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Reg>
