\begin{code}
module Samples.FinNatRegs where

open import Function using (_$_; _∘′_)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_; ε)

open import Notation.Base using (n; h; Fin′)
open import Data.SimpleTypes using (U; B; Γ; Δ; τ; τ₁; τ₂; τ₃; 𝟙; _⊗_; _⊕_; R; I; O; ix₁; sucn-ary-𝟙⊕; _⊇_; module EnvExUImp); open EnvExUImp using (⊇ε)
open import Lambda1.Gates.Regs using (regWritePort; regAddr; regOut)
open import Lambda1.Gates.FinNatRegs using (ℕₙ; ADD; MUL; REGFILE; 𝐍)
open import Lambda1.Lambda1 using (λB[_]; ⟨_⟩; [_]; Let; Loop; _，_; Case⊗_of_; Case⊕_of_or_; #₀; #₁; #₂; K; ↶Case⊕; ↶Let; ↶Case⊗; ↷Case⊗)
\end{code}


%<*regfile>
\AgdaTarget{regfile}
\begin{code}
regfile : λB[ 𝐍 h ] (regWritePort h _ ◅ regAddr h ◅ regAddr h ◅ Γ) (regOut ℕₙ)
regfile = ⟨ REGFILE ⟩ {I $ I $ I $ ⊇ε}

↶regfile : Δ ⊇ Γ  → (τ₁ ◅ τ₂ ◅ τ₃ ◅ Δ) ⊇ Γ
↶regfile = O ∘′ O ∘′ O
\end{code}
%</regfile>


%<*aluCmd>
\AgdaTarget{aluCmd}
\begin{code}
aluCmd : U B
aluCmd = sucn-ary-𝟙⊕ 1
\end{code}
%</aluCmd>

%<*aluOut>
\AgdaTarget{aluOut}
\begin{code}
aluOut : U (Fin′ n)
aluOut = ℕₙ
\end{code}
%</aluOut>

%<*addₙ>
\AgdaTarget{addₙ}
\begin{code}
addₙ : λB[ 𝐍 h ] (ℕₙ ◅ ℕₙ ◅ Γ) ℕₙ
addₙ = ⟨ ADD ⟩ {I $ I $ ⊇ε}
\end{code}
%</addₙ>

%<*mulₙ>
\AgdaTarget{mulₙ}
\begin{code}
mulₙ : λB[ 𝐍 h ] (ℕₙ ◅ ℕₙ ◅ Γ) ℕₙ
mulₙ = ⟨ MUL ⟩ {I $ I $ ⊇ε}
\end{code}
%</mulₙ>

%<*addAlu,mulAlu>
\AgdaTarget{addAlu,mulAlu}
\begin{code}
addAlu mulAlu : λB[ 𝐍 h ] (ℕₙ ◅ ℕₙ ◅ Γ) aluOut
addAlu = addₙ
mulAlu = mulₙ
\end{code}
%</addAlu,mulAlu>


%<*alu>
\AgdaTarget{alu}
\begin{code}
alu : λB[ 𝐍 h ] (ℕₙ ◅ ℕₙ ◅ aluCmd ◅ Γ) aluOut
alu = Case⊕ #₂{-aluCmd-} of K addAlu {↶Case⊕ R} or K mulAlu {↶Case⊕ R}
\end{code}
%</alu>


%<*shamComb>
\AgdaTarget{shamComb}
\begin{code}
shamComb :  λB[ 𝐍 h ]  (     {-regfile-}regWritePort h _ ◅ regAddr h ◅ regAddr h
                          ◅  aluCmd
                          ◅  {-ir-}regAddr h
                          ◅  Γ)
                       (regWritePort h _ ⊗ aluOut)

shamComb =  Case⊗ regfile of
              Let (K alu {↷Case⊗ $ ↶regfile $ R})
                  (K #₁ {↶Let $ ↶Case⊗ $ ↶regfile $ R} ， #₀) ， #₀

\end{code}
%</shamComb>


%<*sham>
\AgdaTarget{sham}
\begin{code}
sham :  λB[ 𝐍 h ]  (     {-ia-}regAddr h ◅ {-ib-}regAddr h
                      ◅  aluCmd
                      ◅  {-ir-}regAddr h
                      ◅  Γ)
                   aluOut
sham {h} = Loop {σ = regWritePort h _} shamComb
\end{code}
%</sham>
