\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Samples.BoolTrioComb where

open import Function.Base using (flip; _$_)
open import Data.Vec.Base using (foldl; replicate)
open import Data.Unit.Base using (tt)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Star.Decoration using (↦)
open import Data.Bool.Base using (Bool; true; false; _∧_; _∨_; if_then_else_) renaming (not to ¬_; _xor_ to _⊻_)
open import Data.Nat.Base using (ℕ)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; module ≡-Reasoning)
open ≡-Reasoning

open import Lambda1.Core.Notation using (n; it)
open import Lambda1.Core.Syn.UTypes using (Γ; Δ₁; Δ₂; vec; R; O; I; ⊇ε)
open import Lambda1.Core.Sem.UTypes using (Env)
open import Lambda1.Core.Syn.State using (St; sK₁; module St; sCon; s⟨_⟩; sLet; _s﹩_); open St
open import Lambda1.Core.Sem.UTypes using (Val; specEnv₁; specEnv₂; specEnv₃)
open import Lambda1.Gates.Bool using (NOT; AND; OR; MUX2; XOR; B4) renaming (module Sim to B4Sim)
open import Lambda1.Core.Syn.Lang using (λB[_]; Con; _﹩_ ; K₁; #₀; #₁ ; #₂) renaming (module WithGates to WithGatesCore)
open import Lambda1.Core.Sim.Base using (⟦_⊨_⟧S)
open B4Sim using (𝔹; B4gS)
open WithGatesCore B4 using (λb)

open import Lambda1.Utils.Patterns.Base using (Foldl-par; sFoldl-par; ZipWith-par′)
open import Lambda1.HOAS.Syn.Lang using (⟨_⟩; reIx₀) renaming (module WithGates to WithGatesHOAS)
open import Lambda1.HOAS.Syn.State using (sReIx₀; s◅)
open WithGatesHOAS B4 using (λh)
open import Lambda1.HOAS.Sim.Base using (⦅_⊨_⦆S) renaming (module WithGateSim to WithGateSimHOAS)
\end{code}



%<*not>
\AgdaTarget{not}
\begin{code}
\end{code}
not : (x : λh 𝔹) → λh 𝔹
not x = ⟨ NOT ⟩ {I $ ⊇ε} ﹩ x
%</not>


%<*not-correct>
\AgdaTarget{not-correct}
\begin{code}
\end{code}
not-correct : ∀ {x : λh 𝔹} ⦃ sx : St x ⦄
              → ⦅ B4gS ⊨ not x ⦆S it ≡ ¬ ⦅ B4gS ⊨ x ⦆S sx
not-correct = refl
%</not-correct>


%<*Not>
\AgdaTarget{Not}
\begin{code}
Not : λb (𝔹 ◅ Γ) 𝔹
Not = ⟨ NOT ⟩ {I $ ⊇ε}
\end{code}
%</Not>


%<*Not-correct>
\AgdaTarget{Not-correct}
\begin{code}
Not-correct : (δ : Env Val (𝔹 ◅ Γ)) → ⟦ B4gS ⊨ Not ⟧S it δ ≡ (specEnv₁ 𝔹 ¬_) δ
Not-correct (↦ _ ◅ _) = refl
\end{code}
%</Not-correct>


%<*and>
\AgdaTarget{and}
\begin{code}
\end{code}
and : (x y : λh 𝔹) → λh 𝔹
and x y = ⟨ AND ⟩ {I $ I $ ⊇ε} ﹩ x ﹩ y
%</and>


%<*and-correct>
\AgdaTarget{and-correct}
\begin{code}
\end{code}
postulate and-correct : {x : λh 𝔹} {y : λh 𝔹} ⦃ sx : St x ⦄ ⦃ sy : St y ⦄
                        → ⦅ B4gS ⊨ and x y ⦆S (sLet sy (sLet (s◅ {c = x} ⦃ sx ⦄)  s⟨ tt ⟩)) ≡ _∧_ (⦅ B4gS ⊨ x ⦆S sx) (⦅ B4gS ⊨ y ⦆S sy)
%</and-correct>


%<*and>
\AgdaTarget{And}
\begin{code}
And : λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
And = ⟨ AND ⟩ {I $ I $ ⊇ε}
\end{code}
%</And>

%<*And-correct>
\AgdaTarget{And-correct}
\begin{code}
And-correct : (δ : Env Val (𝔹 ◅ 𝔹 ◅ Γ)) → ⟦ B4gS ⊨ And ⟧S it δ ≡ (specEnv₂ 𝔹 _∧_) δ
And-correct (↦ _ ◅ ↦ _ ◅ _) = refl
\end{code}
%</And-correct>



%<*or>
\AgdaTarget{or}
\begin{code}
\end{code}
or : (x y : λh 𝔹) → λh 𝔹
or x y = ⟨ OR ⟩ {I $ I $ ⊇ε} ﹩ x ﹩ y
%</or>


%<*or-correct>
\AgdaTarget{or-correct}
\begin{code}
\end{code}
or-correct : ∀ {x y : λh 𝔹} ⦃ sx : St x ⦄ ⦃ sy : St y ⦄
             → ⦅ ⟨ OR ⟩ {I $ I $ ⊇ε} ﹩ x ﹩ y ⦆S (sLet sy (sLet (doit sx) s⟨ tt ⟩)) ≡ _∨_ (⦅ x ⦆S sx) (⦅ y ⦆S sy)
or-correct = refl
%</or-correct>


%<*Or>
\AgdaTarget{Or}
\begin{code}
Or : λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Or = ⟨ OR ⟩ {I $ I $ ⊇ε}
\end{code}
%</Or>


%<*Or-correct>
\AgdaTarget{Or-correct}
\begin{code}
Or-correct : (δ : Env Val (𝔹 ◅ 𝔹 ◅ Γ)) → ⟦ B4gS ⊨ Or ⟧S it δ ≡ (specEnv₂ 𝔹 _∨_) δ
Or-correct (↦ _ ◅ ↦ _ ◅ _) = refl
\end{code}
%</Or-correct>



%<*mux2>
\AgdaTarget{mux2}
\begin{code}
\end{code}
mux2 : ∀ (s : λh 𝔹) (a b : λh 𝔹) → λh 𝔹
mux2 s a b = ⟨ MUX2 ⟩ {I $ I $ I $ ⊇ε} ﹩ s ﹩ a ﹩ b
%</mux2>

%<*mux2-correct>
\AgdaTarget{mux2-correct}
\begin{code}
\end{code}
postulate mux2-correct : ∀ {c : λh 𝔹} {a b : λh 𝔹} ⦃ sc : St c ⦄ ⦃ sa : St a ⦄ ⦃ sb : St b ⦄
                         → ⦅ B4gS ⊨ mux2 c a b ⦆S {!!} ≡ mux2-spec (⦅ B4gS ⊨ c ⦆S sc) (⦅ B4gS ⊨ a ⦆S sa) (⦅ B4gS ⊨ b ⦆S sb)
%</mux2-correct>


%<*Mux2>
\AgdaTarget{Mux2}
\begin{code}
Mux2 : λb (𝔹{-s-} ◅ 𝔹{-a-} ◅ 𝔹{-b-} ◅ Γ) 𝔹
Mux2 = ⟨ MUX2 ⟩ {I $ I $ I $ ⊇ε}
\end{code}
%</Mux2>


%<*flipITE>
\AgdaTarget{flipITE}
\begin{code}
flipITE : ∀ {α : Set} (c : Val 𝔹) (a b : α) → α
flipITE c = flip (if_then_else_ c)
\end{code}
%</flipITE>

%<*Mux2-correct>
\AgdaTarget{Mux2-correct}
\begin{code}
Mux2-correct : (δ : Env Val (𝔹 ◅ 𝔹 ◅ 𝔹 ◅ Γ)) → ⟦ B4gS ⊨ Mux2 ⟧S it δ ≡ (specEnv₃ 𝔹 flipITE) δ
Mux2-correct (↦ _ ◅ ↦ _ ◅ ↦ _ ◅ _) = refl
\end{code}
%</Mux2-correct>



%<*xor>
\AgdaTarget{\_xor\_}
\begin{code}
\end{code}
xor : (x y : λh 𝔹) → λh 𝔹
xor x y = or (and x (not y)) (and (not x) y)
%</xor>


%<*Xor>
\AgdaTarget{Xor}
\begin{code}
Xor : λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Xor = ⟨ XOR ⟩ {I $ I $ ⊇ε}
\end{code}
%</Xor>


%<*Xor-correct>
\AgdaTarget{Xor-correct}
\begin{code}
Xor-correct : (δ : Env Val (𝔹 ◅ 𝔹 ◅ Γ)) → ⟦ B4gS ⊨ Xor ⟧S it δ ≡ (specEnv₂ 𝔹 _⊻_) δ
Xor-correct (↦ _ ◅ ↦ _ ◅ _) = refl
\end{code}
%</Xor-correct>



%<*andN>
\AgdaTarget{andN}
\begin{code}
\end{code}
andN : ∀ {n} (xs : λh (vec 𝔹 n)) → λh 𝔹
andN = foldl-par and (Con true)
%</andN>


%<*andN-correct>
\AgdaTarget{andN-correct}
\begin{code}
\end{code}
postulate andN-correct : (xs : λh (vec 𝔹 n)) (ss : St xs) → ⦅ B4gS ⊨ andN {n} xs ⦆S (sAndN xs ss) ≡ andN-spec {n} (⦅ B4gS ⊨ xs ⦆S ss)
%</andN-correct>


%<*AndN>
\AgdaTarget{AndN}
\begin{code}
AndN : λb (vec 𝔹 n ◅ Γ) 𝔹
AndN = Foldl-par And ﹩ Con true
\end{code}
%</AndN>
AndN : λb (vec 𝔹 n ◅ Γ) 𝔹
AndN {n} {Γ} = andN {n} (reIx₀ Γ)


%<*specAndN>
\AgdaTarget{specAndN}
\begin{code}
specAndN : ∀ {n} → Val (vec 𝔹 n) → Val 𝔹
specAndN = foldl _ _∧_ true
\end{code}
%</specAndN>


%<*sAndN>
\AgdaTarget{sAndN}
\begin{code}
sAndN : St (AndN {n} {Γ})
sAndN = sFoldl-par (replicate (%⟨⟩ {p = I $ I $ ⊇ε})) s﹩ sCon
\end{code}
%</sAndN>


%<*AndN-correct>
\AgdaTarget{AndN-correct}
\begin{code}
postulate AndN-correct : (δ : Env Val (vec 𝔹 n ◅ Γ)) → ⟦ B4gS ⊨ AndN ⟧S sAndN δ ≡ specEnv₁ 𝔹 specAndN δ
\end{code}
%</AndN-correct>





\begin{code}
Mux-bool : ∀ {Γ} -> λb ( 𝔹 {- s -} ◅ 𝔹 {- a -} ◅  𝔹 {- b -} ◅ Γ ) 𝔹
Mux-bool = ⟨ OR ⟩ {I $ I $ ⊇ε}  ﹩  (⟨ AND ⟩ {I $ I $ ⊇ε} ﹩ #₁ ﹩ (⟨ NOT ⟩ {I $ ⊇ε} ﹩ #₀ ))
                                ﹩  (⟨ AND ⟩ {I $ I $ ⊇ε} ﹩ #₂ ﹩ #₀ )
\end{code}

Elementwise multiplexing
\begin{code}
Mux-vect-par : ∀ {Γ n} -> λb (𝔹 ◅ vec 𝔹 n ◅ vec 𝔹 n ◅ Γ) (vec 𝔹 n)
Mux-vect-par = K₁ (ZipWith-par′ f)
  where f = Mux-bool ﹩  #₀
\end{code}

postulate externalize : ∀ {Γ n τ} → λ₁′ Γ (vec⊗ n τ) → Vec (λ₁ τ) n
postulate internalize : ∀ {Γ n τ} → Vec (λ₁′ Γ τ) n → λ₁′ Γ (vec⊗ n τ)

sorter : ∀ {n} {τ} (_cmp_ : λ₁ τ → λ₁ τ → λ₁ (τ ⊗ τ)) (x₀ : λ₁ τ) (xs : Vec (λ₁ τ) n) → λ₁ (τ ⊗ vec⊗ n τ)
sorter _      x₀ []          {_} = x₀ ， one
sorter _cmp_  x₀ (x₁ ∷ xs′)  {Γ} = mapAccumL _cmp_ x₀ (externalize {Γ} (sorter _cmp_ x₁ xs′))
