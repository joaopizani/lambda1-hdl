\begin{code}
module Lambda1.Gates.Nat where

open import Function using (const)
open import Data.Nat.Base using (ℕ; _+_; _*_)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_; ε)
open import Data.Star.Decoration using (↦)

open import Lambda1.Core.Syn.UTypes using (U; Ctx; Γ; ι; 𝟙)
open import Lambda1.Core.Syn.Gates using (Gates);  open Gates

open import Lambda1.Core.Sem.UTypes using (Env; Val)
open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sem.Lang using (idState)
\end{code}


%<*NATIx>
\AgdaTarget{NATIx}
\begin{code}
data NATIx : Set where ADD MUL : NATIx
\end{code}
%</NATIx>


%<*NAT>
\AgdaTarget{NAT}
\begin{code}
NAT : Gates ℕ
NAT .gIx   = NATIx
NAT .gCtx  = const (ι ◅ ι ◅ ε)
NAT .gSt   = const 𝟙
NAT .gOut  = const ι
\end{code}
%</NAT>



\begin{code}
module Sim where
\end{code}

%<*Nat-base>
\AgdaTarget{Natι}
\begin{code}
 Natι : U ℕ
 Natι = ι
\end{code}
%</Nat-base>

%<*add-mul>
\AgdaTarget{add,mul}
\begin{code}
 add mul : Env Val (Natι ◅ Natι ◅ Γ) → Val Natι
 add (↦ x ◅ ↦ y ◅ _) = _+_ x y
 mul (↦ x ◅ ↦ y ◅ _) = _*_ x y
\end{code}
%</add-mul>

%<*NATgS>
\AgdaTarget{NATgS}
\begin{code}
 NATgS : gS NAT
 NATgS ADD = idState add
 NATgS MUL = idState mul
\end{code}
%</NATgS>
