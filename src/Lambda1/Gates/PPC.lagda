\begin{code}
module Lambda1.Gates.PPC where

open import Function using (const)
open import Data.Vec.Base using (Vec; []; _∷_; zipWith; replicate)
open import Data.Nat.Base using (ℕ; suc; zero)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε; _◅_)
open import Data.Star.Decoration using (↦)

open import Lambda1.Core.Notation using (α; n)
open import Lambda1.Core.Syn.UTypes using (B; U; Ctx; ι; 𝟙; vec)
open import Lambda1.Core.Syn.Gates using (Gates);  open Gates

open import Lambda1.Core.Sem.UTypes using (Env; Val)
open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sem.Lang using (idState)
\end{code}


%<*PPCHIIx>
\AgdaTarget{PPCHIIx}
\begin{code}
data PPCHIIx : Set where FAN : (n : ℕ) → PPCHIIx
\end{code}
%</PPCHIIx>

%<*PPCHICtx>
\AgdaTarget{PPCHICtx}
\begin{code}
PPCHICtx : PPCHIIx → Ctx B
PPCHICtx (FAN n) = vec ι n ◅ ε
\end{code}
%</PPCHICtx>

%<*PPCHIOut>
\AgdaTarget{PPCHIOut}
\begin{code}
PPCHIOut : PPCHIIx → U B
PPCHIOut (FAN n) = vec ι n
\end{code}
%</PPCHIOut>

%<*PPCHI>
\AgdaTarget{PPCHI}
\begin{code}
PPCHI : Gates B
PPCHI .gIx   = PPCHIIx
PPCHI .gOut  = PPCHIOut
PPCHI .gCtx  = PPCHICtx
PPCHI .gSt   = const 𝟙
\end{code}
%</PPCHI>



%<*PPCLOIx>
\AgdaTarget{PPCLOIx}
\begin{code}
data PPCLOIx : Set where
  REPLICATE  : (n : ℕ) →  PPCLOIx
  BINOP      :            PPCLOIx
\end{code}
%</PPCLOIx>

%<*PPCLOCtx>
\AgdaTarget{PPCLOCtx}
\begin{code}
PPCLOCtx : PPCLOIx → Ctx B
PPCLOCtx BINOP          = ι ◅ ι ◅ ε
PPCLOCtx (REPLICATE n)  = ι ◅ ε
\end{code}
%</PPCLOCtx>

%<*PPCLOOut>
\AgdaTarget{PPCLOOut}
\begin{code}
PPCLOOut : PPCLOIx → U B
PPCLOOut BINOP          = ι
PPCLOOut (REPLICATE n)  = vec ι n
\end{code}
%</PPCLOOut>

%<*PPCLO>
\AgdaTarget{PPCLO}
\begin{code}
PPCLO : Gates B
PPCLO .gIx   = PPCLOIx
PPCLO .gCtx  = PPCLOCtx
PPCLO .gSt   = const 𝟙
PPCLO .gOut  = PPCLOOut 
\end{code}
%</PPCLO>



\begin{code}
module Sim where
\end{code}

%<*fanN>
\AgdaTarget{fanN}
\begin{code}
 fanN : (_·_ : α → α → α) → (Vec α n → Vec α n)
 fanN {n = zero}    _·_ []         = []
 fanN {n = suc n′}  _·_ (x ∷ xs′)  = x ∷ zipWith _·_ (replicate n′ x) xs′
\end{code}
%</fanN>

%<*fanNCombHI>
\AgdaTarget{fanNCombHI}
\begin{code}
 fanNCombHI : (_·_ : B → B → B) → (Env Val (vec ι n ◅ ε) → Val (vec ι n))
 fanNCombHI _·_ (↦ ins ◅ ε) = fanN _·_ ins
\end{code}
%</fanNCombHI>

%<*PPCHISem>
\AgdaTarget{PPCHISem}
\begin{code}
 PPCHISem : (_·_ : B → B → B) → gS PPCHI
 PPCHISem _·_ (FAN n) = idState (fanNCombHI _·_)
\end{code}
%</PPCHISem>


%<*replicateLO>
\AgdaTarget{replicateLO}
\begin{code}
 replicateCombLO : Env Val (ι ◅ ε) → Val {B} (vec ι n)
 replicateCombLO {n = n} (↦ x ◅ ε) = replicate n x
\end{code}
%</replicateLO>

%<*binopLO>
\AgdaTarget{binopLO}
\begin{code}
 binopCombLO : (_·_ : B → B → B) → (Env Val (ι ◅ ι ◅ ε) → Val ι)
 binopCombLO _·_ (↦ x ◅ ↦ y ◅ ε) = x · y
\end{code}
%</binopLO>

%<*PPCLOSem>
\AgdaTarget{PPCLOSem}
\begin{code}
 PPCLOSem : (_·_ : B → B → B) → gS {B = B} PPCLO
 PPCLOSem _    (REPLICATE n)  = idState replicateCombLO
 PPCLOSem _·_  BINOP          = idState (binopCombLO _·_)
\end{code}
%</PPCLOSem>
