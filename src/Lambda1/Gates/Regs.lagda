\begin{code}
module Lambda1.Gates.Regs where

open import Function using (const)
open import Data.Unit.Base using (⊤)
open import Data.Product using (_×_; _,_)
open import Data.Nat.Base using (ℕ; zero; suc)
open import Data.Fin.Base using (Fin; _↑ˡ_) renaming (zero to Fz; suc to Fs)
open import Data.Sum.Base using (inj₁; inj₂)
open import Data.Vec.Base using (_[_]≔_; lookup)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε; _◅_)
open import Data.Star.Decoration using (↦)

open import Lambda1.Core.Syn.UTypes using (B; U; sucn-ary-𝟙⊕; _⊗_; vec; Ctx)
open import Lambda1.Core.Syn.Gates using (Gates); open Gates

open import Lambda1.Core.Sem.UTypes using (Env; Val)
open import Lambda1.Core.Sem.Gates using (gS)
\end{code}


%<*regAddr>
\AgdaTarget{regAddr}
\begin{code}
regAddr : (h : ℕ) → U B
regAddr = sucn-ary-𝟙⊕
\end{code}
%</regAddr>

%<*regWritePort>
\AgdaTarget{regWritePort}
\begin{code}
regWritePort : (h : ℕ) (τ : U B) → U B
regWritePort h τ = regAddr h ⊗ τ
\end{code}
%</regWritePort>


%<*REGSCtx>
\AgdaTarget{REGSCtx}
\begin{code}
REGSCtx : (h : ℕ) (τ : U B) → Ctx B
REGSCtx h τ = regWritePort h τ ◅ regAddr h ◅ regAddr h ◅ ε
\end{code}
%</REGSCtx>

%<*REGSSt>
\AgdaTarget{REGSSt}
\begin{code}
REGSSt : (h : ℕ) (τ : U B) → U B
REGSSt h τ = vec τ (suc h)
\end{code}
%</REGSSt>

%<*REGSOut>
\AgdaTarget{REGSOut}
\begin{code}
REGSOut : (τ : U B) → U B
REGSOut τ = τ ⊗ τ
\end{code}
%</REGSOut>

%<*REGS>
\AgdaTarget{REGS}
\begin{code}
REGS : (h : ℕ) (τ : U B) → Gates B
REGS _ _  .gIx  = ⊤
REGS h τ  .gCtx = const (REGSCtx h τ)
REGS h τ  .gSt  = const (REGSSt h τ)
REGS _ τ  .gOut = const (REGSOut τ)
\end{code}
%</REGS>



\begin{code}
module Sim where
\end{code}

%<*addr2Fin>
\AgdaTarget{addr2Fin}
\begin{code}
 addr2Fin : ∀ h (a : Val {B} (regAddr h)) → Fin (suc h)
 addr2Fin zero      _          = Fz
 addr2Fin (suc m′)  (inj₁ _)   = Fz ↑ˡ m′
 addr2Fin (suc _)   (inj₂ y)   = Fs (addr2Fin _ y)
\end{code}
%</addr2Fin>


%<*regs>
\AgdaTarget{regs}
\begin{code}
 regs : ∀ h (τ : U B) → Val (REGSSt h τ) → Env Val (REGSCtx h τ) → Val (REGSSt h τ) × Val (REGSOut τ)
 regs h _ s (↦ (w , d) ◅ ↦ x ◅ ↦ y ◅ ε) =  s [ addr2Fin h w ]≔ d  ,  ( lookup s (addr2Fin h x) , lookup s (addr2Fin h y) )
\end{code}
%</regs>


%<*REGSgS>
\AgdaTarget{REGSgS}
\begin{code}
 REGSgS : ∀ h (τ : U B) → gS (REGS h τ)
 REGSgS h τ _tt = regs h τ
\end{code}
%</REGSgS>
