\begin{code}
module Lambda1.Gates.Bool where

open import Function using (const)
open import Data.Bool.Base using (Bool; not; _∧_; _∨_; _xor_; if_then_else_)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_; ε)
open import Data.Star.Decoration using (↦)

open import Lambda1.Core.Syn.UTypes using (U; Ctx; Γ; ι; 𝟙)
open import Lambda1.Core.Syn.Gates using (Gates);  open Gates

open import Lambda1.Core.Sem.UTypes using (Env; Val)
open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sem.Lang using (idState)
\end{code}


%<*B4Ix>
\AgdaTarget{B4Ix}
\begin{code}
data B4Ix : Set where NOT AND OR XOR MUX2 : B4Ix
\end{code}
%</B4Ix>

%<*B4Ctx>
\AgdaTarget{B4Ctx}
\begin{code}
B4Ctx : B4Ix → Ctx Bool
B4Ctx NOT   = ι ◅ ε
B4Ctx AND   = ι ◅ ι ◅ ε
B4Ctx OR    = ι ◅ ι ◅ ε
B4Ctx XOR   = ι ◅ ι ◅ ε
B4Ctx MUX2  = ι ◅ ι ◅ ι ◅ ε
\end{code}
%</B4Ctx>


%<*B4>
\AgdaTarget{B4}
\begin{code}
B4 : Gates Bool
B4 .gIx   = B4Ix
B4 .gCtx  = B4Ctx
B4 .gSt   = const 𝟙
B4 .gOut  = const ι
\end{code}
%</B4>



\begin{code}
module Sim where
\end{code}

%<*Bool-base>
\begin{code}
 𝔹 : U Bool
 𝔹 = ι
\end{code}
%</Bool-base>

%<*not1>
\AgdaTarget{not1Comb}
\begin{code}
 not1Comb : Env Val (𝔹 ◅ Γ) → Val 𝔹
 not1Comb (↦ x ◅ _)  = not x
\end{code}
%</not1>

%<*and2-or2-xor2>
\AgdaTarget{and2Comb,or2Comb,xor2Comb}
\begin{code}
 and2Comb  or2Comb  xor2Comb : Env Val (𝔹 ◅ 𝔹 ◅ Γ) → Val 𝔹
 and2Comb  (↦ x ◅ ↦ y ◅ _) = _∧_ x y
 or2Comb   (↦ x ◅ ↦ y ◅ _) = _∨_ x y
 xor2Comb  (↦ x ◅ ↦ y ◅ _) = _xor_ x y
\end{code}
%</and2-or2-xor2>

%<*mux2>
\AgdaTarget{mux2}
\begin{code}
 mux2Comb : Env Val (𝔹 ◅ 𝔹 ◅ 𝔹 ◅ Γ) → Val 𝔹
 mux2Comb (↦ c ◅ ↦ x ◅ ↦ y ◅ _) = if_then_else_ c y x
\end{code}
%</mux2>

%<*B4gS>
\AgdaTarget{B4gS}
\begin{code}
 B4gS : gS B4
 B4gS NOT   = idState not1Comb
 B4gS AND   = idState and2Comb
 B4gS OR    = idState or2Comb
 B4gS XOR   = idState xor2Comb
 B4gS MUX2  = idState mux2Comb
\end{code}
%</B4gS>
