\begin{code}
module Lambda1.Gates.FinRegs where

open import Function using (const)
open import Data.Nat.Base using (ℕ; zero; suc; _+_; _*_)
open import Data.Nat.DivMod using (_mod_)
open import Data.Fin.Base using (Fin; toℕ)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε; _◅_)
open import Data.Star.Decoration using (↦)

open import Lambda1.Core.Notation using (n)
open import Lambda1.Core.Syn.UTypes using (B; U; ι; 𝟙; Ctx)
open import Lambda1.Core.Syn.Gates using (Gates; _⊞_); open Gates

open import Lambda1.Core.Sem.UTypes using (Env; Val)
open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sem.Lang using (idState)

open import Lambda1.Gates.Regs using (REGS)
\end{code}


%<*FINIx>
\AgdaTarget{FINIx}
\begin{code}
data FINIx : Set where ADD MUL : FINIx
\end{code}
%</FINIx>

%<*FIN>
\AgdaTarget{FIN}
\begin{code}
FIN : Gates (Fin (suc n))
FIN .gIx   = FINIx
FIN .gCtx  = const (ι ◅ ι ◅ ε)
FIN .gSt   = const 𝟙
FIN .gOut  = const ι
\end{code}
%</FIN>


%<*FINREGS>
\AgdaTarget{FINREGS}
\begin{code}
FINREGS : (h : ℕ) → Gates (Fin (suc n))
FINREGS h = FIN ⊞ REGS h ι
\end{code}
%</FINREGS>



\begin{code}
module Sim where
\end{code}

%<*add-mul-finmod>
\AgdaTarget{\_+′\_,\_*′\_}
\begin{code}
 _+′_ _*′_ : Fin (suc n) → Fin (suc n) → Fin (suc n)
 _+′_ {n} x y = (toℕ x + toℕ y) mod (suc n)
 _*′_ {n} x y = (toℕ x * toℕ y) mod (suc n)
\end{code}
%</add-mul-finmod>

%<*Fin-base>
\AgdaTarget{Finι}
\begin{code}
 Finι : U (Fin (suc n))
 Finι = ι
\end{code}
%</Fin-base>

%<*fin-add-mul>
\AgdaTarget{add,mul}
\begin{code}
 add mul : Env (Val {B = Fin (suc n)}) (Finι ◅ Finι ◅ ε) → Val Finι
 add (↦ x ◅ ↦ y ◅ ε) = _+′_ x y
 mul (↦ x ◅ ↦ y ◅ ε) = _*′_ x y
\end{code}
%</fin-add-mul>

%<*FINgS>
\AgdaTarget{FINgS}
\begin{code}
 FINgS : ∀ {n} → gS (FIN {n})
 FINgS ADD = idState add
 FINgS MUL = idState mul
\end{code}
%</FINgS>
