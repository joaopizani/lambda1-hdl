\begin{code}
module Lambda1.Utils.Patterns.Properties where

open import Function using (_∘′_) renaming (_⟨_⟩_ to _≺_≻_)
open import Data.Product using (_×_; _,_)
open import Data.Product.Properties.WithK using (,-injective)
open import Data.Sum using (inj₂; inj₁)
open import Data.Vec using (Vec; []; _∷_; replicate; map)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Star.Decoration using (↦)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; trans; cong₂)

open import Data.MapAccum using (mapAccum²; transformF)
open import Lambda1.Core.Syn.UTypes using (B; _⊗_; vec; U; σ; ρ; τ; Ctx; Γ)
open import Lambda1.Core.Syn.Lang using (λB[_]; Unit; Con; Loop; MapAccL-par; ⟨_⟩)
open import Lambda1.Core.Syn.State using (St; s⟨_⟩; sCon; sUnit; sLoop; sMapAccL-par)
open import Lambda1.Core.Sem.UTypes using (Val)
open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sim.Base using (CtxS; TyS; ⟦_⊨_⟧s; ⟦_⊨_⟧S; ⟦_⊨_⟧n)

open import Lambda1.Core.Syn.Gates using (Gates; G)
\end{code}



-- Ignoring the second parameter of sMapAccL-seq in the pattern means we don't care about the state of f itself
%<*gets0>
\AgdaTarget{gets₀}
\begin{code}
gets₀ : {f : λB[ G ] (σ ◅ Γ) (σ ⊗ τ)} (sm : St (Loop f)) → TyS σ
gets₀ (sLoop s₀ _) = s₀
\end{code}
%</gets0>


%<*par-seq-conv>
\AgdaTarget{\_≛\_}
\begin{code}
_≛_ :  ∀ {n} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} {e : λB[ G ] Γ σ} {xs : λB[ G ] Γ (vec ρ n)}
       (π₁ : St (MapAccL-par f e xs) × TyS (σ ⊗ vec τ n)) (π₂ : St (Loop f) × TyS (vec τ n)) → Set
_≛_ (_ , s′ , xs′) (sm″ , xs″) = (s′ ≡ gets₀ sm″) × (xs′ ≡ xs″)
\end{code}
%</par-seq-conv>


%<*state-free>
\AgdaTarget{state-free}
\begin{code}
state-free : (S : gS G) → (λB[ G ] Γ τ → Set)
state-free S c = ∀ {γ} (sa sb : St c) → ⟦ S ⊨ c ⟧s sa γ ≡ ⟦ S ⊨ c ⟧s sb γ
\end{code}
%</state-free>


\begin{code}
⟦_⊨_⟧s₂ : (S : gS G) (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (s : St f) (γ : CtxS Γ) (i : TyS σ) (x : TyS ρ) → (St f × TyS σ × TyS τ)
⟦ S ⊨ f ⟧s₂ s γ i x = ⟦ S ⊨ f ⟧s s (↦ i ◅ ↦ x ◅ γ)
\end{code}

%<*state-free-initstates>
\AgdaTarget{state-free-initstates}
\begin{code}
state-free-initstates :  ∀  {n G} (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) {e : TyS σ} (xs : TyS (vec ρ n))
                            (S : gS G) (sfas sfbs : Vec (St f) n) {γ : CtxS Γ} (p : state-free S f)
                            → mapAccum² (transformF ⟦ S ⊨ f ⟧s₂ γ) e sfas xs ≡ mapAccum² (transformF ⟦ S ⊨ f ⟧s₂ γ) e sfbs xs
state-free-initstates f {_} []         _  []             []             {_}  _  = refl
state-free-initstates f {e} (x ∷ xs′)  S  (sfa ∷ sfas′)  (sfb ∷ sfbs′)  {γ}  p  =
  let  ih:s , ih:ys = ,-injective (state-free-initstates f {e} xs′ S sfas′ sfbs′ {γ} p)
  in   ⊥
  where postulate ⊥ : _
\end{code}
%</state-free-initstates>


%<*sMapAccL-par†>
\AgdaTarget{sMapAccL-par†}
\begin{code}
sMapAccL-par† : ∀ {n} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} (sfs : Vec (St f) n) {s : TyS σ} {xs : TyS (vec ρ n)} → St (MapAccL-par f (Con s) (Con xs))
sMapAccL-par† sfs {s} {xs} = sMapAccL-par sfs sCon sCon
\end{code}
%</sMapAccL-par†>



%<*MapAccL-par-seq>
\AgdaTarget{MapAccL-par-seq}
\begin{code}
postulate 
 MapAccL-par-seq :  ∀  {n B} {G : Gates B} {σ ρ τ : U B} {Γ : Ctx B}
                       (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (S : gS G)
                       (sf : St f) (s : TyS σ) (xs : Vec (TyS ρ) n) (γ : CtxS Γ) (p : state-free S f)
   →     ⟦ S ⊨ MapAccL-par f (Con s) (Con xs)  ⟧s    (sMapAccL-par† (replicate n sf))  γ
      ≛  ⟦ S ⊨ Loop f                          ⟧n n  (sLoop s sf)                      (map ((_◅ γ) ∘′ ↦) xs)
\end{code}
%</MapAccL-par-seq>

MapAccL-par-seq f _    _    _   []         _ _ = refl , refl
MapAccL-par-seq f S  sf₀  s₀  (x ∷ xs′)  γ p =
  let  s₁ , sf₁ , y = (transformF ⟦ S ⊨ f ⟧s₂ γ) s₀ sf₀ x
       ih:s , ih:ys = MapAccL-par-seq f S sf₁ s₁ xs′ γ p
       lem = state-free-initstates f xs′ S (replicate sf₀) (replicate sf₁) p

       s≡   = {!!}  --                   (cong proj₁             lem)  ≺ trans ≻  {!!}
       ys≡  = {!!}  -- cong₂ _∷_ refl (  (cong (proj₂ ∘′ proj₂)  lem)  ≺ trans ≻  {!!})
  in s≡ , ys≡



%<*MapAccL-par-seq′>
\AgdaTarget{MapAccL-par-seq′}
\begin{code}
\end{code}
%</MapAccL-par-seq′>
postulate
 MapAccL-par-seq′ : ∀ {n} (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (sf : λs f) (s : TyS σ) (xs : Vec (TyS ρ) n) (γ : CtxS Γ) (p : state-free f)
   →     ⟦ S ⊨ MapAccL-par′  f      ⟧s′[ sMapAccL-par′     (replicate sf) ]     (↦ s ◅ ↦ xs ◅ γ)
      ≛  ⟦ S ⊨ MapAccL-seq   (K f)  ⟧n′[ sMapAccumL-seq s  (sK sf) ]         n  (vec-envs xs γ)
