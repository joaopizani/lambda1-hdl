\begin{code}
module Lambda1.Utils.Patterns.Base where

open import Function using (_∘′_; _$_)
open import Data.Unit.Base using (tt)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Vec using (Vec; replicate; map; zipWith)
open import Data.Nat.Base using (zero; suc)

open import Lambda1.Core.Syn.UTypes using (B; 𝟙; _⊗_; vec; τ; σ; ρ; ν; Ctx; Γ; _⊇_);  open _⊇_
open import Lambda1.Core.Syn.Gates using (G)
open import Lambda1.Core.Syn.Lang using (λB[_]; Unit; Loop; _，_; MapAccL-par; MapAccL2-par; _﹩_; #₀; #₁; #₂; K; K₁; K₂)
open import Lambda1.Core.Syn.State using (St; sUnit; sLoop; _s，_; sMapAccL-par; _s﹩_; sK′; s#₀; s#₁; sK₁)

open import Lambda1.Core.Sem.UTypes using (Env; Val)

open import Lambda1.Utils.BasicCombinators using (Units; sUnits; Fst; sFst; Snd; sSnd)
\end{code}



%<*MapAccL-par′>
\AgdaTarget{MapAccL-par′}
\begin{code}
MapAccL-par′ : ∀ {n} (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) (σ ⊗ vec τ n)
MapAccL-par′ f = MapAccL-par (K wk f) #₀ #₁
  where wk = I $ I $ O $ O $ R
\end{code}
%</MapAccL-par′>

%<*MapAccL2-par′>
\AgdaTarget{MapAccL2-par′}
\begin{code}
MapAccL2-par′ : ∀ {n} (f : λB[ G ] (σ ◅ ρ ◅ τ ◅ Γ) (σ ⊗ ν)) → λB[ G ] (σ ◅ vec ρ n ◅ vec τ n ◅ Γ) (σ ⊗ vec ν n)
MapAccL2-par′ f = MapAccL2-par (K wk f) #₀ #₁ #₂
  where wk = I $ I $ I $ O $ O $ O $ R
\end{code}
%</MapAccL2-par′>


%<*sMapAccL-par′>
\AgdaTarget{sMapAccL-par′}
\begin{code}
sMapAccL-par′ : ∀ {n} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} (sfs : Vec (St f) n) → St (MapAccL-par′ {n = n} f)
sMapAccL-par′ sfs = sMapAccL-par (map (λ s → sK′ s) sfs) s#₀ s#₁
\end{code}
%</sMapAccL-par′>

%<*sMapAccL-par-rep′>
\AgdaTarget{sMapAccL-par-rep′}
\begin{code}
sMapAccL-par-rep′ : ∀ {n} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} (sf : St f) → St (MapAccL-par′ {n = n} f)
sMapAccL-par-rep′ {n = n} = sMapAccL-par′ ∘′ replicate n
\end{code}
%</sMapAccL-par-rep′>



%<*Mapper>
\AgdaTarget{Mapper}
\begin{code}
Mapper : (f : λB[ G ] (ρ ◅ Γ) τ) → λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)
Mapper f = #₀ ， (K₁ f)
\end{code}
%</Mapper>

%<*sMapper>
\AgdaTarget{sMapper}
\begin{code}
sMapper : {f : λB[ G ] (ρ ◅ Γ) τ} (sf : St f) → St  (Mapper {σ = σ} f)
sMapper sf = s#₀ s， (sK₁ sf)
\end{code}
%</sMapper>


%<*Zipper>
\AgdaTarget{Zipper}
\begin{code}
Zipper : λB[ G ] (σ ◅ ρ ◅ τ ◅ Γ) (σ ⊗ (ρ ⊗ τ))
Zipper = Mapper (#₀ ， #₁)
\end{code}
%</Zipper>


ZipperWith has the same code as Mapper with the difference being it types signature.
%<*ZipperWith>
\AgdaTarget{ZipperWith}
\begin{code}
ZipperWith : (f : λB[ G ] (ρ ◅ τ ◅ Γ) σ) → λB[ G ] (𝟙 ◅ ρ ◅ τ ◅ Γ) (𝟙 ⊗ σ)
ZipperWith f = #₀ ， K₁ f
\end{code}
%</ZipperWith>


%<*Folder>
\AgdaTarget{folder}
\begin{code}
Folder : (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ σ)
Folder f = #₀ ， f
\end{code}
%</folder-deep>

%<*sFolder>
\AgdaTarget{sFolder}
\begin{code}
sFolder : {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : St f) → St (Folder {ρ = ρ} f)
sFolder sf = s#₀ s， sf
\end{code}
%</sFolder>




%<*Map-par>
\AgdaTarget{Map-par}
\begin{code}
Map-par : ∀ {n} (f : λB[ G ] (ρ ◅ Γ) τ) → λB[ G ] (vec ρ n ◅ Γ) (vec τ n)
Map-par f = Snd (MapAccL-par′ (Mapper f)  ﹩  Unit)
\end{code}
%</Map-par>

%<*sMap-par>
\AgdaTarget{sMap-par}
\begin{code}
sMap-par : ∀ {n} {f : λB[ G ] (ρ ◅ Γ) τ} (sfs : Vec (St f) n) → St (Map-par {n = n} f)
sMap-par sfs = sSnd (sMapAccL-par′ (map sMapper sfs)  s﹩  sUnit)
\end{code}
%</sMap-par>

%<*sMap-par-rep>
\AgdaTarget{sMap-par-rep}
\begin{code}
sMap-par-rep : ∀ {n} {f : λB[ G ] (ρ ◅ Γ) τ} (sf : St f) → St (Map-par {n = n} f)
sMap-par-rep {n = n} = sMap-par ∘′ replicate n
\end{code}
%</sMap-par-rep>


%<*Map-seq>
\AgdaTarget{Map-seq}
\begin{code}
Map-seq : (f : λB[ G ] (ρ ◅ Γ) τ) → λB[ G ] (ρ ◅ Γ) τ
Map-seq f = Loop {σ = 𝟙} (Mapper f)
\end{code}
%</Map-seq>
-- TODO: show that map-seq f ≋ f

%<*sMap-seq>
\AgdaTarget{sMap-seq}
\begin{code}
sMap-seq : {f : λB[ G ] (ρ ◅ Γ) τ} (sf : St f) → St (Map-seq f)
sMap-seq sf = sLoop tt (sMapper sf)
\end{code}
%</sMap-seq>






%<*Foldl-scanl-par>
\AgdaTarget{Foldl-scanl-par}
\begin{code}
Foldl-scanl-par : ∀ {n} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) (σ ⊗ vec σ n)
Foldl-scanl-par f = MapAccL-par′ (Folder f)
\end{code}
%</Foldl-scanl-par>

%<*sFoldl-scanl-par>
\AgdaTarget{sFoldl-scanl-par}
\begin{code}
sFoldl-scanl-par : ∀ {n} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sfs : Vec (St f) n) → St (Foldl-scanl-par {n = n} f)
sFoldl-scanl-par sfs = sMapAccL-par′ (map sFolder sfs)
\end{code}
%</sFoldl-scanl-par>

%<*sFoldl-scanl-par-rep>
\AgdaTarget{sFoldl-scanl-par-rep}
\begin{code}
sFoldl-scanl-par-rep : ∀ {n} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : St f) → St (Foldl-scanl-par {n = n} f)
sFoldl-scanl-par-rep {n = n} = sFoldl-scanl-par ∘′ replicate n
\end{code}
%</sFoldl-scanl-par-rep>


%<*Foldl-scanl-seq>
\AgdaTarget{Foldl-scanl-seq}
\begin{code}
Foldl-scanl-seq : ∀ {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (ρ ◅ Γ) σ
Foldl-scanl-seq f = Loop (Folder f)
\end{code}
%</Foldl-scanl-seq>

%<*sFoldl-scanl-seq>
\AgdaTarget{sFoldl-scanl-seq}
\begin{code}
sFoldl-scanl-seq : ∀ {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (s : Val σ) (sf : St f) → St (Foldl-scanl-seq f)
sFoldl-scanl-seq s sf = sLoop s (sFolder sf)
\end{code}
%</sFoldl-scanl-seq>


%<*Foldl-par>
\AgdaTarget{Foldl-par}
\begin{code}
Foldl-par : ∀ {n} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) σ
Foldl-par f = Fst (Foldl-scanl-par f)
\end{code}
%</Foldl-par>

%<*sFoldl-par>
\AgdaTarget{sFoldl-par}
\begin{code}
sFoldl-par : ∀ {n} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sfs : Vec (St f) n) → St (Foldl-par {n = n} f)
sFoldl-par sfs = sFst (sFoldl-scanl-par sfs)
\end{code}
%</sFoldl-par>

%<*sFoldl-par-rep>
\AgdaTarget{sFoldl-par-rep}
\begin{code}
sFoldl-par-rep : ∀ {n} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : St f) → St (Foldl-par {n = n} f)
sFoldl-par-rep {n = n} = sFoldl-par ∘′ replicate n
\end{code}
%</sFoldl-par-rep>


%<*Scanl-par>
\AgdaTarget{Scanl-par}
\begin{code}
Scanl-par : ∀ {n} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) (vec σ n)
Scanl-par f = Snd (Foldl-scanl-par f)
\end{code}
%</Scanl-par>

%<*sScanl-par>
\AgdaTarget{sScanl-par}
\begin{code}
sScanl-par : ∀ {n} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sfs : Vec (St f) n) → St (Scanl-par {n = n} f)
sScanl-par sfs = sSnd (sFoldl-scanl-par sfs)
\end{code}
%</sFoldl-par>

%<*sScanl-par-rep>
\AgdaTarget{sScanl-par-rep}
\begin{code}
sScanl-par-rep : ∀ {n} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : St f) → St (Scanl-par {n = n} f)
sScanl-par-rep {n = n} = sScanl-par ∘′ replicate n
\end{code}
%</sFoldl-par-rep>



%<*Foldl-seq-Scanl-seq>
\AgdaTarget{Foldl-seq, Scanl-seq}
\begin{code}
Foldl-seq Scanl-seq : ∀ {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (ρ ◅ Γ) σ
Foldl-seq = Foldl-scanl-seq
Scanl-seq = Foldl-scanl-seq
\end{code}
%</Foldl-seq-Scanl-seq>

%<*sFoldl-seq-sScanl-seq>
\AgdaTarget{sFoldl-seq, sScanl-seq}
\begin{code}
sFoldl-seq : ∀ {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (s : Val σ) (sf : St f) → St (Foldl-seq f)
sScanl-seq : ∀ {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (s : Val σ) (sf : St f) → St (Scanl-seq f)
sFoldl-seq = sFoldl-scanl-seq
sScanl-seq = sFoldl-scanl-seq
\end{code}
%</sFoldl-seq-sScanl-seq>



\begin{code}
ZipWith-seq′ : ∀ {ρ τ σ} (f : λB[ G ] (ρ ◅ τ ◅ Γ) σ) → λB[ G ] (ρ ◅ τ ◅ Γ) σ
ZipWith-seq′ f = f

ZipWith-par′ : ∀ {n} {ρ τ σ} (f : λB[ G ] (ρ ◅ τ ◅ Γ) σ) → λB[ G ] (vec ρ n ◅ vec τ n ◅ Γ) (vec σ n)
ZipWith-par′ f = Snd (MapAccL2-par′ (ZipperWith f) ﹩  Unit)

Zip-seq′ : ∀ {ρ τ} → λB[ G ] (ρ ◅ τ ◅ Γ) (ρ ⊗ τ)
Zip-seq′ = ZipWith-seq′   (#₀ ， #₁)

Zip-par′ : ∀ {n} {ρ τ} → λB[ G ] (vec ρ n ◅ vec τ n ◅ Γ) (vec (ρ ⊗ τ) n)
Zip-par′ = ZipWith-par′  (#₀ ， #₁)
\end{code}
