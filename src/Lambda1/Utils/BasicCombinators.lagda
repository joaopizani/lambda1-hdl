\begin{code}
module Lambda1.Utils.BasicCombinators where

open import Data.Nat.Base using (zero; suc)

open import Lambda1.Core.Syn.UTypes using (B; 𝟙; vec; _⊗_; τ; υ; ν; _⊇_; R; Γ)
open import Lambda1.Core.Syn.Gates using (G)
open import Lambda1.Core.Syn.Lang using (λB[_]; K; ⟨_⟩; Unit; Nil; Cons; Case⊗_of_; #₀; #₁)
open import Lambda1.Core.Syn.State using (St; s⟨_⟩; sUnit; sCase⊗; sNil; sCons; s#₀; s#₁; sK)
\end{code}


%<*Replicate>
\AgdaTarget{Replicate}
\begin{code}
Replicate : ∀ (x : λB[ G ] Γ τ) {n} → λB[ G ] Γ (vec τ n)
Replicate _ {zero}   = Nil
Replicate x {suc _}  = Cons x (Replicate x)
\end{code}
%</Replicate>

%<*sReplicate>
\AgdaTarget{sReplicate}
\begin{code}
sReplicate : ∀ {x : λB[ G ] Γ τ} (s : St x) {n} → St (Replicate x {n})
sReplicate _ {zero}   = sNil
sReplicate s {suc _}  = sCons s (sReplicate s)
\end{code}
%</sReplicate>


%<*Units>
\AgdaTarget{Units}
\begin{code}
Units : ∀ {n} → λB[ G ] Γ (vec 𝟙 n)
Units = Replicate Unit
\end{code}
%</Units>

%<*sUnits>
\AgdaTarget{sUnits}
\begin{code}
sUnits : ∀ {n} → St {B} {G} {Γ} (Units {n = n})
sUnits {n = n} = sReplicate sUnit {n}
\end{code}
%</sUnits>


%<*Fst>
\AgdaTarget{Fst}
\begin{code}
Fst : (c : λB[ G ] Γ (υ ⊗ ν)) → λB[ G ] Γ υ
Fst c = Case⊗ c of #₀
\end{code}
%</Fst>

%<*sFst>
\AgdaTarget{sFst}
\begin{code}
sFst : {c : λB[ G ] Γ (υ ⊗ ν)} (s : St c) → St (Fst c)
sFst s = sCase⊗ s s#₀
\end{code}
%</sFst>


%<*Snd>
\AgdaTarget{Snd}
\begin{code}
Snd : (c : λB[ G ] Γ (υ ⊗ ν)) → λB[ G ] Γ ν
Snd c = Case⊗ c of #₁
\end{code}
%</Snd>

%<*sSnd>
\AgdaTarget{sSnd}
\begin{code}
sSnd : {c : λB[ G ] Γ (υ ⊗ ν)} (s : St c) → St (Snd c)
sSnd s = sCase⊗ s s#₁
\end{code}
%</sSnd>

