\begin{code}
module Lambda1.Core.Syn.UTypes where

open import Data.Nat.Base using (ℕ; zero; suc)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive renaming (map to mapCtx)

import Data.Star.Environment as Env
import Data.Star.Environment.Extra as EnvEx
import Data.Star.Environment.Extra.Properties as EnvExProps
\end{code}


%<*variables-base-types>
\AgdaTarget{B,C,B₁,B₂,C₁,C₂}
\begin{code}
variable B C  B₁ B₂ C₁ C₂ : Set
\end{code}
%</variables-base-types>


* First, we define inductively the collection of all possible types for circuit inputs/outputs
%<*type-universe>
\AgdaTarget{U}
\begin{code}
data U (B : Set) : Set where
  𝟙 ι       : U B
  _⊗_ _⊕_   : (τ υ : U B) → U B
  vec       : (τ : U B) (n : ℕ) → U B
\end{code}
%</type-universe>

\begin{code}
infixr 1 _⊕_
infixr 2 _⊗_
\end{code}


%<*variables-circ-types>
\AgdaTarget{τ,σ,ρ,ν,υ,ω,τ₀,τ₁,τ₂,τ₃,τ₄,σ₀,σ₁,σ₂,ρ₀,ρ₁,ρ₂,ν₀,ν₁,ν₂,υ₀,υ₁,υ₂,ω₀,ω₁,ω₂}
\begin{code}
variable τ σ ρ ν υ ω  τ₀ τ₁ τ₂ τ₃ τ₄ σ₀ σ₁ σ₂ ρ₀ ρ₁ ρ₂ ν₀ ν₁ ν₂ υ₀ υ₁ υ₂ ω₀ ω₁ ω₂ : U B
\end{code}
%</variables-circ-types>


%<*sucn-ary-unit>
\AgdaTarget{sucn-ary-𝟙⊕}
\begin{code}
sucn-ary-𝟙⊕ : (m : ℕ) → U B
sucn-ary-𝟙⊕ zero      = 𝟙
sucn-ary-𝟙⊕ (suc m′)  = 𝟙 ⊕ sucn-ary-𝟙⊕ m′
\end{code}
%</sucn-ary-unit>


%<*Option>
\AgdaTarget{\_？}
\begin{code}
_？ : (τ : U B) → U B
_？ τ = 𝟙 ⊕ τ
\end{code}
%</Option>



%<*fold-tau>
\AgdaTarget{foldτ}
\begin{code}
foldτ : {B : Set} {ℓ : _} {α : Set ℓ} (𝟙ₐ ιₐ : α) (_⊗ₐ_ _⊕ₐ_ : α → α → α) (vecₐ : α → ℕ → α) → (U B → α)
foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ  𝟙          = 𝟙ₐ
foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ  ι          = ιₐ
foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ  (φ ⊗ ω)    = (foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ φ) ⊗ₐ (foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ ω)
foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ  (φ ⊕ ω)    = (foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ φ) ⊕ₐ (foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ ω)
foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ  (vec σ n)  = vecₐ (foldτ 𝟙ₐ ιₐ _⊗ₐ_ _⊕ₐ_ vecₐ σ) n
\end{code}
%</fold-tau>


%<*down-tau>
\AgdaTarget{⇓τ}
\begin{code}
⇓τ : (ι′ : U C) (τ : U B) → U C
⇓τ ι′ = foldτ 𝟙 ι′ _⊗_ _⊕_ vec
\end{code}
%</down-tau>



* A Ctx is a reflexive transitive closure (sequence) of types [e.g. (𝟙 ◅ (σ ⊗ ρ) ◅ ε)]
* Re-exporting some stuff partially applied to circuit type universe for convenience
%<*modules-ctx>
\begin{code}
open module EnvUExp         (B : Set)    = Env (U B) using () renaming (Ctxt to Ctx) public
open module EnvUImpSyn      {B : Set}    = Env (U B) using (_∋_) renaming (vz to Z; vs to S) public

open module EnvExUImp          {B : Set}    = EnvEx (U B) using (ix₀; ix₁; ix₂; ix₃; ix₄; _⊇_; R; O; I; ⊇ε; Kix; comp⊇; Env⊇; reIx₀∋) public
open module EnvExPropsUImpMap  {B C : Set}  = EnvExProps.MapProps (U B) (U C) using (map-∋; map-⊇) public

open EnvExProps using (Env⊇-comp⊇) public
\end{code}
%</modules-ctx>


\begin{code}
variable Γ Δ Θ  Γ₁ Γ₂ Δ₁ Δ₂ Θ₁ Θ₂ : Ctx B
\end{code}


%<*refineCtx>
\AgdaTarget{⇓Γ}
\begin{code}
⇓Γ : (ι′ : U C) (Γ : Ctx B) → Ctx C
⇓Γ ι′ = mapCtx (⇓τ ι′)
\end{code}
%</refineCtx>
