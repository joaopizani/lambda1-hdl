\begin{code}
module Lambda1.Core.Syn.Lang where

open import Function using (_$_; _∘′_)
open import Data.Unit.Base using (tt)
open import Function using (_∘′_; flip; const)
open import Data.Nat.Base using (ℕ; zero; suc)
open import Data.Sum using (inj₁; inj₂)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)

open import Data.Vec using (Vec; []; _∷_)

open import Lambda1.Core.Notation using (n; α)
open import Lambda1.Core.Syn.UTypes using (B; U; Ctx; Z; S; ix₀; ix₁; ix₂; ix₃; ix₄; _∋_; _⊇_; R; O; I
                                          ; Kix; comp⊇; τ; τ₀; τ₁; τ₂; τ₃; τ₄; σ; ρ; υ; ν; ω; Γ; Δ);  open U
open import Lambda1.Core.Syn.Gates using (Gates; _⊞_; G; G₁; G₂);  open Gates

open import Lambda1.Core.Sem.UTypes using (Val)
\end{code}



\begin{code}
infixr 20  _，_
\end{code}

* λB is a "first-order" simply-typed lambda calculus with De Bruijn indices for vars.
 + Main differences from STLC:
   - MAINLY: No arrow types, no abs, no app.
       + REASON: All elements of λB should have a reasonably direct synthesis scheme. Higher-order terms would not.
   - Lets for sharing
   - Dedicated constructor for feedback loops (recursion)
   - A form of "application" can be emulated with lets. Circuits can really only be "applied" to variables
       + Anything else let-bound.
%<*LambdaB-basic>
\AgdaTarget{λB[\_]}
\begin{code}
data λB[_] (G : Gates B) : (Γ : Ctx B) (τ : U B) → Set where
  ⟨_⟩    : (g : gIx G)  {p : Δ ⊇ gCtx G g}   → λB[ G ] Δ (gOut G g)

  [_]    : (i : Γ ∋ τ)                       → λB[ G ] Γ τ

  Let    : (x : λB[ G ] Γ ρ)  (f : λB[ G ] (ρ ◅ Γ) τ)
                        → λB[ G ] Γ τ

  Loop   : (f : λB[ G ] (σ ◅ Γ) (σ ⊗ τ))
                   → λB[ G ] Γ τ
\end{code}
%</LambdaB-basic>

%<*Lambda1-unit-val>
\begin{code}
  Unit  :                λB[ G ] Γ 𝟙
  Con   : (v : Val τ) →  λB[ G ] Γ τ
\end{code}
%</Lambda1-unit-val>

%<*LambdaB-prod-coprod>
\begin{code}
  _，_           : (x : λB[ G ] Γ υ)  (y : λB[ G ] Γ ν)                    → λB[ G ] Γ (υ ⊗ ν)
  Case⊗_of_     : (xy : λB[ G ] Γ (υ ⊗ ν))  (f : λB[ G ] (υ ◅ ν ◅ Γ) τ)    → λB[ G ] Γ τ

  Inl           : (x : λB[ G ] Γ υ)                                                           → λB[ G ] Γ (υ ⊕ ν)
  Inr           : (y : λB[ G ] Γ ν)                                                           → λB[ G ] Γ (υ ⊕ ν)
  Case⊕_of_or_  : (xy : λB[ G ] Γ (υ ⊕ ν))  (f : λB[ G ] (υ ◅ Γ) τ)  (g : λB[ G ] (ν ◅ Γ) τ)  → λB[ G ] Γ τ
\end{code}
%</LambdaB-prod-coprod>

%<*LambdaB-vec>
\begin{code}
  Nil          :                                                 λB[ G ] Γ (vec τ zero)
  Cons         : (x : λB[ G ] Γ τ) (xs : λB[ G ] Γ (vec τ n)) →  λB[ G ] Γ (vec τ (suc n))

  Head         : (xs : λB[ G ] Γ (vec τ (suc n)))  → λB[ G ] Γ τ
  Tail         : (xs : λB[ G ] Γ (vec τ (suc n)))  → λB[ G ] Γ (vec τ n)
\end{code}
%</LambdaB-vec>




%<*Loop-out-next>
\AgdaTarget{Loop-out-next}
\begin{code}
Loop-out-next : (o : λB[ G ] (σ ◅ Γ) τ) (n : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] Γ τ
Loop-out-next o n = Loop (n ， o)
\end{code}
%</Loop-out-next>


\begin{code}
infixl 90 _﹩_
\end{code}$

%<*app-flipped-let>
\AgdaTarget{\_﹩\_}
\begin{code}
_﹩_ : (f : λB[ G ] (ρ ◅ Γ) τ) (x : λB[ G ] Γ ρ) → λB[ G ] Γ τ
_﹩_ = flip Let
\end{code}
%<*app-flipped-let>


%<*jump-binders>
\AgdaTarget{↶Let,↷Let,↶Loop,↶Case⊗,↶Case⊕}
\begin{code}
↶Let : Δ ⊇ Γ  → (ρ ◅ Δ) ⊇  Γ
↷Let : Δ ⊇ Γ  → (ρ ◅ Δ) ⊇ (ρ ◅ Γ)

↶Loop : Δ ⊇ Γ  → (ρ ◅ Δ) ⊇ Γ
↷Loop : Δ ⊇ Γ  → (ρ ◅ Δ) ⊇ (ρ ◅ Γ)

↶Case⊕ : Δ ⊇ Γ  → (υ ◅ Δ) ⊇  Γ
↷Case⊕ : Δ ⊇ Γ  → (υ ◅ Δ) ⊇ (υ ◅ Γ)

↶Case⊗ : Δ ⊇ Γ  → (υ ◅ ν ◅ Δ) ⊇  Γ
↷Case⊗ : Δ ⊇ Γ  → (υ ◅ ν ◅ Δ) ⊇ (υ ◅ ν ◅ Γ)

↶Let = O
↷Let = I

↶Loop = O
↷Loop = I

↶Case⊕ = O
↷Case⊕ = I

↶Case⊗ = O ∘′ O
↷Case⊗ = I ∘′ I
\end{code}
%</jump-binders>



%<*var-circs>
\AgdaTarget{\#₀,\#₁,\#₂,\#₃,\#₄}
\begin{code}
#₀ : λB[ G ] (τ₀                      ◅ Γ) τ₀
#₁ : λB[ G ] (τ₀ ◅ τ₁                 ◅ Γ) τ₁
#₂ : λB[ G ] (τ₀ ◅ τ₁ ◅ τ₂            ◅ Γ) τ₂
#₃ : λB[ G ] (τ₀ ◅ τ₁ ◅ τ₂ ◅ τ₃       ◅ Γ) τ₃
#₄ : λB[ G ] (τ₀ ◅ τ₁ ◅ τ₂ ◅ τ₃ ◅ τ₄  ◅ Γ) τ₄
#₀ = [ ix₀ ];  #₁ = [ ix₁ ];  #₂ = [ ix₂ ];  #₃ = [ ix₃ ];  #₄ = [ ix₄ ]
\end{code}
%</var-circs>


* inj₁⊞ and inj₂⊞ can be written as folds, can K also?
\begin{code}
foldλB : {T : Ctx B → U B → Set}
         (𝐠 : {Γ : Ctx B}                    (g : gIx G) (wk : Γ ⊇ gCtx G g)                         → T Γ (gOut G g))
         (𝐢 : {Γ : Ctx B} {τ : U B}          (i : Γ ∋ τ)                                             → T Γ τ)
         (𝐞 : {Γ : Ctx B} {ρ τ : U B}        (x : T Γ ρ) (b : T (ρ ◅ Γ) τ)                           → T Γ τ)
         (𝐨 : {Γ : Ctx B} {σ τ : U B}        (f : T (σ ◅ Γ) (σ ⊗ τ))                                 → T Γ τ)
         (𝐮 : {Γ : Ctx B}                                                                            → T Γ 𝟙)
         (𝐯 : {Γ : Ctx B} {τ : U B}          (v : Val τ)                                             → T Γ τ)
         (𝐱 : {Γ : Ctx B} {σ ρ : U B}        (x : T Γ σ) (y : T Γ ρ)                                 → T Γ (σ ⊗ ρ))
         (𝐩 : {Γ : Ctx B} {σ ρ τ : U B}      (xy : T Γ (σ ⊗ ρ)) (f : T (σ ◅ ρ ◅ Γ) τ)                → T Γ τ)
         (𝐥 : {Γ : Ctx B} {σ ρ : U B}        (x : T Γ σ)                                             → T Γ (σ ⊕ ρ))
         (𝐫 : {Γ : Ctx B} {σ ρ : U B}        (y : T Γ ρ)                                             → T Γ (σ ⊕ ρ))
         (𝐬 : {Γ : Ctx B} {σ ρ τ : U B}      (xy : T Γ (σ ⊕ ρ)) (f : T (σ ◅ Γ) τ) (g : T (ρ ◅ Γ) τ)  → T Γ τ)
         (𝐧 : {Γ : Ctx B} {τ : U B}                                                                  →  T Γ (vec τ zero))
         (𝐜 : {Γ : Ctx B} {τ : U B} {n : ℕ}  (x : T Γ τ) (xs : T Γ (vec τ n))                        →  T Γ (vec τ (suc n)))
         (𝐡 : {Γ : Ctx B} {τ : U B} {n : ℕ}  (xs : T Γ (vec τ (suc n)))                              → T Γ τ)
         (𝐭 : {Γ : Ctx B} {τ : U B} {n : ℕ}  (xs : T Γ (vec τ (suc n)))                              → T Γ (vec τ n))
         (c : λB[ G ] Γ τ) → T Γ τ
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (⟨_⟩ g {wk})      = 𝐠 g wk
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 ([_] i)           = 𝐢 i
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Let x f)         = 𝐞 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 x) (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 f)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Loop f)          = 𝐨 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 f)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 Unit              = 𝐮
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Con v)           = 𝐯 v
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (_，_ x y)        = 𝐱 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 x) (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 y)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Case⊗_of_ xy f)  = 𝐩 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 xy) (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 f)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 Nil               = 𝐧
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Cons x xs)       = 𝐜 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 x) (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 xs)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Head xs)         = 𝐡 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 xs)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Tail xs)         = 𝐭 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 xs)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Inl x)           = 𝐥 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 x)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Inr y)           = 𝐫 (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 y)
foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 (Case⊕_of_or_ xy f g) = 𝐬  (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 xy) (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 f)
                                                            (foldλB 𝐠 𝐢 𝐞 𝐨 𝐮 𝐯 𝐱 𝐩 𝐥 𝐫 𝐬 𝐧 𝐜 𝐡 𝐭 g)
\end{code}


%<*K>
\AgdaTarget{K}
\begin{code}
K : (p : Δ ⊇ Γ) (c : λB[ G ] Γ τ) → λB[ G ] Δ τ
K p (⟨_⟩ g {wk})          = ⟨_⟩ g {comp⊇ wk p}
K p ([_] i)               = [_] (Kix p i)
K p (Let x f)             = Let (K p x) (K (I p) f)
K p (Loop f)              = Loop (K (I p) f)
K p Unit                  = Unit
K p (Con v)               = Con v
K p (_，_ x y)            = _，_ (K p x) (K p y)
K p (Case⊗ xy of f)       = Case⊗ (K p xy) of (K (I $ I p) f)
K p (Inl x)               = Inl (K p x)
K p (Inr y)               = Inr (K p y)
K p (Case⊕ xy of f or g)  = Case⊕ (K p xy) of (K (I p) f) or (K (I p) g)
K p Nil                   = Nil
K p (Cons x xs)           = Cons (K p x) (K p xs)
K p (Head xs)             = Head (K p xs)
K p (Tail xs)             = Tail (K p xs)
\end{code}
%</K>


%<*K-nums>
\AgdaTarget{K₁,K₂,K₃,K₄}
\begin{code}
K₁ : (c : λB[ G ] Γ τ) → λB[ G ] (τ₁                 ◅ Γ) τ
K₂ : (c : λB[ G ] Γ τ) → λB[ G ] (τ₁ ◅ τ₂            ◅ Γ) τ
K₃ : (c : λB[ G ] Γ τ) → λB[ G ] (τ₁ ◅ τ₂ ◅ τ₃       ◅ Γ) τ
K₄ : (c : λB[ G ] Γ τ) → λB[ G ] (τ₁ ◅ τ₂ ◅ τ₃ ◅ τ₄  ◅ Γ) τ

K₁ = K (O R)
K₂ = K₁ ∘′ K₁;  K₃ = K₁ ∘′ K₂;  K₄ = K₁ ∘′ K₃
\end{code}
%</K-nums>





%<*Vec-elim>
\AgdaTarget{Vec-elim}
\begin{code}
Vec-elim : (P : ℕ → U B) (f : ∀ {m} →  λB[ G ] (ρ ◅ P m ◅ Γ) (P (suc m))) (e : λB[ G ] Γ (P zero)) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ (P n)
Vec-elim {n = zero}  P _ e _   = e
Vec-elim {n = suc _} P f e xs  = f ﹩ Head (K₁ xs) ﹩ Vec-elim P f e (Tail xs)
\end{code}
%</Vec-elim>


%<*Foldr-par>
\AgdaTarget{Foldr-par}
\begin{code}
Foldr-par : (f : λB[ G ] (ρ ◅ σ ◅ Γ) σ) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ σ
Foldr-par {σ = σ} f e xs = Vec-elim (const σ) f e xs
\end{code}
%</Foldr-par>

%<*Foldl-par>
\AgdaTarget{Foldl-par}
\begin{code}
Foldl-par : (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ σ
Foldl-par {n = zero}   _ e _   = e
Foldl-par {n = suc _}  f e xs  = Foldl-par f (f ﹩ K₁ e ﹩ Head xs) (Tail xs)
\end{code}
%</Foldl-par>


%<*MapAccL-par>
\AgdaTarget{MapAccL-par}
\begin{code}
MapAccL-par : (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ (σ ⊗ vec τ n)
MapAccL-par {n = zero}   _ e _  = e ， Nil
MapAccL-par {n = suc _}  f e xs =  let wk = I $ I $ O $ O $ R in
                                   Case⊗ f ﹩ K₁ e ﹩ (Head xs) of  {-e′ y′-}
                                     Case⊗ MapAccL-par (K wk f) #₀{-e′-} (Tail (K₂ xs)) of  {-e″ ys′-}
                                       #₀{-e″-} ， Cons #₃{-y′-} #₁{-ys′-}
\end{code}
%</MapAccL-par>

%<*MapAccL2-par>
\AgdaTarget{MapAccL2-par}
\begin{code}
MapAccL2-par : (f : λB[ G ] (σ ◅ ρ ◅ τ ◅ Γ) (σ ⊗ υ)) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) (ys : λB[ G ] Γ (vec τ n)) → λB[ G ] Γ (σ ⊗ vec υ n)
MapAccL2-par {n = zero}    _ e _ _  = e ， Nil
MapAccL2-par {n = suc _}  f e xs ys =  let wk = I $ I $ I $ O $ O $ R in
                                       Case⊗ f ﹩ K₂ e ﹩ K₁ (Head xs) ﹩ Head ys of
                                         Case⊗ MapAccL2-par (K wk f) #₀ (Tail (K₂ xs)) (Tail (K₂ ys)) of
                                           #₀ ， Cons #₃ #₁
\end{code}
%</MapAccL2-par>



%<*inj₁⊞>
\AgdaTarget{inj₁⊞}
\begin{code}
inj₁⊞ : (c : λB[ G₁ ] Γ τ) → λB[ G₁ ⊞ G₂ ] Γ τ
inj₁⊞ = foldλB  (λ g wk → ⟨ inj₁ g ⟩ {wk})
                [_] Let Loop
                Unit Con
                _，_ Case⊗_of_
                Inl Inr Case⊕_of_or_
                Nil Cons Head Tail
\end{code}
%</inj₁⊞>

%<*inj₂⊞>
\AgdaTarget{inj₂⊞}
\begin{code}
inj₂⊞ : (c : λB[ G₂ ] Γ τ) → λB[ G₁ ⊞ G₂ ] Γ τ
inj₂⊞ = foldλB  (λ g wk → ⟨ inj₂ g ⟩ {wk})
                [_] Let Loop
                Unit Con
                _，_ Case⊗_of_
                Inl Inr Case⊕_of_or_
                Nil Cons Head Tail
\end{code}
%</inj₂⊞>




\begin{code}
module WithGates {B} (G : Gates B) where
\end{code}


\begin{code}
 λb : (Γ : Ctx B) (τ : U B) → Set
 λb Γ τ = λB[ G ] Γ τ
\end{code}
