\begin{code}
module Lambda1.Core.Syn.State where

open import Function using (flip; _∘′_; _$_)
open import Data.Nat.Base using (suc)
open import Data.Vec using (Vec; []; _∷_; map; replicate)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Sum using (inj₂)

open import Lambda1.Core.Notation using (ℓ; n)
open import Lambda1.Core.Syn.UTypes using (B; U; Ctx; _∋_; ix₀; ix₁; ix₂; ix₃; ix₄; _⊇_; Kix
                                          ; Γ; Δ; τ; τ₀; τ₁; τ₂; τ₃; τ₄; σ; ρ; υ; ν);  open U; open _⊇_
open import Lambda1.Core.Syn.Gates using (Gates; _⊞_; G; G₁; G₂);  open Gates
open import Lambda1.Core.Syn.Lang using (λB[_]; _﹩_; #₀; #₁; #₂; #₃; #₄; K; K₁; K₂; K₃; K₄; MapAccL-par; inj₁⊞; inj₂⊞; foldλB); open λB[_]

open import Lambda1.Core.Sem.UTypes using (Val; def)
\end{code}


-- The core state datatype constructor are to be used as part of instance search, so that "obvious" states
-- can be built for circuits (those for "specific" circuits, and not for circuit combinators).
%<*Lambda1-state>
\AgdaTarget{St,%⟨⟩,%[],%Let,%Unit,%Con,%，,%Case⊗,%Inl,%Inr,%Case⊕,%Nil,%Cons,%Head,%Tail}
\begin{code}
data St {G : Gates B} : (c : λB[ G ] Γ τ) → Set where
  %Loop           : {f : λB[ G ] (σ ◅ Γ) (σ ⊗ τ)}                          (si : Val σ) ⦃ sf : St f ⦄      → St (Loop f)

  instance %⟨⟩    : {g : gIx G} {sg : Val (gSt G g)} {p : Δ ⊇ gCtx G g}                                    → St {τ = gOut G g} (⟨ g ⟩ {p})
  instance %[]    : {i : Γ ∋ σ}                                                                            → St [ i ]
  instance %Let   : {x : λB[ G ] Γ σ} {f : λB[ G ] (σ ◅ Γ) τ}              ⦃ sx : St x ⦄ ⦃ sf : St f ⦄     → St (Let x f)

  instance %Unit  :                                                                                           St {Γ = Γ} Unit
  instance %Con   : {v : Val τ}                                                                            →  St {Γ = Γ} {τ} (Con v)

  instance %，     : {x : λB[ G ] Γ υ} {y : λB[ G ] Γ ν}                   ⦃ sx : St x ⦄    ⦃ sy : St y ⦄  → St (x ， y)
  instance %Case⊗  : {xy : λB[ G ] Γ (σ ⊗ ρ)} {f : λB[ G ] (σ ◅ ρ ◅ Γ) τ}  ⦃ sxy : St xy ⦄  ⦃ sf : St f ⦄  → St (Case⊗ xy of f)

  instance %Inl    : {x : λB[ G ] Γ υ}                                     ⦃ sx : St x ⦄                   → St (Inl {ν = ν} x)
  instance %Inr    : {y : λB[ G ] Γ ν}                                     ⦃ sy : St y ⦄                   → St (Inr {υ = υ} y)
  instance %Case⊕  : {xy : λB[ G ] Γ (σ ⊕ ρ)} {f : λB[ G ] (σ ◅ Γ) τ} {g : λB[ G ] (ρ ◅ Γ) τ}
                                             ⦃ sxy : St xy ⦄ ⦃ sf : St f ⦄ ⦃ sg : St g ⦄                   → St (Case⊕ xy of f or g)

  instance %Nil     :                                                                                         St {Γ = Γ} (Nil {τ = τ})
  instance %Cons    : {x : λB[ G ] Γ τ} {xs : λB[ G ] Γ (vec τ n)}         ⦃ sx : St x ⦄ ⦃ sxs : St xs ⦄   →  St (Cons x xs)
  instance %Head    : {xs : λB[ G ] Γ (vec τ (suc n))}                     ⦃ sxs : St xs ⦄                 →  St (Head xs)
  instance %Tail    : {xs : λB[ G ] Γ (vec τ (suc n))}                     ⦃ sxs : St xs ⦄                 →  St (Tail xs)
\end{code}
%</Lambda1-state>


<*Lambda1-state-explicit>
\AgdaTarget{s⟨_⟩,s[_],sLet,sLoop,sUnit,sCon,_s，_,sCase⊗,sInl,sInr,sCase⊕,sNil,sCons,sHead,sTail}
\begin{code}
module St-explicit where
 pattern s⟨_⟩ {g = g}  sg   {p}     = %⟨⟩ {g = g} {sg} {p}
 pattern s[_]          i            = %[] {i = i}
 pattern sLet {Γ = Γ}  sx   se      = %Let {Γ = Γ} ⦃ sx ⦄ ⦃ se ⦄
 pattern sLoop         si   sf      = %Loop si ⦃ sf ⦄
 
 pattern sUnit                      = %Unit
 pattern sCon {v = v}               = %Con {v = v}
 
 pattern _s，_         sx   sy      = %， ⦃ sx ⦄ ⦃ sy ⦄
 pattern sCase⊗        sxy  sf      = %Case⊗ ⦃ sxy ⦄ ⦃ sf ⦄
 
 pattern sInl          sx           = %Inl ⦃ sx ⦄
 pattern sInr          sy           = %Inr ⦃ sy ⦄
 pattern sCase⊕        sxy  sf  sg  = %Case⊕ ⦃ sxy ⦄ ⦃ sf ⦄ ⦃ sg ⦄
 
 pattern sNil                       = %Nil
 pattern sCons         x    xs      = %Cons ⦃ x ⦄ ⦃ xs ⦄
 pattern sHead         xs           = %Head ⦃ xs ⦄
 pattern sTail         xs           = %Tail ⦃ xs ⦄

 infixr 4 _s，_
\end{code}
</Lambda1-state-explicit>


\begin{code}
open St-explicit public
\end{code}


%<*sApp-flipped-let>
\AgdaTarget{\_s﹩\_}
\begin{code}
_s﹩_ : {f : λB[ G ] (σ ◅ Γ) τ} {x : λB[ G ] Γ σ} (sf : St f) (sx : St x) → St (f ﹩ x)
_s﹩_ = flip sLet
\end{code}
%</sApp-flipped-let>

\begin{code}
infixl 90 _s﹩_
\end{code}


%<*var-states>
\AgdaTarget{\s#₀,\s#₁,\s#₂,\s#₃,\s#₄}
\begin{code}
s#₀ : St {G = G} {τ₀                      ◅ Γ} #₀
s#₁ : St {G = G} {τ₀ ◅ τ₁                 ◅ Γ} #₁
s#₂ : St {G = G} {τ₀ ◅ τ₁ ◅ τ₂            ◅ Γ} #₂
s#₃ : St {G = G} {τ₀ ◅ τ₁ ◅ τ₂ ◅ τ₃       ◅ Γ} #₃
s#₄ : St {G = G} {τ₀ ◅ τ₁ ◅ τ₂ ◅ τ₃ ◅ τ₄  ◅ Γ} #₄
s#₀ = s[ ix₀ ];  s#₁ = s[ ix₁ ];  s#₂ = s[ ix₂ ];  s#₃ = s[ ix₃ ]; s#₄ = s[ ix₄ ]
\end{code}
%</var-states>


%<*%K>
\AgdaTarget{%K}
\begin{code}
%K : {c : λB[ G ] Γ τ} ⦃ p : Δ ⊇ Γ ⦄ ⦃ s : St c ⦄ → St (K p c)
%K ⦃ p ⦄ ⦃ %⟨⟩ {sg = sg} ⦄                 = %⟨⟩ {sg = sg}
%K ⦃ p ⦄ ⦃ %[] {i = i} ⦄                   = %[] {i = Kix p i}
%K ⦃ p ⦄ ⦃ %Let ⦃ sx ⦄ ⦃ sf ⦄ ⦄            = %Let ⦃ %K ⦃ p ⦄ ⦃ sx ⦄ ⦄ ⦃ %K ⦃ I p ⦄ ⦃ sf ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Loop si ⦃ sf ⦄ ⦄               = %Loop si ⦃ %K ⦃ I p ⦄ ⦃ sf ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Unit ⦄                         = %Unit
%K ⦃ p ⦄ ⦃ %Con ⦄                          = %Con
%K ⦃ p ⦄ ⦃ %， ⦃ sx ⦄ ⦃ sy ⦄ ⦄             =  %， ⦃ %K ⦃ p ⦄ ⦃ sx ⦄ ⦄ ⦃ %K ⦃ p ⦄ ⦃ sy ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Case⊗ ⦃ sxy ⦄ ⦃ sf ⦄ ⦄         = %Case⊗ ⦃ %K ⦃ p ⦄ ⦃ sxy ⦄ ⦄ ⦃ %K ⦃ I $ I $ p ⦄ ⦃ sf ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Inl ⦃ sx ⦄ ⦄                   = %Inl ⦃ %K ⦃ p ⦄ ⦃ sx ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Inr ⦃ sy ⦄ ⦄                   = %Inr ⦃ %K ⦃ p ⦄ ⦃ sy ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Case⊕ ⦃ sxy ⦄ ⦃ sf ⦄ ⦃ sg ⦄ ⦄  = %Case⊕ ⦃ %K ⦃ p ⦄ ⦃ sxy ⦄ ⦄ ⦃ %K ⦃ I p ⦄ ⦃ sf ⦄ ⦄ ⦃ %K ⦃ I p ⦄ ⦃ sg ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Nil ⦄                          = %Nil
%K ⦃ p ⦄ ⦃ %Cons ⦃ sx ⦄ ⦃ sxs ⦄ ⦄          = %Cons ⦃ %K ⦃ p ⦄ ⦃ sx ⦄ ⦄ ⦃ %K ⦃ p ⦄ ⦃ sxs ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Head ⦃ sxs ⦄ ⦄                 = %Head ⦃ %K ⦃ p ⦄ ⦃ sxs ⦄ ⦄
%K ⦃ p ⦄ ⦃ %Tail ⦃ sxs ⦄ ⦄                 = %Tail ⦃ %K ⦃ p ⦄ ⦃ sxs ⦄ ⦄
\end{code}
%</%K>

%<*sK>
\AgdaTarget{sK}
\begin{code}
sK : (p : Δ ⊇ Γ) {c : λB[ G ] Γ τ} (s : St c) → St (K p c)
sK p s = %K ⦃ p ⦄ ⦃ s ⦄
\end{code}
%</sK>

%<*sK′>
\AgdaTarget{sK′}
\begin{code}
sK′ : {c : λB[ G ] Γ τ} (s : St c) {p : Δ ⊇ Γ} → St (K p c)
sK′ s {p} = sK p s
\end{code}
%</sK′>


%<*sK-nums>
\AgdaTarget{sK₁,sK₂,sK₃,sK₄}
\begin{code}
sK₁ : {c : λB[ G ] Γ τ} (s : St c)  → St {Γ = τ₁                 ◅ Γ} (K₁ c)
sK₂ : {c : λB[ G ] Γ τ} (s : St c)  → St {Γ = τ₁ ◅ τ₂            ◅ Γ} (K₂ c)
sK₃ : {c : λB[ G ] Γ τ} (s : St c)  → St {Γ = τ₁ ◅ τ₂ ◅ τ₃       ◅ Γ} (K₃ c)
sK₄ : {c : λB[ G ] Γ τ} (s : St c)  → St {Γ = τ₁ ◅ τ₂ ◅ τ₃ ◅ τ₄  ◅ Γ} (K₄ c)

sK₁ = sK (O R)
sK₂ = sK₁ ∘′ sK₁;  sK₃ = sK₁ ∘′ sK₂;  sK₄ = sK₁ ∘′ sK₃
\end{code}
%</sK-nums>


%<*sDef>
\AgdaTarget{sDef}
\begin{code}
sDef : {G : Gates B} (b : B) {c : λB[ G ] Γ τ} → St c
sDef {G = G}  b  {⟨_⟩ g}                 = s⟨_⟩ (def b (gSt G g))
sDef          _  {[_] i}                 = s[_] i
sDef          b  {Let x e}               = sLet (sDef b {x}) (sDef b {e})
sDef          b  {Loop {σ = σ} f}        = sLoop (def b σ) (sDef b {f})
sDef          _  {Unit}                  = sUnit
sDef          _  {Con _}                 = sCon
sDef          b  {_，_ x y}              = _s，_ (sDef b {x}) (sDef b {y})
sDef          b  {Case⊗_of_ x∧y f}       = sCase⊗ (sDef b {x∧y}) (sDef b {f})
sDef          b  {Inl x}                 = sInl (sDef b {x})
sDef          b  {Inr y}                 = sInr (sDef b {y})
sDef          b  {Case⊕_of_or_ x∨y f g}  = sCase⊕ (sDef b {x∨y}) (sDef b {f}) (sDef b {g})
sDef          _  {Nil}                   = sNil
sDef          b  {Cons x xs}             = sCons (sDef b {x}) (sDef b {xs})
sDef          b  {Head xs}               = sHead (sDef b {xs})
sDef          b  {Tail xs}               = sTail (sDef b {xs})
\end{code}
%</sDef>


%<*sinj₁⊞>
\AgdaTarget{sinj₁⊞}
\begin{code}
sinj₁⊞ : {c : λB[ G₁ ] Γ τ} (s : St c) → St (inj₁⊞ {G₂ = G₂} c)
sinj₁⊞ s⟨ g ⟩              = s⟨ g ⟩
sinj₁⊞ s[ i ]              = s[ i ]
sinj₁⊞ (sLet sx se)        = sLet (sinj₁⊞ sx) (sinj₁⊞ se)
sinj₁⊞ (sLoop si sf)       = sLoop si (sinj₁⊞ sf)
sinj₁⊞ sUnit               = sUnit
sinj₁⊞ sCon                = sCon
sinj₁⊞ (sx s， sy)         = sinj₁⊞ sx s， sinj₁⊞ sy
sinj₁⊞ (sCase⊗ sxy sf)     = sCase⊗ (sinj₁⊞ sxy) (sinj₁⊞ sf)
sinj₁⊞ (sInl sx)           = sInl (sinj₁⊞ sx)
sinj₁⊞ (sInr sy)           = sInr (sinj₁⊞ sy)
sinj₁⊞ (sCase⊕ sxy sf sg)  = sCase⊕ (sinj₁⊞ sxy) (sinj₁⊞ sf) (sinj₁⊞ sg)
sinj₁⊞ sNil                = sNil
sinj₁⊞ (sCons sx sxs)      = sCons (sinj₁⊞ sx) (sinj₁⊞ sxs)
sinj₁⊞ (sHead sxs)         = sHead (sinj₁⊞ sxs)
sinj₁⊞ (sTail sxs)         = sTail (sinj₁⊞ sxs)
\end{code}
%</sinj₁⊞>


%<*sinj₂⊞>
\AgdaTarget{sinj₂⊞}
\begin{code}
sinj₂⊞ : {c : λB[ G₂ ] Γ τ} (s : St c) → St (inj₂⊞ {G₁ = G₁} c)
sinj₂⊞ s⟨ g ⟩              = s⟨ g ⟩
sinj₂⊞ s[ i ]              = s[ i ]
sinj₂⊞ (sLet sx se)        = sLet (sinj₂⊞ sx) (sinj₂⊞ se)
sinj₂⊞ (sLoop si sf)       = sLoop si (sinj₂⊞ sf)
sinj₂⊞ sUnit               = sUnit
sinj₂⊞ sCon                = sCon
sinj₂⊞ (sx s， sy)         = sinj₂⊞ sx s， sinj₂⊞ sy
sinj₂⊞ (sCase⊗ sxy sf)     = sCase⊗ (sinj₂⊞ sxy) (sinj₂⊞ sf)
sinj₂⊞ (sInl sx)           = sInl (sinj₂⊞ sx)
sinj₂⊞ (sInr sy)           = sInr (sinj₂⊞ sy)
sinj₂⊞ (sCase⊕ sxy sf sg)  = sCase⊕ (sinj₂⊞ sxy) (sinj₂⊞ sf) (sinj₂⊞ sg)
sinj₂⊞ sNil                = sNil
sinj₂⊞ (sCons sx sxs)      = sCons (sinj₂⊞ sx) (sinj₂⊞ sxs)
sinj₂⊞ (sHead sxs)         = sHead (sinj₂⊞ sxs)
sinj₂⊞ (sTail sxs)         = sTail (sinj₂⊞ sxs)
\end{code}
%</sinj₂⊞>



%<*sMapAccL-par>
\AgdaTarget{sMapAccL-par}
\begin{code}
sMapAccL-par :  {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} {e : λB[ G ] Γ σ} {xs : λB[ G ] Γ (vec ρ n)}
                (sfs : Vec (St f) n) (se : St e) (sxs : St xs) → St (MapAccL-par f e xs)
sMapAccL-par []           se _    = se s， sNil
sMapAccL-par (sf ∷ sfs′)  se sxs  = sCase⊗
                                        (sf s﹩ sK′ se s﹩ sHead sxs)
                                        (sCase⊗
                                            (sMapAccL-par (map (λ h → sK′ h) sfs′) s#₀ (sTail (sK₂ sxs)))
                                            (s#₀ s， sCons s#₃ s#₁))
\end{code}
%</sMapAccL-par>


