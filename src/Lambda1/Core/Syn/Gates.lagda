\begin{code}
module Lambda1.Core.Syn.Gates where

open import Data.Sum using (_⊎_; [_,_]′)

open import Lambda1.Core.Syn.UTypes using (B; C; U; Ctx)
\end{code}



%<*Gates>
\AgdaTarget{Gates}
\begin{code}
record Gates B : Set₁ where
  field
    gIx      : Set
    gCtx     : (g : gIx) → Ctx B
    gSt gOut : (g : gIx) → U B
\end{code}
%</Gates>

\begin{code}
variable
  G  G₁ G₂ : Gates B
  H  H₁ H₂ : Gates B
  L  L₁ L₂ : Gates C
\end{code}
    


\begin{code}
infixr 1 _⊞_
\end{code}

%<*gatelib-union>
\AgdaTarget{⊞}
\begin{code}
_⊞_ : (G₁ G₂ : Gates B) → Gates B
G₁ ⊞ G₂ = record  { gIx   = gIx G₁ ⊎ gIx G₂
                  ; gSt   = [ gSt G₁  , gSt G₂ ]′
                  ; gCtx  = [ gCtx G₁ , gCtx G₂ ]′
                  ; gOut  = [ gOut G₁ , gOut G₂ ]′
                  } where open Gates
\end{code}
%</gatelib-union>
