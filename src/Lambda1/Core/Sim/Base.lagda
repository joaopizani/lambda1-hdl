\begin{code}
module Lambda1.Core.Sim.Base where

open import Function.Base using (flip; id; _$_; _∘′_)
open import Data.Unit.Base using (⊤)
open import Data.Product using (_×_; proj₁; proj₂; _,_) renaming (map to map×)
open import Data.Sum.Base using (_⊎_; inj₁; inj₂; [_,_]′)
open import Data.Vec.Base using ([]; _∷_; Vec; head; tail)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Star.Decoration using (↦)
open import Data.MapAccum using (mapAccumL)

open import Lambda1.Core.Syn.UTypes using (B; C; U; τ; σ; ρ; Ctx; Γ; Δ; _∋_; _⊇_; ⇓τ; ⇓Γ; Env⊇);  open U; open _⊇_
open import Lambda1.Core.Syn.Gates using (Gates; G);  open Gates
open import Lambda1.Core.Syn.Lang using (λB[_]; K);  open λB[_]
open import Lambda1.Core.Syn.State using (St; sK; %Loop; module St-explicit);  open St-explicit

open import Lambda1.Core.Sem.UTypes using (UF; CtxF; Val; Env; lookup)
open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sem.Lang using (CircF)
\end{code}



-- TODO: this becomes module parameter for semantics
%<*TyS>
\AgdaTarget{TyS}
\begin{code}
TyS : UF B
TyS = Val
\end{code}
%</TyS>

%<*CtxS>
\AgdaTarget{CtxS}
\begin{code}
CtxS : CtxF B
CtxS = Env TyS
\end{code}
%</CtxS>


%<*CircS>
\AgdaTarget{CircS}
\begin{code}
CircS : ∀ (F G : Set → Set) → CircF B
CircS F G {Γ = Γ} {τ} c = St c → F (CtxS Γ) → (St c) × G (TyS τ)
\end{code}
%</CircS>

%<*CircS-noretstate>
\AgdaTarget{CircS₂}
\begin{code}
CircS₂ : ∀ (F G : Set → Set) → CircF B
CircS₂ F G {Γ = Γ} {τ} c = St c → F (CtxS Γ) → G (TyS τ)
\end{code}
%</Circ-noretstate>


%<*simulation-decls>
\begin{code}
⟦_⊨_⟧s : ∀ (S : gS G) (c : λB[ G ] Γ τ)     → CircS   id            id            c
⟦_⊨_⟧n : ∀ (S : gS G) (c : λB[ G ] Γ τ)  n  → CircS   (flip Vec n)  (flip Vec n)  c

⟦_⊨_⟧S : ∀ (S : gS G) (c : λB[ G ] Γ τ)     → CircS₂   id            id            c
⟦_⊨_⟧N : ∀ (S : gS G) (c : λB[ G ] Γ τ)  n  → CircS₂   (flip Vec n)  (flip Vec n)  c
\end{code}
%</simulation-decls>


%<*simulation-step-withstate-defs>
\AgdaTarget{⟦\_⊨\_⟧s}
\begin{code}
⟦ S ⊨ ⟨ g ⟩ {p} ⟧s s⟨ sg ⟩ γ = map× (λ s → s⟨ s ⟩) id $ (S g) sg (Env⊇ p γ)

⟦ _ ⊨ [ i ] ⟧s s γ = s , lookup γ i

⟦ S ⊨ Let x b ⟧s (sLet sx sb) γ =
  let  sx′ , rx = ⟦ S ⊨ x ⟧s sx γ
       sb′ , rb = ⟦ S ⊨ b ⟧s sb (↦ rx ◅ γ)
  in  (sLet sx′ sb′) , rb

⟦ S ⊨ Loop f ⟧s (sLoop si sf) γ =
  let  sf′ , si′ , rl = ⟦ S ⊨ f ⟧s sf (↦ si ◅ γ)
  in   (sLoop si′ sf′) , rl

⟦ _ ⊨ Unit   ⟧s _ _ = sUnit  , _
⟦ _ ⊨ Con v  ⟧s _ _ = sCon   , v

⟦ S ⊨ x ， y ⟧s (sx s， sy) γ =
  let  sx′ , rx = ⟦ S ⊨ x ⟧s sx γ
       sy′ , ry = ⟦ S ⊨ y ⟧s sy γ
  in (sx′ s， sy′) , (rx , ry)

⟦ S ⊨ Case⊗ xy of f ⟧s (sCase⊗ sxy sf) γ =
  let  sxy′  , (rx , ry)  = ⟦ S ⊨ xy ⟧s  sxy  γ
       sf′   , rz         = ⟦ S ⊨ f  ⟧s  sf   (↦ rx ◅ ↦ ry ◅ γ)
  in  (sCase⊗ sxy′ sf′) , rz

⟦ S ⊨ Inl x ⟧s (sInl sx) = map× sInl inj₁ ∘′ ⟦ S ⊨ x ⟧s sx
⟦ S ⊨ Inr y ⟧s (sInr sy) = map× sInr inj₂ ∘′ ⟦ S ⊨ y ⟧s sy

⟦ S ⊨ Case⊕ xy of f or g ⟧s (sCase⊕ sxy sf sg) γ =
  let  sxy′ , rxy = ⟦ S ⊨ xy ⟧s sxy γ
  in   [  map× (flip (sCase⊕ sxy′) sg)  id ∘′ (⟦ S ⊨ f ⟧s sf ∘′ (_◅ γ) ∘′ ↦)
       ,  map× (sCase⊕ sxy′ sf)         id ∘′ (⟦ S ⊨ g ⟧s sg ∘′ (_◅ γ) ∘′ ↦) ]′ rxy

⟦ _ ⊨ Nil ⟧s _ _ = sNil , []

⟦ S ⊨ Cons x xs ⟧s (sCons sx sxs) γ =
  let  sx′   , rx   = ⟦ S ⊨ x ⟧s   sx   γ
       sxs′  , rxs  = ⟦ S ⊨ xs ⟧s  sxs  γ
  in  (sCons sx′ sxs′) , (rx ∷ rxs)

⟦ S ⊨ Head xs ⟧s (sHead sxs) = map× sHead head ∘′ ⟦ S ⊨ xs ⟧s sxs
⟦ S ⊨ Tail xs ⟧s (sTail sxs) = map× sTail tail ∘′ ⟦ S ⊨ xs ⟧s sxs
\end{code}
%</simulation-step-withstate-defs>


%<*simulation-step-nostate>
\AgdaTarget{⟦\_⊨\_⟧S}
\begin{code}
⟦ S ⊨ c ⟧S s = proj₂ ∘′ ⟦ S ⊨ c ⟧s s
\end{code}
%</simulation-step-nostate>

%<*simulation-vec-withstate>
\AgdaTarget{⟦\_⊨\_⟧n}
\begin{code}
⟦ S ⊨ c ⟧n n s = mapAccumL {n = n} ⟦ S ⊨ c ⟧s s
\end{code}
%</simulation-vec-withstate>

%<*simulation-vec-nostate>
\AgdaTarget{⟦\_⊨\_⟧N}
\begin{code}
⟦ S ⊨ c ⟧N n s = proj₂ ∘′ ⟦ S ⊨ c ⟧n n s
\end{code}
%</simulation-vec-nostate>


%<*evalSeq>
\AgdaTarget{evalSeq}
\begin{code}
evalSeq : (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (S : gS G) → ⊤ → St (Loop f) → CtxS (ρ ◅ Γ) → (⊤ × St (Loop f) × TyS τ)
(evalSeq f) S u smf xγ =  u , ⟦ S ⊨ Loop f ⟧s smf xγ
\end{code}
%</evalSeq>



\begin{code}
module WithGateSim (S : gS G) where
\end{code}

\begin{code}
 ⟦_⟧s  : ∀ {Γ τ} (c : λB[ G ] Γ τ)     → CircS id            id            c
 ⟦_⟧n  : ∀ {Γ τ} (c : λB[ G ] Γ τ)  n  → CircS (flip Vec n)  (flip Vec n)  c

 ⟦_⟧S  : ∀ {Γ τ} (c : λB[ G ] Γ τ)     → CircS₂ id            id            c
 ⟦_⟧N  : ∀ {Γ τ} (c : λB[ G ] Γ τ)  n  → CircS₂ (flip Vec n)  (flip Vec n)  c
\end{code}

\begin{code}
 ⟦_⟧s = ⟦ S ⊨_⟧s
 ⟦_⟧n = ⟦ S ⊨_⟧n

 ⟦_⟧S = ⟦ S ⊨_⟧S
 ⟦_⟧N = ⟦ S ⊨_⟧N
\end{code}
