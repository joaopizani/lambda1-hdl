\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Lambda1.Core.Sim.Stream where

open import Function using (_∘′_)
open import Data.Product using (proj₂; _×_)
open import Codata.Musical.Stream using (Stream)

open import Data.MapAccum.Stream using (mapAccumStream)

open import Lambda1.Core.Syn.UTypes using (Γ; τ)
open import Lambda1.Core.Syn.Gates using (G; Gates)
open import Lambda1.Core.Syn.Lang using (λB[_])
open import Lambda1.Core.Syn.State using (St)

open import Lambda1.Core.Sem.Gates using (gS)

open import Lambda1.Core.Sim.Base using (CircS; CircS₂; CtxS; TyS; ⟦_⊨_⟧s)
\end{code}


%<*simulation-stream-retstate>
\AgdaTarget{⟦\_⊨\_⟧*}
\begin{code}
⟦_⊨_⟧* : ∀ (S : gS G) (c : λB[ G ] Γ τ) → St c → Stream (CtxS Γ) → Stream (St c) × Stream (TyS τ)
⟦ S ⊨ c ⟧* = mapAccumStream ⟦ S ⊨ c ⟧s
\end{code}
%</simulation-stream-retstate>


%<*simulation-stream-dontretstate>
\AgdaTarget{⟦\_⊨\_⟧★}
\begin{code}
⟦_⊨_⟧★ : ∀ (S : gS G) (c : λB[ G ] Γ τ) → St c → Stream (CtxS Γ) → Stream (TyS τ)
⟦ S ⊨ c ⟧★ s = proj₂ ∘′ ⟦ S ⊨ c ⟧* s
\end{code}
%</simulation-stream-dontretstate>


\begin{code}
module SimulationWithGateSim {B} {G : Gates B} (S : gS G) where
\end{code}

\begin{code}
 ⟦_⟧* : ∀ {Γ τ} (c : λB[ G ] Γ τ) → St c → Stream (CtxS Γ) → Stream (St c) × Stream (TyS τ)
 ⟦_⟧* = ⟦ S ⊨_⟧*
\end{code}

\begin{code}
 ⟦_⟧★ : ∀ {Γ τ} (c : λB[ G ] Γ τ) → St c → Stream (CtxS Γ) → Stream (TyS τ)
 ⟦_⟧★ = ⟦ S ⊨_⟧★
\end{code}
