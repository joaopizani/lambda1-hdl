\begin{code}
module Lambda1.Core.Sim.Refine where

open import Function.Base using (_$_)
open import Data.Nat.Base using (suc; zero)
open import Data.Product using (_×_; _,_; Σ) renaming (map to map×; proj₁ to p₁; proj₂ to p₂)
open import Data.Product.Properties using (,-injectiveʳ; ,-injectiveˡ)
open import Data.Sum.Base using (inj₁; inj₂)
open import Data.Vec.Base using (Vec; head; _∷_; map)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε; _◅_)
open import Data.Star.Decoration using (↦)
open import Data.Star.Pointer using (done; step)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; cong₂; sym; trans; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-⟩; step-≡-∣; _∎)

open import Lambda1.Core.Notation using (n; α; β; φ)
open import Lambda1.Core.Syn.UTypes using (B; C; U; Ctx; vec; ι; _⊗_; σ; τ; Γ; Δ; ⇓τ; ⇓Γ; _⊇_; _∋_; Env⊇; map-⊇; map-∋; R; O; I)
open import Lambda1.Core.Syn.Gates using (Gates; H);  open Gates
open import Lambda1.Core.Syn.Lang using (λB[_]; K);  open λB[_]
open import Lambda1.Core.Syn.State using (St; sK; %Loop; module St-explicit);  open St-explicit

open import Lambda1.Core.Sem.UTypes using (Val; ⇓t; ⇓γ; Env)
open import Lambda1.Core.Sem.Gates using (gS)

open import Lambda1.Core.Sim.Base using (⟦_⊨_⟧s)
open import Lambda1.Core.Sim.K using (K-pres-out)
\end{code}


\begin{code}
×-≡,≡→≡ : ∀ {ℓ} {α β : Set ℓ} {p₁@(x₁ , y₁) p₂@(x₂ , y₂) : α × β} → x₁ ≡ x₂ → y₁ ≡ y₂ → p₁ ≡ p₂
×-≡,≡→≡ refl refl = refl
\end{code}



%<*⇓t-comm-alg>
\begin{code}
⇓t-comm-head :  ∀ {B} {ι′ : U C} (⇓ι : Val {B} ι → Val ι′) {τ : U B}
                → (xs : Vec (Val τ) (suc n))
                → (⇓t ⇓ι {τ}) (head xs) ≡ head ((⇓t {ι′ = ι′} ⇓ι {vec τ (suc n)}) xs)
⇓t-comm-head ⇓ι (x ∷ xs) = refl
\end{code}
%</⇓t-comm-alg>


%<*refine-gamma-Env-incl>
\AgdaTarget{⇓γ-Env⊇}
\begin{code}
⇓γ-Env⊇ : ∀ {B C} {ι′ : U C} {Γ Δ : Ctx B}
            (⇓ι : Val {B} ι → Val ι′) (p : Δ ⊇ Γ) (δ : Env Val Δ)
            → ⇓γ ⇓ι (Env⊇ p δ) ≡ Env⊇ (map-⊇ (⇓τ ι′) p) (⇓γ ⇓ι δ)
⇓γ-Env⊇              ⇓ι R       _           = refl
⇓γ-Env⊇              ⇓ι (O p′)  (↦ x ◅ δ′)  = ⇓γ-Env⊇ ⇓ι p′ δ′
⇓γ-Env⊇ {Δ = τ ◅ _}  ⇓ι (I p′)  (↦ x ◅ δ′)  = cong (λ e → ↦ (⇓t ⇓ι {τ} x) ◅ e) (⇓γ-Env⊇ ⇓ι p′ δ′)
\end{code}
%</refine-gamma-Env-incl>



* Parameters in common to all definitions related to refinement
\begin{code}
module Params (S : gS H) {L : Gates C} (T : gS L) {ι′ : U C} (⇓ι : Val ι → Val ι′) where
\end{code}


%<*impBy>
\AgdaTarget{impBy}
\begin{code}
 impBy :  (g : gIx H)  (c : λB[ L ] (⇓Γ ι′ (gCtx H g)) (⇓τ ι′ (gOut H g)))
          (⇓gSt : Val (gSt H g) → St c) → Set
 impBy g c ⇓gSt = let ⇓t″ = ⇓t ⇓ι {gOut H g} in
   ∀ sg γ →     map× ⇓gSt ⇓t″  (S        g        sg          γ)
             ≡                 ⟦ T ⊨_⟧s  c  (⇓gSt sg)  (⇓γ ⇓ι γ)
\end{code}
%</impBy>


%<*imp-gate>
\AgdaTarget{⇓g}
\begin{code}
 record ⇓g (g : gIx H) : Set where
   field
     c     : λB[ L ] (⇓Γ ι′ (gCtx H g)) (⇓τ ι′ (gOut H g))
     ⇓gSt  : Val (gSt H g) → St c
     imp   : impBy g c ⇓gSt
\end{code}
%</imp-gate>


%<*imp-gates>
\AgdaTarget{⇓G}
\begin{code}
 ⇓G : Set
 ⇓G = ∀ (g : gIx H) → ⇓g g
\end{code}
%</imp-gates>



\begin{code}
 open ⇓g
\end{code}

%<*ref-circ>
\AgdaTarget{⇓c}
\begin{code}
 ⇓c : ∀ (li : ⇓G) {τ} (c : λB[ H ] Γ τ) → λB[ L ] (⇓Γ ι′ Γ) (⇓τ ι′ τ)
 
 ⇓c li      (⟨_⟩ g {p})  = K (map-⊇ (⇓τ ι′) p) ((li g) .c)
 ⇓c li      ([_] ix)     = [_] (map-∋ (⇓τ ι′) ix)
 ⇓c li {τ}  (Con v)      = Con (⇓t ⇓ι {τ} v)
 
 -- simply recursive cases
 ⇓c li (Let x b)              = Let (⇓c li x) (⇓c li b)
 ⇓c li (Loop c)               = Loop (⇓c li c)
 ⇓c li Unit                   = Unit
 ⇓c li (_，_ x y)             = _，_ (⇓c li x) (⇓c li y)
 ⇓c li (Case⊗_of_ xy f)       = Case⊗_of_ (⇓c li xy) (⇓c li f)
 ⇓c li (Inl x)                = Inl (⇓c li x)
 ⇓c li (Inr y)                = Inr (⇓c li y)
 ⇓c li (Case⊕_of_or_ xy f g)  = Case⊕_of_or_ (⇓c li xy) (⇓c li f) (⇓c li g)
 ⇓c li Nil                    = Nil
 ⇓c li (Cons x xs)            = Cons (⇓c li x) (⇓c li xs)
 ⇓c li (Head xs)              = Head (⇓c li xs)
 ⇓c li (Tail xs)              = Tail (⇓c li xs)
\end{code}
%</ref-circ>


%<*ref-state>
\AgdaTarget{⇓s}
\begin{code}
 ⇓s : ∀ (li : ⇓G) {τ} {c : λB[ H ] Γ τ} (s : St c) → St (⇓c li c)
 
 ⇓s li      (s⟨_⟩ sg {p})              = sK    (map-⊇ (⇓τ ι′) p)   (((li _) .⇓gSt) sg)
 ⇓s li      (s[_] ix)                  = s[_]  (map-∋ (⇓τ ι′) ix)
 ⇓s li {τ}  sCon                       = sCon
 
 ⇓s li      (%Loop {σ = σ} si ⦃ sf ⦄)  = sLoop (⇓t ⇓ι {σ} si) (⇓s li sf)
 
 -- simply recursive cases
 ⇓s li (sLet sx sb)        = sLet (⇓s li sx) (⇓s li sb)
 ⇓s li sUnit               = sUnit
 ⇓s li (_s，_ sx sy)       = _s，_ (⇓s li sx) (⇓s li sy)
 ⇓s li (sCase⊗ sxy sf)     = sCase⊗ (⇓s li sxy) (⇓s li sf)
 ⇓s li (sInl sx)           = sInl (⇓s li sx)
 ⇓s li (sInr sy)           = sInr (⇓s li sy)
 ⇓s li (sCase⊕ sxy sf sg)  = sCase⊕ (⇓s li sxy) (⇓s li sf) (⇓s li sg)
 ⇓s li sNil                = sNil
 ⇓s li (sCons sx sxs)      = sCons (⇓s li sx) (⇓s li sxs)
 ⇓s li (sHead sxs)         = sHead (⇓s li sxs)
 ⇓s li (sTail sxs)         = sTail (⇓s li sxs)
\end{code}
%</ref-state>




%<*ref-pres-out-Gate>
\AgdaTarget{⇓c-pres-out-Gate}
\begin{code}
 ⇓c-pres-out-Gate :  ∀  (li : ⇓G) (g : gIx H) (q : Γ ⊇ gCtx H g) (sg : Val (gSt H g)) (γ : Env Val Γ)
                     →     (⇓t ⇓ι {gOut H g}) (  p₂ (⟦_⊨_⟧s {G = H} S         (⟨ g ⟩ {q})          s⟨ sg ⟩          γ))
                        ≡                        p₂ (⟦_⊨_⟧s {G = L} T (⇓c li  (⟨ g ⟩ {q})) (⇓s li  s⟨ sg ⟩) (⇓γ ⇓ι  γ))
 ⇓c-pres-out-Gate li g q sg γ =
   begin
     (⇓t ⇓ι {gOut H g}) (p₂ (⟦_⊨_⟧s {G = H}    S  (⟨ g ⟩ {q})  s⟨ sg ⟩  γ))

   ≡⟨⟩

     (⇓t ⇓ι {gOut H g}) (p₂                 (  S  g               sg                 (Env⊇ q γ)))

   ≡⟨ ,-injectiveʳ ((li g .imp) sg (Env⊇ q γ)) ⟩

     p₂  (    ⟦ T ⊨ li g .c ⟧s
                  ((li g .⇓gSt) sg)
                  (⇓γ ⇓ι (Env⊇ q γ))
         )

   ≡⟨ cong (λ e → p₂ (⟦ T ⊨ li g .c ⟧s ((li g .⇓gSt) sg) e) ) (⇓γ-Env⊇ ⇓ι q γ) ⟩

     p₂  (    ⟦ T ⊨ li g .c ⟧s
                  ((li g .⇓gSt) sg)
                  (Env⊇ (map-⊇ (⇓τ ι′) q) (⇓γ ⇓ι γ))
         )

   ≡⟨ K-pres-out T (li g .c) ((li g .⇓gSt) sg) (map-⊇ (⇓τ ι′) q) (⇓γ ⇓ι γ) ⟩

     p₂  (    ⟦ T ⊨ K (map-⊇ (⇓τ ι′) q) (li g .c) ⟧s
                  (sK (map-⊇ (⇓τ ι′) q) ((li g .⇓gSt) sg))
                  (⇓γ ⇓ι γ)
         )

   ≡⟨⟩

     p₂  (    ⟦ T ⊨ ⇓c li (⟨ g ⟩ {q}) ⟧s
                  (⇓s li  s⟨ sg ⟩)
                  (⇓γ ⇓ι γ)
         )
   ∎
\end{code}
%</ref-pres-out-Gate>


%<*ref-pres-out-Var>
\AgdaTarget{⇓c-pres-out-Var}
\begin{code}
 ⇓c-pres-out-Var :  ∀ (li : ⇓G) (ix : Γ ∋ τ) (γ : Env Val Γ)
                    →     (⇓t ⇓ι {τ}) (  p₂ ((⟦_⊨_⟧s {G = H} S           [ ix ])          s[ ix ]          γ))
                       ≡                 p₂ ((⟦_⊨_⟧s         T ((⇓c li)  [ ix ])) (⇓s li  s[ ix ]) (⇓γ ⇓ι  γ))
 ⇓c-pres-out-Var li (done refl  ◅ ε)    (↦ _ ◅ _)   = refl
 ⇓c-pres-out-Var li (step _     ◅ ix′)  (↦ _ ◅ γ′)  = ⇓c-pres-out-Var li ix′ γ′
\end{code}
%</ref-pres-out-Var>


-- declarations first for mutual recursion
\begin{code}
 ⇓c-pres-out-b₁ : ∀  (li : ⇓G) {τ σ}
                     (f : λB[ H ] (σ ◅ Γ) τ) (sf : St f) (x : λB[ H ] Γ σ) (sx : St x)
                     (γ : Env Val Γ)
                     →    ⇓t ⇓ι {τ} (  p₂ (⟦ S ⊨       f ⟧s        sf  (↦ (p₂ (⟦ S ⊨ x       ⟧s        sx         γ) )  ◅        γ)))
                        ≡              p₂ (⟦ T ⊨ ⇓c li f ⟧s (⇓s li sf) (↦ (p₂ (⟦ T ⊨ ⇓c li x ⟧s (⇓s li sx) (⇓γ ⇓ι γ)))  ◅ (⇓γ ⇓ι γ)))

 ⇓c-pres-out :  ∀ (li : ⇓G) {τ} (c : λB[ H ] Γ τ) (s : St c) (γ : Env Val Γ)
                →     (⇓t ⇓ι {τ}) (  p₂ (⟦ S ⊨          c ⟧s         s          γ))
                   ≡                 p₂ (⟦ T ⊨ (⇓c li)  c ⟧s (⇓s li  s) (⇓γ ⇓ι  γ))

 postulate ⇓c-pres-out-b₂ : ∀  (li : ⇓G) {τ σ ρ}
                               (f : λB[ H ] (σ ◅ ρ ◅ Γ) τ) (sf : St f) (xy : λB[ H ] Γ (σ ⊗ ρ)) (sxy : St xy)
                               (γ : Env Val Γ)
                               →    ⇓t ⇓ι {τ}
                                      (p₂
                                       (⟦ S ⊨ f ⟧s sf
                                        (↦ (p₁ (p₂ (⟦ S ⊨ xy ⟧s sxy γ))) ◅
                                         ↦ (p₂ (p₂ (⟦ S ⊨ xy ⟧s sxy γ))) ◅ γ)))
                                  ≡
                                       p₂
                                       (⟦ T ⊨ ⇓c li f ⟧s (⇓s li sf)
                                        (↦ (p₁ (p₂ (⟦ T ⊨ ⇓c li xy ⟧s (⇓s li sxy) (⇓γ ⇓ι γ)))) ◅
                                         ↦ (p₂ (p₂ (⟦ T ⊨ ⇓c li xy ⟧s (⇓s li sxy) (⇓γ ⇓ι γ)))) ◅
                                         (⇓γ ⇓ι γ)))
\end{code}


%<*ref-pres-out-bound1>
\AgdaTarget{⇓c-pres-out-b₁}
\begin{code}
 ⇓c-pres-out-b₁ li {τ} {σ} f sf x sx γ =
  sym (
   begin
     p₂ (⟦ T ⊨ ⇓c li f ⟧s (⇓s li sf) (↦ (p₂ (⟦ T ⊨ ⇓c li x ⟧s (⇓s li sx) (⇓γ ⇓ι γ))) ◅ (⇓γ ⇓ι γ)))
   -- IH on each bound term
   ≡⟨ cong (λ e → p₂ (⟦ T ⊨ ⇓c li f ⟧s (⇓s li sf) (↦ e ◅ (⇓γ ⇓ι γ)))) (sym $ ⇓c-pres-out li x sx γ) ⟩
     p₂ (⟦ T ⊨ ⇓c li f ⟧s (⇓s li sf) (↦ (⇓t ⇓ι {σ} (p₂ (⟦ S ⊨ x ⟧s sx γ))) ◅ (⇓γ ⇓ι γ)))
   -- IH on the whole with extended env
   ≡⟨ sym $ ⇓c-pres-out li f sf (↦ (p₂ (⟦ S ⊨ x ⟧s sx γ)) ◅ γ) ⟩
     (⇓t ⇓ι {τ}) (p₂ (⟦ S ⊨ f ⟧s sf (↦ (p₂ (⟦ S ⊨ x ⟧s sx γ)) ◅ γ)))
   ∎)
\end{code}
%</ref-pres-out-bound1>


%<*ref-pres-out-bound2>
\AgdaTarget{⇓c-pres-out-b₂}
\begin{code}
\end{code}
%</ref-pres-out-bound2>


%<*ref-pres-out>
\AgdaTarget{⇓c-pres-out}
\begin{code}
 ⇓c-pres-out li (⟨_⟩ g {q})  s⟨ sg ⟩    γ = ⇓c-pres-out-Gate li g q sg γ
 ⇓c-pres-out li ([_] ix)     s[ .ix ]   γ = ⇓c-pres-out-Var  li ix     γ

 ⇓c-pres-out li (Con _)  _ _ = refl
 ⇓c-pres-out li Unit     _ _ = refl
 ⇓c-pres-out li Nil      _ _ = refl

 ⇓c-pres-out li      (_，_ x y)             (_s，_ sx sy)    γ = ×-≡,≡→≡ (⇓c-pres-out li x sx γ) (⇓c-pres-out li y sy γ)
 ⇓c-pres-out li      (Inl x)                (sInl sx)        γ = cong inj₁ (⇓c-pres-out li x sx γ)
 ⇓c-pres-out li      (Inr y)                (sInr sy)        γ = cong inj₂ (⇓c-pres-out li y sy γ)
 ⇓c-pres-out li      (Loop f)               (sLoop si sf)    γ = cong p₂ (⇓c-pres-out li f sf (↦ si ◅ γ))
 ⇓c-pres-out li      (Let x f)              (sLet sx sf)     γ = ⇓c-pres-out-b₁ li f sf x sx γ

 ⇓c-pres-out li      (Case⊗_of_ xy f)       (sCase⊗ sxy sf)  γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-out li      (Case⊕_of_or_ xy f g)  s                γ = ⊥ where postulate ⊥ : _

 ⇓c-pres-out li      (Cons x xs)            (sCons sx sxs)   γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-out li {τ}  (Head xs)              (sHead sxs)      γ = trans  (⇓t-comm-head ⇓ι {τ} (p₂ (⟦ S ⊨ xs ⟧s sxs γ)))
                                                                        (cong head (⇓c-pres-out li xs sxs γ))
 ⇓c-pres-out li      (Tail xs)              (sTail sxs)      γ = ⊥ where postulate ⊥ : _
\end{code}
%</ref-pres-out>


%<*⇓c-pres-out-map>
\AgdaTarget{⇓c-pres-out-map}
\begin{code}
\end{code}
%</⇓c-pres-out-map>



%<*ref-pres-s-Gate>
\AgdaTarget{⇓c-pres-s-Gate}
\begin{code}
 postulate ⇓c-pres-s-Gate :  ∀ (li : ⇓G) {g : gIx H} (q : Γ ⊇ gCtx H g) (sg : Val (gSt H g)) (γ : Env Val Γ)
                             →     (⇓s li) (  p₁ (⟦ S ⊨          (⟨ g ⟩ {q}) ⟧s         s⟨ sg ⟩          γ))
                                ≡             p₁ (⟦ T ⊨ (⇓c li)  (⟨ g ⟩ {q}) ⟧s (⇓s li  s⟨ sg ⟩) (⇓γ ⇓ι  γ))
\end{code}
%</ref-pres-s-Gate>


%<*ref-pres-s-Var>
\AgdaTarget{⇓c-pres-s-Var}
\begin{code}
 ⇓c-pres-s-Var :  ∀ (li : ⇓G) (ix : Γ ∋ τ) (γ : Env Val Γ)
                  →     (⇓s li) (  p₁ (⟦ S ⊨          [ ix ] ⟧s         s[ ix ]          γ))
                     ≡             p₁ (⟦ T ⊨ (⇓c li)  [ ix ] ⟧s (⇓s li  s[ ix ]) (⇓γ ⇓ι  γ))
 ⇓c-pres-s-Var li ix γ = refl
\end{code}
%</ref-pres-s-Var>


%<*ref-pres-s>
\AgdaTarget{⇓c-pres-s}
\begin{code}
 ⇓c-pres-s :  ∀  (li : ⇓G) (c : λB[ H ] Γ τ) (s : St c) (γ : Env Val Γ)
              →     (⇓s li) (  p₁ (⟦ S ⊨          c ⟧s         s          γ))
                 ≡             p₁ (⟦ T ⊨ (⇓c li)  c ⟧s (⇓s li  s) (⇓γ ⇓ι  γ))
 ⇓c-pres-s li c (s⟨_⟩ sg {q})      γ = ⇓c-pres-s-Gate li q sg γ
 ⇓c-pres-s li c (s[_] ix)          γ = ⇓c-pres-s-Var  li ix   γ
 ⇓c-pres-s li c (%Loop si ⦃ sf ⦄)  γ = ⊥ where postulate ⊥ : _

 -- simple cases
 ⇓c-pres-s li c sCon   _ = refl
 ⇓c-pres-s li c sUnit  _ = refl

 ⇓c-pres-s li c (sLet sx sb)        γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (_s，_ sx sy)       γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sCase⊗ sxy sf)     γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sInl sx)           γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sInr sy)           γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sCase⊕ sxy sf sg)  γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c sNil                γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sCons sx sxs)      γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sHead sxs)         γ = ⊥ where postulate ⊥ : _
 ⇓c-pres-s li c (sTail sxs)         γ = ⊥ where postulate ⊥ : _
\end{code}
%</ref-pres-s>



%<*ref-preserve>
\AgdaTarget{⇓c-pres}
\begin{code}
 ⇓c-pres :  ∀ (li : ⇓G) (c : λB[ H ] Γ τ) (s : St c) (γ : Env Val Γ)
            →     map× (⇓s li) (⇓t ⇓ι {τ}) (  ⟦ S ⊨          c ⟧s         s          γ)
               ≡                              ⟦ T ⊨ (⇓c li)  c ⟧s (⇓s li  s) (⇓γ ⇓ι  γ)
 ⇓c-pres li c s γ =  ×-≡,≡→≡  (⇓c-pres-s    li c s γ)
                              (⇓c-pres-out  li c s γ)
\end{code}
%</ref-preserve>



* Re-export the module so that the definitions are available with the extra telescope in front
\begin{code}
open Params public
\end{code}
