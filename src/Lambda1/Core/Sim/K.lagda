\begin{code}
module Lambda1.Core.Sim.K where

open import Function.Base using (_$_; id)

open import Data.Product using (_×_; _,_; uncurry) renaming (map to map×; proj₁ to p₁; proj₂ to p₂)
open import Data.Product.Properties using (,-injective)
open import Data.Sum.Base using (inj₁; inj₂)
open import Data.Sum.Properties using (inj₁-injective; inj₂-injective)
open import Data.Vec.Base using (_∷_; head; tail)

open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Star.Decoration using (↦)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; cong₂; trans; sym; module ≡-Reasoning)
open ≡-Reasoning using (begin_; step-≡-⟩; _∎)

open import Lambda1.Core.Syn.UTypes using (B; σ; τ; ρ; Γ; Δ; Ctx; U; _∋_; _⊇_; Kix; comp⊇; Env⊇; I; O; R; Env⊇-comp⊇)
open import Lambda1.Core.Syn.Gates using (Gates);  open Gates
open import Lambda1.Core.Syn.Lang using (λB[_]; K);  open λB[_]
open import Lambda1.Core.Syn.State using (St; sK; module St-explicit);  open St-explicit

open import Lambda1.Core.Sem.Gates using (gS)

open import Lambda1.Core.Sim.Base using (TyS; CtxS; ⟦_⊨_⟧s)
\end{code}


\begin{code}
×-≡,≡→≡ : ∀ {ℓ} {α β : Set ℓ} {p₁@(x₁ , y₁) p₂@(x₂ , y₂) : α × β} → x₁ ≡ x₂ → y₁ ≡ y₂ → p₁ ≡ p₂
×-≡,≡→≡ refl refl = refl
\end{code}


\begin{code}
module Params {G : Gates B} (S : gS G) where
\end{code}

\begin{code}
 module _ {Γ Δ : Ctx B} {τ : U B} where
\end{code}

%<*K-pres-out-Gate>
\AgdaTarget{K-pres-out-Gate}
\begin{code}
  K-pres-out-Gate :  ∀  (g : gIx G) (sg : TyS (gSt G g)) (pg : Γ ⊇ gCtx G g) (p : Δ ⊇ Γ) (δ : CtxS Δ)
                     →     p₂ (⟦_⊨_⟧s {G = G} S (⟨ g ⟩ {pg})          s⟨ sg ⟩                 (Env⊇ p δ))
                        ≡  p₂ (⟦_⊨_⟧s {G = G} S (⟨ g ⟩ {comp⊇ pg p})  (s⟨ sg ⟩ {comp⊇ pg p})  δ)
  K-pres-out-Gate g sg pg p δ = cong (λ e → p₂ (S g sg e)) (Env⊇-comp⊇ pg p)
\end{code}
%</K-pres-out-Gate>


%<*K-pres-out-Var>
\AgdaTarget{K-pres-out-Var}
\begin{code}
  postulate K-pres-out-Var :  ∀  (i : Γ ∋ τ) (p : Δ ⊇ Γ) (δ : CtxS Δ)
                              →     p₂ (⟦_⊨_⟧s {G = G} S [ i ]        s[ i ]        (Env⊇ p δ))
                                 ≡  p₂ (⟦_⊨_⟧s {G = G} S [ Kix p i ]  s[ Kix p i ]  δ)
\end{code}
%</K-pres-out-Var>


%<*K-pres-out-bound₁>
\AgdaTarget{K-pres-out-bound₁}
\begin{code}
  postulate K-pres-out-bound₁ :  ∀  (b : λB[ G ] (σ ◅ Γ) τ) (sb : St b) (p : Δ ⊇ Γ) (δ : CtxS Δ) (t : TyS σ)
                                 →     p₂ (⟦ S ⊨ b          ⟧s sb             (↦ t ◅ Env⊇ p δ))
                                    ≡  p₂ (⟦ S ⊨ K (I p) b  ⟧s (sK (I p) sb)  (↦ t ◅ δ))
\end{code}
%</K-pres-out-bound₁>


%<*K-pres-out-bound₂>
\AgdaTarget{K-pres-out-bound₂}
\begin{code}
  postulate K-pres-out-bound₂ :  ∀  (b : λB[ G ] (σ ◅ ρ ◅ Γ) τ) (sb : St b) (p : Δ ⊇ Γ) (δ : CtxS Δ) (t : TyS σ) (u : TyS ρ)
                                 →     p₂ (⟦ S ⊨ b                ⟧s sb                   (↦ t ◅ ↦ u ◅ Env⊇ p δ))
                                    ≡  p₂ (⟦ S ⊨ K (I $ I $ p) b  ⟧s (sK (I $ I $ p) sb)  (↦ t ◅ ↦ u ◅ δ))
\end{code}
%</K-pres-out-bound₂>



%<*K-pres-out>
\AgdaTarget{K-pres-out}
\begin{code}
 K-pres-out :  ∀ (c : λB[ G ] Γ τ) (s : St c) (p : Δ ⊇ Γ) (δ : CtxS Δ)
                    →     p₂ (⟦ S ⊨ c      ⟧s s         (Env⊇ p δ))
                       ≡  p₂ (⟦ S ⊨ K p c  ⟧s (sK p s)  δ)
 
 K-pres-out (⟨ g ⟩ {q})  s⟨ sg ⟩  p δ = K-pres-out-Gate  {τ = gOut G g} g sg q p δ
 K-pres-out [ i ]        s[ .i ]  p δ = K-pres-out-Var   i p δ
 
 K-pres-out Unit     sUnit  _ _ = refl
 K-pres-out (Con _)  sCon   _ _ = refl
 K-pres-out Nil      sNil   _ _ = refl
 
 K-pres-out (x ， y)     (sx s， sy)     p δ = cong₂  _,_    (K-pres-out x sx p δ)   (K-pres-out y sy p δ)
 K-pres-out (Cons x xs)  (sCons sx sxs)  p δ = cong₂  _∷_    (K-pres-out x sx p δ)   (K-pres-out xs sxs p δ)
 K-pres-out (Inl x)      (sInl sx)       p δ = cong   inj₁   (K-pres-out x sx p δ)
 K-pres-out (Inr y)      (sInr sy)       p δ = cong   inj₂   (K-pres-out y sy p δ)
 K-pres-out (Head xs)    (sHead sxs)     p δ = cong   head   (K-pres-out xs sxs p δ)
 K-pres-out (Tail xs)    (sTail sxs)     p δ = cong   tail   (K-pres-out xs sxs p δ)

 K-pres-out (Loop c)     (sLoop si sc)   p δ = cong   p₂     (K-pres-out-bound₁ c sc p δ si)
 
 K-pres-out (Let x b)    (sLet sx sb)    p δ =
   let  x′   = p₂ (⟦ S ⊨ x      ⟧s  sx         (Env⊇ p δ))
        Kx′  = p₂ (⟦ S ⊨ K p x  ⟧s  (sK p sx)  δ)
        ih   = K-pres-out x sx p δ
        cg   = λ t → p₂ (⟦ S ⊨ b ⟧s sb (↦ t ◅ Env⊇ p δ))
   in
   begin
       p₂ (⟦ S ⊨ b ⟧s               sb                     (↦ x′ ◅ Env⊇ p δ))
   ≡⟨ cong cg ih ⟩
       p₂ (⟦ S ⊨ b ⟧s               sb                     (↦ Kx′ ◅ Env⊇ p δ))
   ≡⟨ K-pres-out-bound₁ b sb p δ Kx′ ⟩
       p₂ (⟦ S ⊨ K p (Let x b) ⟧s   (sK p (sLet sx sb))    δ)
   ∎

 K-pres-out (Case⊗ xy of f) (sCase⊗ sxy sf) p δ =
   let  xy′   = p₂ (⟦ S ⊨      xy ⟧s  sxy         (Env⊇ p δ))
        Kxy′  = p₂ (⟦ S ⊨ K p  xy ⟧s  (sK p sxy)  δ)
        ih    = K-pres-out xy sxy p δ
        cg    = λ t u → p₂ (⟦ S ⊨ f ⟧s sf (↦ (p₁ t) ◅ ↦ (p₂ u) ◅ Env⊇ p δ))
   in
   begin
     p₂ (⟦ S ⊨ f ⟧s                sf                   (↦ (p₁ xy′)   ◅ ↦ (p₂ xy′)   ◅ Env⊇ p δ))
   ≡⟨ cong₂ cg ih ih ⟩
     p₂ (⟦ S ⊨ f ⟧s                sf                   (↦ (p₁ Kxy′)  ◅ ↦ (p₂ Kxy′)  ◅ Env⊇ p δ))
   ≡⟨ K-pres-out-bound₂  f sf p δ (p₁ Kxy′) (p₂ Kxy′) ⟩
     p₂ (⟦ S ⊨ K (I $ I $ p) f ⟧s  (sK (I $ I $ p) sf)  (↦ (p₁ Kxy′)  ◅ ↦ (p₂ Kxy′)  ◅ δ))

   ∎

 
 K-pres-out (Case⊕ xy of f or g) (sCase⊕ sxy sf sg) p δ with  p₂ (⟦ S ⊨      xy ⟧s  sxy         (Env⊇ p δ))  in eqO
                                                        |     p₂ (⟦ S ⊨ K p  xy ⟧s  (sK p sxy)  δ)           in eqK

 -- absurd cases, where the non-K'ed goes on way and the K'ed goes another
 ... | inj₂ y′ | inj₁ Kx′ = ⊥ where postulate ⊥ : _
 ... | inj₁ x′ | inj₂ Ky′ = ⊥ where postulate ⊥ : _

 ... | inj₁ x′ | inj₁ Kx′ =
   let  inj₁-subproof = trans (sym eqO) (trans (K-pres-out xy sxy p δ) eqK)
   in
   begin
     p₂ (⟦ S ⊨          f ⟧s  sf             (↦ x′   ◅ Env⊇ p δ))
   ≡⟨ cong  (λ t → p₂ (⟦ S ⊨ f ⟧s sf (↦ t ◅ Env⊇ p δ))) (inj₁-injective inj₁-subproof) ⟩
     p₂ (⟦ S ⊨          f ⟧s  sf             (↦ Kx′  ◅ Env⊇ p δ))
   ≡⟨ K-pres-out-bound₁ f sf p δ Kx′ ⟩
     p₂ (⟦ S ⊨ K (I p)  f ⟧s  (sK (I p) sf)  (↦ Kx′  ◅ δ))
   ∎

 ... | inj₂ y′ | inj₂ Ky′ =
   let  inj₂-subproof = trans (sym eqO) (trans (K-pres-out xy sxy p δ) eqK)
   in
   begin
     p₂ (⟦ S ⊨          g ⟧s  sg             (↦ y′   ◅ Env⊇ p δ))
   ≡⟨ cong (λ t → p₂ (⟦ S ⊨ g ⟧s sg (↦ t ◅ Env⊇ p δ))) (inj₂-injective inj₂-subproof) ⟩
     p₂ (⟦ S ⊨          g ⟧s  sg             (↦ Ky′  ◅ Env⊇ p δ))
   ≡⟨ K-pres-out-bound₁ g sg p δ Ky′ ⟩
     p₂ (⟦ S ⊨ K (I p)  g ⟧s  (sK (I p) sg)  (↦ Ky′  ◅ δ))
   ∎
\end{code}
%</K-pres-out>


%<*K-pres-s>
\AgdaTarget{K-pres-s}
\begin{code}
 K-pres-s :  ∀ {c : λB[ G ] Γ τ} (s : St c) (p : Δ ⊇ Γ) (δ : CtxS Δ)
                  →     sK p (  p₁ (⟦ S ⊨ c      ⟧s s         (Env⊇ p δ)))
                     ≡          p₁ (⟦ S ⊨ K p c  ⟧s (sK p s)  δ)
 
 K-pres-s sUnit               p δ = refl
 K-pres-s sCon                p δ = refl
 K-pres-s sNil                p δ = refl
 K-pres-s (sx s， sy)         p δ = cong₂ _s，_  (K-pres-s sx p δ) (K-pres-s sy p δ)
 K-pres-s (sInl sx)           p δ = cong  sInl   (K-pres-s sx p δ)
 K-pres-s (sInr sy)           p δ = cong  sInr   (K-pres-s sy p δ)
 K-pres-s (sCons sx sxs)      p δ = cong₂ sCons  (K-pres-s sx p δ) (K-pres-s sxs p δ)
 K-pres-s (sHead sxs)         p δ = cong  sHead  (K-pres-s sxs p δ)
 K-pres-s (sTail sxs)         p δ = cong  sTail  (K-pres-s sxs p δ)

 K-pres-s s⟨ sg ⟩             p δ = ⊥ where postulate ⊥ : _
 K-pres-s s[ i ]              p δ = ⊥ where postulate ⊥ : _
 K-pres-s (sLoop si sc)       p δ = ⊥ where postulate ⊥ : _
 K-pres-s (sCase⊗ sxy sf)     p δ = ⊥ where postulate ⊥ : _
 K-pres-s (sCase⊕ sxy sf sg)  p δ = ⊥ where postulate ⊥ : _
 K-pres-s (sLet sx sb)        p δ = ⊥ where postulate ⊥ : _
\end{code}
%</K-pres-s>


%<*K-pres>
\AgdaTarget{K-pres}
\begin{code}
 K-pres :  ∀ (c : λB[ G ] Γ τ) (s : St c) (p : Δ ⊇ Γ) (δ : CtxS Δ)
                →     map× (sK p) id (  ⟦ S ⊨ c     ⟧s  s         (Env⊇ p δ))
                   ≡                    ⟦ S ⊨ K p c ⟧s  (sK p s)  δ
 K-pres c s p δ = ×-≡,≡→≡  (K-pres-s s p δ)
                           (K-pres-out c s p δ)
\end{code}
%</K-pres>


* Re-export the module so that the definitions are available with the extra telescope in front
\begin{code}
open Params public
\end{code}
