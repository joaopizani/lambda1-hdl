\begin{code}
module Lambda1.Core.Notation where

open import Level using (Level)
open import Data.Unit using (⊤)
open import Data.Product using (_×_; _,_)
open import Data.Sum using (_⊎_)
open import Data.Nat using (ℕ; zero; suc)
\end{code}


%<*variables-levels-sets>
\AgdaTarget{ℓ,ℓ₁,ℓ₂,ℓ₃,ℓ₄,ℓ₅,α,β,φ,ψ,θ,α₁,α₂,β₁,β₂,φ₁,φ₂,ψ₁,ψ₂,θ₁,θ₂}
\begin{code}
variable
  ℓ  ℓ₁ ℓ₂ ℓ₃ ℓ₄ ℓ₅ : Level
  α β φ ψ θ  α₁ α₂ β₁ β₂ φ₁ φ₂ ψ₁ ψ₂ θ₁ θ₂ : Set ℓ
\end{code}
%</variables-levels-sets>


%<*variables-naturals>
\begin{code}
variable
  n m h  : ℕ
\end{code}
%</variables-naturals>


%<*sucn-ary-union-of-units>
\AgdaTarget{sucn-ary-⊤⊎}
\begin{code}
sucn-ary-⊤⊎ : (m : ℕ) → Set 
sucn-ary-⊤⊎ zero      = ⊤
sucn-ary-⊤⊎ (suc m′)  = ⊤ ⊎ sucn-ary-⊤⊎ m′
\end{code}
%</sucn-ary-union-of-units>


%<*idAcc>
\AgdaTarget{idAcc}
\begin{code}
idAcc : (α → β) → (φ → α → φ × β)
idAcc f s x = s , f x
\end{code}
%</idAcc>
