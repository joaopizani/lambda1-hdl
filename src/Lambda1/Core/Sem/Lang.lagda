\begin{code}
module Lambda1.Core.Sem.Lang where

open import Data.Product using (_×_; _,′_)

open import Lambda1.Core.Notation using (α; β; φ)
open import Lambda1.Core.Syn.Gates using (Gates)
open import Lambda1.Core.Syn.Lang using (λB[_])
\end{code}


%<*idState>
\AgdaTarget{idState}
\begin{code}
idState : (α → β) → (φ → α → φ × β)
idState f s x = s ,′ f x
\end{code}
%</idState>


%<*CircF>
\AgdaTarget{CircF}
\begin{code}
CircF : (B : Set) → Set₁
CircF B = ∀ {G : Gates B} {Γ τ} (c : λB[ G ] Γ τ) → Set
\end{code}
%</CircF>
