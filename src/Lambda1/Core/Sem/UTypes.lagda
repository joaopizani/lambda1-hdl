\begin{code}
module Lambda1.Core.Sem.UTypes where

open import Function using (const; id; _∘′_)
open import Data.Nat.Base using (ℕ)
open import Data.Unit.Base using (tt; ⊤)
open import Data.Product using (_×_; _,′_) renaming (map to map×)
open import Data.Sum.Base using (_⊎_; inj₁) renaming (map to map⊎)
open import Data.Vec.Base using (Vec; map; replicate)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)
open import Data.Star.Decoration renaming (gmapAll to mapEnv)
open import Relation.Binary.PropositionalEquality.Core using (_≡_)
import Data.Star.Environment as Env

open import Lambda1.Core.Syn.UTypes using (B; C; U; 𝟙; ι; _⊗_; _⊕_; vec; σ; ρ; ω; τ; Ctx; foldτ; ⇓τ; ⇓Γ)
\end{code}


%<*modules-env>
\begin{code}
open module EnvUImpSem {B : Set} = Env (U B) using (Env; lookup) public
\end{code}
%</modules-env>


%<*semantic-type-for-U>
\AgdaTarget{UF}
\begin{code}
UF : (B : Set) → Set₁
UF B = (τ : U B) → Set
\end{code}
%</semantic-type-for-U>


%<*semantic-type-for-Ctx>
\AgdaTarget{CtxF}
\begin{code}
CtxF : (B : Set) → Set₁
CtxF B = (Γ : Ctx B) → Set
\end{code}
%</semantic-type-for-Ctx>



* A generic (well-typed) SEMANTIC REFINEMENT function, based on the refinement for the syntax of EDSL types
%<*semantic-refinement-generic>
\AgdaTarget{⇓τ}
\begin{code}
⇓t′ :  {𝟙ₐ : Set} {_⊗ₐ_ _⊕ₐ_ : Set → Set → Set} {vecₐ : Set → ℕ → Set} {ι′ : U C}
       (⇓ι : B → foldτ 𝟙ₐ C _⊗ₐ_ _⊕ₐ_ vecₐ ι′)
       → let foUB = foldτ 𝟙ₐ B _⊗ₐ_ _⊕ₐ_ vecₐ
             foUC = foldτ 𝟙ₐ C _⊗ₐ_ _⊕ₐ_ vecₐ
         in
             (∀ υ ω → foUB (υ ⊗ ω) → foUB υ × foUB ω)
           → (∀ υ ω → foUC (⇓τ ι′ υ) × foUC (⇓τ ι′ ω) → foUC (⇓τ ι′ υ) ⊗ₐ foUC (⇓τ ι′ ω))

           → (∀ υ ω → foUB (υ ⊕ ω) → foUB υ ⊎ foUB ω)
           → (∀ υ ω → foUC (⇓τ ι′ υ) ⊎ foUC (⇓τ ι′ ω) → foUC (⇓τ ι′ υ) ⊕ₐ foUC (⇓τ ι′ ω))

           → (∀ υ n → foUB (vec υ n) → Vec (foUB υ) n)
           → (∀ υ n → Vec (foUC (⇓τ ι′ υ)) n → vecₐ (foUC (⇓τ ι′ υ)) n)

           → (τ : U B) → foUB τ → foUC (⇓τ ι′ τ)
⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n 𝟙          = id
⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n ι          = ⇓ι
⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n (σ ⊗ ρ)    = ⇑⊗ σ ρ ∘′ map× (⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n σ) (⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n ρ)  ∘′ ⇓⊗ σ ρ
⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n (σ ⊕ ρ)    = ⇑⊕ σ ρ ∘′ map⊎ (⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n σ) (⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n ρ)  ∘′ ⇓⊕ σ ρ
⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n (vec σ n)  = ⇑n σ n ∘′ map  (⇓t′ ⇓ι ⇓⊗ ⇑⊗ ⇓⊕ ⇑⊕ ⇓n ⇑n σ)                               ∘′ ⇓n σ n
\end{code}
%</semantic-refinement-generic>



-- Only applicable to simulation
<*semantic-function-U-denotational>
\AgdaTarget{Val}
\begin{code}
Val : UF B
Val {B} = foldτ ⊤ B _×_ _⊎_ Vec
\end{code}
</semantic-function-U-denotational>


%<*semantic-refinement-values>
\AgdaTarget{⇓t}
\begin{code}
⇓t : {ι′ : U C} (⇓ι : Val ι → Val ι′) {τ : U B} → Val τ → Val (⇓τ ι′ τ)
⇓t ⇓ι {τ} = ⇓t′ ⇓ι id⇓⊗ id⇑⊗ id⇓⊕ id⇑⊕ id⇓n id⇑n τ
  where  id⇓⊗ = λ _ _ → id;  id⇑⊗ = λ _ _ → id  -- each of the funcs have
         id⇓⊕ = λ _ _ → id;  id⇑⊕ = λ _ _ → id  -- differently-typed args
         id⇓n = λ _ _ → id;  id⇑n = λ _ _ → id  -- so can't be one single expr
\end{code}
%</semantic-refinement-values>


%<*semantic-refinement-environments>
\AgdaTarget{⇓γ}
\begin{code}
⇓γ : {ι′ : U C} (⇓ι : Val ι → Val ι′) {Γ : Ctx B} → Env Val Γ → Env Val (⇓Γ ι′ Γ) 
⇓γ {ι′ = ι′} ⇓ι = mapEnv  (const tt)
                          (⇓τ ι′)                      -- mapping at type level
                          (λ {_} {_} {τ} → ⇓t ⇓ι {τ})  -- mapping at value level
\end{code}
%</semantic-refinement-environments>


%<*def>
\AgdaTarget{def}
\begin{code}
def : (b : B) (τ : U B) → Val τ
def _ 𝟙          = tt                     -- no choice
def b ι          = b                      -- no choice
def b (τ ⊗ υ)    = def b τ ,′ def b υ     -- no choice
def b (τ ⊕ _)    = inj₁ (def b τ)         -- could be inj₂, choice made here
def b (vec τ n)  = replicate n (def b τ)  -- could be [], choice made here
\end{code}
%</def>


%<*specEnv1>
\AgdaTarget{specEnv₁}
\begin{code}
specEnv₁ : ∀ (τ : U B) {Γ} (f : Val σ → Val τ) → Env Val (σ ◅ Γ) → Val τ
specEnv₁ _ f (↦ x ◅ _) = f x
\end{code}
%</specEnv1>


%<*specEnv2>
\AgdaTarget{specEnv₂}
\begin{code}
specEnv₂ : ∀ (τ : U B) {Γ} (f : Val σ → Val ρ → Val τ) → Env Val (σ ◅ ρ ◅ Γ) → Val τ
specEnv₂ _ f (↦ x ◅ ↦ y ◅ _) = f x y
\end{code}
%</specEnv2>


%<*specEnv3>
\AgdaTarget{specEnv₃}
\begin{code}
specEnv₃ : ∀ (τ : U B) {Γ}  (f : Val σ → Val ρ → Val ω → Val τ)
                            → Env Val (σ ◅ ρ ◅ ω ◅ Γ) → Val τ
specEnv₃ _ f (↦ x ◅ ↦ y ◅ ↦ z ◅ _) = f x y z
\end{code}
%</specEnv3>
