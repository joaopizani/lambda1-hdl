\begin{code}
module Lambda1.Core.Sem.Gates where

open import Data.Product using (_×_)
open import Data.Sum.Base using (inj₁; inj₂; [_,_]′)

open import Lambda1.Core.Syn.UTypes using (B; U; Ctx)
open import Lambda1.Core.Syn.Gates using (Gates; G₁; G₂; _⊞_)

open import Lambda1.Core.Sem.UTypes using (Val; Env)
\end{code}


<*GateF>
\AgdaTarget{GateF}
\begin{code}
GateF : (B : Set) → Set₁
GateF B = (σ : U B) (Γ : Ctx B) (τ : U B) → Set
\end{code}
</GateF>


* Parameterized by a "gate semantics type constructor"
\begin{code}
gS′ : (GateSem : GateF B) (G : Gates B) → Set
gS′ GateSem G = let open Gates G in (g : gIx) → GateSem (gSt g) (gCtx g) (gOut g)
\end{code}


\begin{code}
infixr 1 _gS⊞_
\end{code}

%<*gatelib-union-semantics>
\AgdaTarget{\_gS⊞\_}
\begin{code}
_gS⊞_ : {F : GateF B} (S₁ : gS′ F G₁) (S₂ : gS′ F G₂) → gS′ F (G₁ ⊞ G₂)
(S₁ gS⊞ S₂) (inj₁ x) = S₁ x
(S₁ gS⊞ S₂) (inj₂ y) = S₂ y
\end{code}
%</gatelib-union-semantics>



-- Applicable only to simulation
%<*GateS>
\AgdaTarget{GateS}
\begin{code}
GateS : GateF B
GateS σ Γ τ = (s : Val σ) (γ : Env Val Γ) → Val σ × Val τ
\end{code}
%</GateS>


-- TODO: make the general definition be used in the other modules, instead of the specific one
%<*gS>
\AgdaTarget{gS}
\begin{code}
gS : (G : Gates B) → Set
gS = gS′ GateS
\end{code}
%</gS>
