\begin{code}
module Lambda1.HOAS.Syn.State where

open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)

open import Lambda1.Core.Syn.UTypes using (B; τ; σ; Ctx; Γ; reIx₀∋)
open import Lambda1.Core.Syn.Lang using (module λB[_]);  open λB[_]
open import Lambda1.Core.Syn.Gates using (G; Gates)

open import Lambda1.HOAS.Syn.Lang using (λH[_]; reIx₀)

import Lambda1.Core.Syn.State
open Lambda1.Core.Syn.State using (module St-explicit);  open St-explicit public
open Lambda1.Core.Syn.State using (St; module St; sDef) public
open St public
\end{code}



* Assumes Δ ⊒ (τ ◅ Γ). Stuck in postulate otherwise
%<*sReIx0>
\AgdaTarget{sReIx₀}
\begin{code}
instance
 sReIx₀ : ∀ {τ} ⦃ Γ : Ctx B ⦄ {Δ : Ctx B} → St {G = G} {Δ} (reIx₀ Γ)
 sReIx₀ {τ = τ} ⦃ Γ ⦄ {Δ} = s[ reIx₀∋ τ Γ Δ ]
\end{code}
%</sReIx0>


\begin{code}
postulate s◅ : {c : λH[ G ] τ} ⦃ sc′ : St (c {Γ}) ⦄ → St (c {σ ◅ Γ})
\end{code}
