\begin{code}
module Lambda1.HOAS.Syn.Lang where

open import Data.Unit.Base using (tt)
open import Data.Sum using (inj₁)
open import Data.Nat.Base using (zero; suc)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)

open import Lambda1.Core.Syn.UTypes using (B; U; 𝟙; _⊗_; _⊕_; vec; τ; σ; ρ; υ; ν; Ctx; reIx₀∋)
open import Lambda1.Core.Syn.Gates using (Gates; G; _⊞_)
open import Lambda1.Core.Syn.Lang renaming (module WithGates to WithGatesCore) public
\end{code}



* Assumes Δ ⊒ (τ ◅ Γ). Stuck in postulate otherwise
%<*reIx0>
\AgdaTarget{reIx₀}
\begin{code}
reIx₀ : ∀ (Γ {Δ} : Ctx B) → λB[ G ] Δ τ
reIx₀ {τ = τ} Γ {Δ} = [ reIx₀∋ τ Γ Δ ]
\end{code}
%</reIx0>


%<*LambdaH>
\AgdaTarget{λH[\_]}
\begin{code}
λH[_] : ∀ {B} (G : Gates B) (τ : U B) → Set
λH[_] {B} G τ = ∀ {Γ : Ctx B} → λB[ G ] Γ τ
\end{code}
%</LambdaH>


%<*let′>
\AgdaTarget{let′}
\begin{code}
let′ : (x : λH[ G ] σ) (f : λH[ G ] σ → λH[ G ] τ) → λH[ G ] τ
(let′ x f) {Γ} = Let x (f (reIx₀ Γ))
\end{code}
%</let′>

%<*let′>
\begin{code}
syntax let′ e (λ x → b) = let′ x ≔ e in′ b
infixr 1 let′
\end{code}
%</let′>


%<*loop>
\AgdaTarget{loop}
\begin{code}
loop : (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)) (x : λH[ G ] ρ) → λH[ G ] τ
(loop f x) {Γ} = Loop (f (reIx₀ Γ) x)
\end{code}
%</loop>

%<*loop-notation>
\begin{code}
syntax loop {σ = σ} (λ s → b) = loop s ∶ σ in′ b
infixr 1 loop
\end{code}
%</loop-notation>

%<*loop-out-next>
\AgdaTarget{loop-out-next}
\begin{code}
loop-out-next : (fo : λH[ G ] σ → λH[ G ] τ) (fn : λH[ G ] σ → λH[ G ] σ) → λH[ G ] τ
(loop-out-next fo fn) {Γ} = Loop-out-next (fo (reIx₀ Γ)) (fn (reIx₀ Γ))
\end{code}
%</loop-out-next>

-- NOTE: s₁ and s₂ are different names for the SAME value, Agda mixfix binding feature requires this.
%<*loop-out-next-notation>
\begin{code}
syntax loop-out-next {σ = σ} (λ s₁ → o) (λ s₂ → n) = loop s₁ type s₂ ∶ σ  out o next n
infixr 1 loop-out-next
\end{code}
%</loop-out-next-notation>


%<*pair>
\AgdaTarget{\_,\_}
\begin{code}
_,_ : (x : λH[ G ] υ) (y : λH[ G ] ν) → λH[ G ] (υ ⊗ ν)
(x , y) = x ， y
\end{code}
%</pair>

%<*case-prod>
\AgdaTarget{case⊗\_of\_}
\begin{code}
case⊗_of_ : (xy : λH[ G ] (υ ⊗ ν)) (f : λH[ G ] υ → λH[ G ] ν → λH[ G ] τ) → λH[ G ] τ
(case⊗_of_ {ν = ν} xy f) {Γ} = Case⊗ xy of f (reIx₀ (ν ◅ Γ)) (reIx₀ Γ)
\end{code}
%</case-prod>


%<*inl>
\AgdaTarget{inl}
\begin{code}
inl : (x : λH[ G ] υ) → λH[ G ] (υ ⊕ ν)
inl x = Inl x
\end{code}
%</inl>

%<*inr>
\AgdaTarget{inr}
\begin{code}
inr : (y : λH[ G ] ν) → λH[ G ] (υ ⊕ ν)
inr y = Inr y
\end{code}
%</inr>

%<*case-coprod>
\AgdaTarget{case⊕\_of\_or\_}
\begin{code}
case⊕_of_or_ : (xy : λH[ G ] (υ ⊕ ν)) (f : λH[ G ] υ → λH[ G ] τ) (g : λH[ G ] ν → λH[ G ] τ) → λH[ G ] τ
(case⊕ xy of f or g) {Γ} = Case⊕ xy of f (reIx₀ Γ) or g (reIx₀ Γ)
\end{code}
%</case-coprod>


%<*nil>
\AgdaTarget{nil}
\begin{code}
nil : λH[ G ] (vec τ zero)
nil = Nil
\end{code}
%</nil>

%<*cons>
\AgdaTarget{cons}
\begin{code}
cons : ∀ {n} (x : λH[ G ] τ) (xs : λH[ G ] (vec τ n)) → λH[ G ] (vec τ (suc n))
cons x xs = Cons x xs
\end{code}
%</cons>

%<*mapAccL-par>
\AgdaTarget{mapAccumL-par}
\begin{code}
mapAccL-par : ∀ {n} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (σ ⊗ vec τ n)
(mapAccL-par {ρ = ρ} f e xs) {Γ} = MapAccL-par (f (reIx₀ (ρ ◅ Γ)) (reIx₀ Γ)) e xs
\end{code}
%</mapAccumL-par>




%<*fork>
\AgdaTarget{fork}
\begin{code}
fork : (x : λH[ G ] τ) → λH[ G ] (τ ⊗ τ)
fork x = let′ y ≔ x in′ (y , y)
\end{code}
%</fork>


%<*fst>
\AgdaTarget{fst}
\begin{code}
fst : (xy : λH[ G ] (υ ⊗ ν)) → λH[ G ] υ
fst xy = case⊗ xy of (λ x _ → x)
\end{code}
%</fst>

%<*snd>
\AgdaTarget{snd}
\begin{code}
snd : (xy : λH[ G ] (υ ⊗ ν)) → λH[ G ] ν
snd xy =  case⊗ xy of (λ _ y → y)
\end{code}
%</snd>


-- TODO: internalize this?
%<*replicate>
\AgdaTarget{replicate}
\begin{code}
replicate : ∀ {n} (x : λH[ G ] τ) → λH[ G ] (vec τ n)
replicate {n = zero}   _ = nil
replicate {n = suc _}  x = cons x (replicate x)
\end{code}
%</replicate>


units : ∀ {n B} → λH[_] {B} UNIT (vec 𝟙 n)
units = replicate unit
%<*units>
\AgdaTarget{units}
\begin{code}
\end{code}
%</ones>




\begin{code}
module WithGates {B} (G : Gates B) where
\end{code}

\begin{code}
 λh : ∀ (τ : U B) → Set
 λh τ = λH[ G ] τ
\end{code}
