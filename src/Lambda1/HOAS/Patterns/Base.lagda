\begin{code}
module Lambda1.HOAS.Patterns.Base where

open import Function using (_∘′_)
open import Data.Nat.Base using (zero; suc)
open import Data.Vec using (Vec; []; _∷_; replicate; map)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (_◅_)

open import Lambda1.Core.Syn.UTypes using (B; U; 𝟙; _⊗_; vec; σ; ρ; τ; Ctx; Γ)
open import Lambda1.Core.Syn.Gates using (G)
open import Lambda1.Core.Syn.Lang using (Unit)
open import Lambda1.Core.Syn.State using (St; _s，_; sCase⊗; sLoop; sMapAccL-par)

open import Lambda1.Core.Sem.UTypes using (Val)

open import Lambda1.HOAS.Syn.Lang using (reIx₀; λH[_]; _,_; loop; fst; snd; nil; cons; mapAccL-par)

open import Lambda1.Utils.BasicCombinators using (Replicate)
\end{code}




%<*mapper>
\AgdaTarget{mapper}
\begin{code}
mapper : (f : λH[ G ] ρ → λH[ G ] τ) (u : λH[ G ] σ) (x : λH[ G ] ρ) → λH[ G ] (σ ⊗ τ)
mapper f u x = u , f x
\end{code}
%</mapper>

%<*smapper>
\AgdaTarget{smapper}
\begin{code}
sMapper : ∀ {Γ : Ctx B} {f : λH[ G ] ρ → λH[ G ] τ} {u : λH[ G ] σ} {x : λH[ G ] ρ} (sfx : St (f x)) (su : St u) → St {Γ = Γ} (mapper f u x)
sMapper sfx su = su s， sfx
\end{code}
%</smapper>


%<*iterator>
\AgdaTarget{iterator}
\begin{code}
iterator : (f : λH[ G ] σ → λH[ G ] σ) (u : λH[ G ] σ) (x : λH[_] {B} G ρ) → λH[ G ] (σ ⊗ σ)
iterator f u _ = u , f u
\end{code}
%</iterator>


%<*folder>
\AgdaTarget{folder}
\begin{code}
folder : (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (u : λH[ G ] σ) (x : λH[ G ] ρ) → λH[ G ] (σ ⊗ σ)
folder f u x = u , f u x
\end{code}
%</folder>





%<*smapAccL-par>
\AgdaTarget{smapAccL-par}
\begin{code}
smapAccL-par : ∀  {n} {f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)} {e : λH[ G ] σ} {xs : λH[ G ] (vec ρ n)}
                  (sf : (s : λH[ G ] σ) (r : λH[ G ] ρ) → St {Γ = σ ◅ ρ ◅ Γ} (f s r)) (se : St e) (sxs : St xs) → St  (mapAccL-par f e xs)
smapAccL-par {n = n} sf se sxs = sMapAccL-par (replicate n (sf (reIx₀ _) (reIx₀ _))) se sxs
\end{code}
%</smapAccL-par>


%<*sloop>
\AgdaTarget{sloop}
\begin{code}
sloop : ∀ {f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)} {x : λH[ G ] ρ}
          (sf : (s : λH[ G ] σ) (r : λH[ G ] ρ) → St {Γ = σ ◅ Γ} (f s r)) (e : Val σ) → St (loop f x)
sloop {x = x} sf e = sLoop e (sf (reIx₀ _) x)
\end{code}
%</sloop>






%<*map-par>
\AgdaTarget{map-par}
\begin{code}
map-par : ∀ {n} (f : λH[ G ] ρ → λH[ G ] τ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (vec τ n)
map-par f = snd ∘′ mapAccL-par (mapper f) Unit
\end{code}
%</map-par>

smap-par : ∀ {n} {f : λH ρ → λH τ} (sf : St {Γ = ρ ◅ Γ} (f (reIx₀ Γ))) (xs : Vec (El ρ) n) → St (map-par f (val xs))
smap-par {Γ = Γ} {ρ} {τ} {f} sf xs = sCase⊗ (sMapAccumL-par (replicate sMapper) sOne (sVal xs)) (sReIx₀ Γ)



%<*map-seq>
\AgdaTarget{map-seq}
\begin{code}
map-seq : ∀ (f : λH[ G ] ρ → λH[ G ] τ) (x : λH[ G ] ρ) → λH[ G ] τ
map-seq f = loop (mapper {σ = 𝟙} f)
\end{code}
%</map-seq>

smap-seq : ∀ {f : λH ρ → λH τ} (sf : St {Γ = ρ ◅ Γ} (f (reIx₀ Γ))) (x : El ρ) → St (map-seq f (val x))
smap-seq {Γ = Γ} sf x = sMapAccumL-seq tt ((sReIx₀ Γ) s， ({!!}))




%<*iterate-par>
\AgdaTarget{iterate-par}
\begin{code}
iterate-par : ∀ {n} (f : λH[ G ] τ → λH[ G ] τ) (x : λH[ G ] τ) → λH[ G ] (τ ⊗ vec τ n)
iterate-par f x = mapAccL-par (iterator f) x (Replicate Unit)
\end{code}
%</iterate-par>

%<*iterate-seq>
\AgdaTarget{iterate-seq}
\begin{code}
iterate-seq : (f : λH[ G ] τ → λH[ G ] τ) → λH[ G ] τ
iterate-seq f = loop (iterator f) Unit
\end{code}
%</iterate-seq>


%<*repeat-par>
\AgdaTarget{repeat-par}
\begin{code}
repeat-par : ∀ {n} (f : λH[ G ] τ → λH[ G ] τ) (x : λH[ G ] τ) → λH[ G ] τ
repeat-par {n = n} f = fst ∘′ iterate-par {n = n} f
\end{code}
%</repeat-par>

%<*repeat-seq>
\AgdaTarget{repeat-seq}
\begin{code}
repeat-seq : (f : λH[ G ] τ → λH[ G ] τ) → λH[ G ] τ
repeat-seq = iterate-seq
\end{code}
%</repeat-seq>



%<*foldl-scanl-par>
\AgdaTarget{foldl-scanl-par}
\begin{code}
foldl-scanl-par : ∀ {n} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (σ ⊗ vec σ n)
foldl-scanl-par f = mapAccL-par (folder f)
\end{code}
%</foldl-scanl-par>

%<*foldl-scanl-seq>
\AgdaTarget{foldl-scanl-seq}
\begin{code}
foldl-scanl-seq : (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (x : λH[ G ] ρ) → λH[ G ] σ
foldl-scanl-seq f = loop (folder f)
\end{code}
%</foldl-scanl-seq>

%<*foldl-par>
\AgdaTarget{foldl-par}
\begin{code}
foldl-par : ∀ {n} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] σ
foldl-par f e = fst ∘′ foldl-scanl-par f e
\end{code}
%</foldl-par>

%<*scanl-par>
\AgdaTarget{scanl-par}
\begin{code}
scanl-par : ∀ {n} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (vec σ n)
scanl-par f e = snd ∘′ foldl-scanl-par f e
\end{code}
%</scanl-par>




%<*triangle>
\AgdaTarget{triangle}
\begin{code}
triangle : ∀ {n} (f : λH[ G ] τ → λH[ G ] τ) (x : λH[ G ] τ) → λH[ G ] (vec τ (suc n))
triangle {n = zero}    f x = cons x nil
triangle {n = suc n′}  f x = cons x (map-par f (triangle {n = n′} f x))
\end{code}
%</triangle>


-- Note: the mapAccL being applied is the "insertion" step in insertion sort
\begin{code}
\end{code}
insertionSort : ∀ {n} (c : λH[ G ] τ → λH[ G ] τ → λH[ G ] (τ ⊗ τ)) (xs : λH[ G ] (vec τ n)) → λH[ G ] (vec τ n)
insertionSort {n = zero}    c _   = nil
insertionSort {n = suc n′}  c xs  = case[] xs of λ x xs′ → mapAccL-par n′ c x (insertionSort {n = n′} c xs′)


---- Attempt (failed) to write seq. insertionSort. Loop can create NFAs, but L(insertionSort) is not regular.
---- Input stream example:  x ≡ 3 ∷ 7 ∷ 1 ∷ 2 ∷ 8 ∷ 3 ∷ 4 ∷ 9 ∷ 9 ...
---- Output stream example:  ⟦ insertionSort ⟧ x ≡ ... ∷ 1 ∷ 2 ∷ 3 ∷ 3 ∷ 4 ∷ 7 ∷ 8 ∷ 9 ∷ 9 ∷ ...
---- The PREFIX "..." in the output stream indicates that the smallest element among those presented
---- in a given prefix of the input CANNOT be determined BEFORE ALL THE ELEMENTS TO BE SORTED HAVE BEEN PRESENTED
---- AT THE INPUT.
---- CAN BE FIXED WITH AN UPPER BOUND ON INPUT SIZE
--%<*insertionSort-seq>
--\begin{code}
postulate insertionSort-seq : (cmp : λH[ G ] τ → λH[ G ] τ → λH[ G ] (τ ⊗ τ)) (x : λH[ G ] τ) → λH[ G ] τ
--\end{code}
--%</insertionSort-seq>
