\begin{code}
module Lambda1.HOAS.Patterns.Properties where
\end{code}


\begin{code}
\end{code}
postulate mapAccumL-par-seq :  ∀ {n B} {Γ : Ctxt B} {σ ρ τ} {f : λH σ → λH ρ → λH (σ ⊗ τ)}
                               {sf : (s : λH σ) (r : λH ρ) → λs (f s r)}
                               (s₀ : El σ)
                               (xs : Vec (El ρ) n) (γ : Env El Γ)
                               →  ⟦ mapAccumL-par f (val s₀) (val xs) |⟧s₂   (st-mapAccumL-par {f = f} sf s₀)  γ
                                  ≛
                                  mapAccumL n  (λ s x → {!⟦ mapAccumL-seq f (val x) |⟧s {!!} {!!}!})   (st-mapAccumL-seq {f = f} sf s₀)  xs



\begin{code}
\end{code}
postulate
  map-par-seq : ∀ {n B} {Γ : Ctxt B} {ρ τ} {f : λH ρ → λH τ} {sf : λs {Γ = ρ ◅ Γ} (f (reIx₀ Γ))} (xs : Vec (El ρ) n) (γ : Env El Γ)
    →     ⟦ map-par f (val xs) |⟧s₂  (st-map-par {f = f}   sf xs)  γ
       ≡  map  (λ x →  ⟦ map-seq f (val x) |⟧s₂  (st-map-seq″ {f = f}  sf x)  γ)  xs

