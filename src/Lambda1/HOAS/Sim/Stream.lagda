\begin{code}
{-# OPTIONS --guardedness #-}
\end{code}

\begin{code}
module Lambda1.HOAS.Sim.Stream where

open import Function using (flip)
open import Data.Product using (_×_)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε)
open import Codata.Musical.Stream using (Stream; repeat)

open import Lambda1.Core.SimpleTypes using (Γ; τ)
open import Lambda1.Gates using (G; Gates)
open import Lambda1.State using (λs)
open import Lambda1.Simulation using (gS; CircSim; CircSim₂; TyS)
open import Lambda1.Simulation.Stream using (⟦_⊨_⟧*; ⟦_⊨_⟧★)
open import Lambda1.Surface.Lambda1 using (λH[_])
open import Lambda1.Surface.Simulation using (CircS; CircS₂)
\end{code}


\begin{code}
⦅_⊨_⦆*̊  : ∀ (S : gS G) (c : λH[ G ] τ) → CircSim  Stream Stream Stream {Γ = Γ} c
⦅_⊨_⦆★̊  : ∀ (S : gS G) (c : λH[ G ] τ) → CircSim₂ Stream Stream        {Γ = Γ} c
⦅ S ⊨ c ⦆*̊  = ⟦ S ⊨ c ⟧*
⦅ S ⊨ c ⦆★̊  = ⟦ S ⊨ c ⟧★
\end{code}


\begin{code}
⦅_⊨_⦆* : ∀ (S : gS G) (c : λH[ G ] τ) → CircS  Stream Stream c
⦅_⊨_⦆★ : ∀ (S : gS G) (c : λH[ G ] τ) → CircS₂ Stream        c
⦅ S ⊨ c ⦆* = flip ⦅ S ⊨ c ⦆*̊ (repeat ε)
⦅ S ⊨ c ⦆★ = flip ⦅ S ⊨ c ⦆★̊ (repeat ε)
\end{code}



\begin{code}
module SimulationSurfaceWithGateSim {B} {G : Gates B} (S : gS G) where
\end{code}

 ⦅_⦆*̊ : ∀ (c : λH[ G ] τ) (s : λs {B} c) → (Stream (λs c) × Stream (TyS τ))
 ⦅_⦆★̊ : ∀ (c : λH[ G ] τ) (s : λs {B} c) → Stream (TyS τ)
\begin{code}
 ⦅_⦆*̊ : ∀ (c : λH[ G ] τ) → CircSim Stream Stream Stream {Γ = Γ} c
 ⦅_⦆★̊ : ∀ (c : λH[ G ] τ) → CircSim₂ Stream Stream {Γ = Γ} c
 ⦅_⦆*̊ = ⦅ S ⊨_⦆*̊
 ⦅_⦆★̊ = ⦅ S ⊨_⦆★̊
\end{code}

\begin{code}
 ⦅_⦆* : ∀ (c : λH[ G ] τ) (s : λs {B} c) → (Stream (λs c) × Stream (TyS τ))
 ⦅_⦆★ : ∀ (c : λH[ G ] τ) (s : λs {B} c) → Stream (TyS τ)
 ⦅_⦆* = ⦅ S ⊨_⦆*
 ⦅_⦆★ = ⦅ S ⊨_⦆★
\end{code}
