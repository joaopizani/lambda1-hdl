\begin{code}
module Lambda1.HOAS.Sim.Base where

open import Function using (id; flip; _∘′_)
open import Data.Product using (_×_)
open import Data.Vec using (Vec; replicate)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive using (ε)

open import Lambda1.Core.Syn.UTypes using (U; B; τ; Ctx; Γ)
open import Lambda1.Core.Syn.Gates using (G; Gates)
open import Lambda1.Core.Syn.Lang using (λB[_])
open import Lambda1.Core.Syn.State using (St)

open import Lambda1.Core.Sem.Gates using (gS)
open import Lambda1.Core.Sem.Lang using (CircF)
open import Lambda1.Core.Sim.Base using (CtxS; CircS; CircS₂; TyS; ⟦_⊨_⟧s; ⟦_⊨_⟧S; ⟦_⊨_⟧n; ⟦_⊨_⟧N)
open import Lambda1.HOAS.Syn.Lang using (λH[_])
\end{code}


%<*Surface-semantic-function-full>
\AgdaTarget{CircHS}
\begin{code}
CircHS : (F : Set → Set) → CircF B
CircHS F {τ = τ} c = (s : St c) → St c × F (TyS τ)
\end{code}
%</Surface-semantic-function-full>

%<*Surface-semantic-function-nostate>
\AgdaTarget{CircHS₂}
\begin{code}
CircHS₂ : (F : Set → Set) → CircF B
CircHS₂ F {τ = τ} c = (s : St c) → F (TyS τ)
\end{code}
%</Surface-semantic-function-nostate>


\begin{code}
⦅_⊨_⦆s̊  : ∀ (S : gS G) (c : λH[ G ] τ) {Γ}     → CircS    id            id            {Γ = Γ} c
⦅_⊨_⦆n̊  : ∀ (S : gS G) (c : λH[ G ] τ) {Γ}  n  → CircS    (flip Vec n)  (flip Vec n)  {Γ = Γ} c
⦅_⊨_⦆S̊  : ∀ (S : gS G) (c : λH[ G ] τ) {Γ}     → CircS₂   id            id            {Γ = Γ} c
⦅_⊨_⦆N̊  : ∀ (S : gS G) (c : λH[ G ] τ) {Γ}  n  → CircS₂   (flip Vec n)  (flip Vec n)  {Γ = Γ} c

⦅_⊨_⦆s  : ∀ (S : gS G) (c : λH[ G ] τ)     → CircHS    id            c
⦅_⊨_⦆n  : ∀ (S : gS G) (c : λH[ G ] τ)  n  → CircHS    (flip Vec n)  c
⦅_⊨_⦆S  : ∀ (S : gS G) (c : λH[ G ] τ)     → CircHS₂   id            c
⦅_⊨_⦆N  : ∀ (S : gS G) (c : λH[ G ] τ)  n  → CircHS₂   (flip Vec n)  c
\end{code}


\AgdaTarget{⦅\_⊨\_⦆s̊,⦅\_⊨\_⦆n̊,⦅\_⊨\_⦆*̊,⦅\_⊨\_⦆S̊,⦅\_⊨\_⦆N̊,⦅\_⊨\_⦆★̊}
\begin{code}
⦅ S ⊨ c ⦆s̊    = ⟦ S ⊨ c ⟧s
⦅ S ⊨ c ⦆n̊    = ⟦ S ⊨ c ⟧n
⦅ S ⊨ c ⦆S̊    = ⟦ S ⊨ c ⟧S
⦅ S ⊨ c ⦆N̊    = ⟦ S ⊨ c ⟧N
\end{code}

\AgdaTarget{⦅\_⊨\_⦆s,⦅\_⊨\_⦆S,⦅\_⊨\_⦆*,⦅\_⊨\_⦆★,⦅\_⊨\_⦆n,⦅\_⊨\_⦆N}
\begin{code}
⦅ S ⊨ c ⦆s      = flip ⦅ S ⊨ c ⦆s̊ ε
⦅ S ⊨ c ⦆S      = flip ⦅ S ⊨ c ⦆S̊ ε
⦅ S ⊨ c ⦆n n s  = ⦅ S ⊨ c ⦆n̊ n s (replicate n ε)
⦅ S ⊨ c ⦆N n s  = ⦅ S ⊨ c ⦆N̊ n s (replicate n ε)
\end{code}



\begin{code}
module WithGateSim (S : gS G) where
 ⦅_⦆s̊  : ∀ (c : λH[ G ] τ) {Γ}    → CircS    id            id            {Γ = Γ} c
 ⦅_⦆n̊  : ∀ (c : λH[ G ] τ) {Γ} n  → CircS    (flip Vec n)  (flip Vec n)  {Γ = Γ} c
 ⦅_⦆S̊  : ∀ (c : λH[ G ] τ) {Γ}    → CircS₂   id            id            {Γ = Γ} c
 ⦅_⦆N̊  : ∀ (c : λH[ G ] τ) {Γ} n  → CircS₂   (flip Vec n)  (flip Vec n)  {Γ = Γ} c

 ⦅_⦆s  : ∀ (c : λH[ G ] τ)     (s : St c)  → (St c × TyS τ)
 ⦅_⦆n  : ∀ (c : λH[ G ] τ)  n  (s : St c)  → (St c × Vec (TyS τ) n)
 ⦅_⦆S  : ∀ (c : λH[ G ] τ)     (s : St c)  → TyS τ
 ⦅_⦆N  : ∀ (c : λH[ G ] τ)  n  (s : St c)  → Vec (TyS τ) n
\end{code}

\AgdaTarget{⦅\_⦆s̊,⦅\_⦆n̊,⦅\_⦆*̊,⦅\_⦆S̊,⦅\_⦆N̊,⦅\_⦆★̊}
\begin{code}
 ⦅_⦆s̊ = ⦅ S ⊨_⦆s̊
 ⦅_⦆n̊ = ⦅ S ⊨_⦆n̊
 ⦅_⦆S̊ = ⦅ S ⊨_⦆S̊
 ⦅_⦆N̊ = ⦅ S ⊨_⦆N̊
\end{code}

\AgdaTarget{⦅\_⦆s,⦅\_⦆S,⦅\_⦆*,⦅\_⦆★,⦅\_⦆n,⦅\_⦆N}
\begin{code}
 ⦅_⦆s = ⦅ S ⊨_⦆s
 ⦅_⦆S = ⦅ S ⊨_⦆S
 ⦅_⦆n = ⦅ S ⊨_⦆n
 ⦅_⦆N = ⦅ S ⊨_⦆N
\end{code}
